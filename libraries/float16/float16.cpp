#include "float16.h"
#include <Arduino.h>

float16::float16() : val(0){};

float16::float16(float f)
{
    if (f == 0)
    {
        val = 0;
        return;
    }

    const unsigned long int ff = *reinterpret_cast<unsigned long int *>(&f);
    const unsigned long int mant = ff & 0x007FFFFF;

    /* Handle special numbers. */
    if ((ff & 0x7F800000) == 0x7F800000)
    {
        /* exponent is all ones, it's a nan or inf. Shift down to keep
         * sign bit. */
        val = (ff >> 16);

        /* Now we don't know what we have in the mantissa, but we may not care. */
        if (mant)
        {
            /* mantissa is non-zero, it's a NaN. This means we can
             * ignore the junk in our mantissa and just make sure we
             * have at least one bit set. */
            val |= 0x0001;
        }
        else
        {
            /* Mantissa is zero, so it's an inf. Clear our matissa, too. */
            val &= (0xFFFF << (15 - nbits_exp));
        }
        return;
    }

    const uint8_t fexp(((ff & 0x7FFFFFFF) >> 23) - 127);

    /*
    Serial.print("floating is ");
    Serial.println(ff,HEX);
    Serial.print("exponent is ");
    Serial.println(fexp,DEC);
    Serial.print("mantissa is ");
    Serial.println(mant,HEX);
    */

    const uint16_t bias = (1 << (nbits_exp - 1)) - 1;
    /*
    Serial.print("new bias is ");
    Serial.println(bias,DEC);
    */
    const uint16_t newexp = ((fexp + bias) & (0xFFFF >> (16 - nbits_exp)));
    /*
    Serial.print("newexp is ");
    Serial.println(newexp,HEX);
    */

    // new mantissa is just the 16-nbits_exp-1 most significant bits of the old
    const uint16_t newmant = (mant >> (23 - (16 - nbits_exp - 1)));

    /*
    Serial.print("newmant is ");
    Serial.println(newmant,HEX);
    */

    // construct new number
    val = newmant | (newexp << (15 - nbits_exp)) | ((ff >> 31) << 15);
    /*
    Serial.print("float16 is ");
    Serial.println(val,HEX);

    // test it
    float fff=float(*this);

    Serial.print("Difference is ");
    Serial.println(f/fff);
    */
}

float16::operator float() const
{
    if (val == 0)
        return 0.0f;

    // extract sign bit
    unsigned long int ff = val & 0x8000;
    // and shift it up
    ff <<= 16;

    // shift up exponent so it's the least significant bits of the
    // 32-bit exp
    const long int oldexp = ((val & 0x7FFF) >> (15 - nbits_exp));

    /* If old exponent is all ones, it's a NaN/Inf, so we must set the
     * new exponent to all ones, too. We don't have to do anything
     * special to the mantissa. */
    if (oldexp == (0x7FFF >> (15-nbits_exp)))
    {
        ff |= 0x7F800000;
    }
    else
    {
        const uint16_t bias = (1 << (nbits_exp - 1)) - 1;
        ff |= ((oldexp - bias + 127) << 23);
    }

    /*
    Serial.print("exp is ");
    Serial.println(oldexp,HEX);
    */

    // shift mantissa up so it's the most significant bits of the 32-bit
    // mantissa
    const uint32_t mantissa_mask = (0xFFFF >> (nbits_exp + 1));
    ff |= ((val & mantissa_mask) << (23 - (15 - nbits_exp)));

    const float f = *reinterpret_cast<float *>(&ff);
    /*
    Serial.print("backconverted float is ");
    Serial.println(f);
    */

    return f;
}
