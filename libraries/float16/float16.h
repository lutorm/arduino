#pragma once

#include <inttypes.h>

/* The float16 class is a simple implementation of something
   float-like that only takes up 2 bytes. The number of bits in the
   exponent is settable using nbits_exp, the remaining bits are used
   up for one sign bit and the mantissa.  This can be used in
   memory-constrained situations when we don't need a full float.
*/

class float16
{
private:
    // number of bits in the exponent
    static constexpr int nbits_exp = 6;

    uint16_t val;

public:
    float16();
    float16(float);
    operator float() const;
};
