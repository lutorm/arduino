#include "PwmPID.h"

PwmPIDBase::PwmPIDBase(float const *        input,
                       float const *        setpoint,
                       prog_uint8_t const * address) :
    PIDEvent(input, &_output, setpoint, true),
    _output(0.0),
    _address(address)
{
    SetOutputLimits(0.0, 1.0);
}

void PwmPIDBase::_dispatch(ms_t now_ms)
{
    PIDEvent::_dispatch(now_ms);
}

void PwmPIDBase::send_data(uint16_t & crc) const
{
    ::send_data_pgmaddr(_address, _output, crc);
}
