#pragma once

#include "Arduino.h"
#include "EventLoop.h"
#include "float16.h"

/* Abstract Core PID controller class, which does not deal with timing itself. */
class PIDCore
{
public:
    // constructor.  links the PID to the Input, Output, and Setpoint.
    // Optional feedforward
    PIDCore(const float * Input,
            float *       Output,
            const float * Setpoint,
            bool          PositiveDirection);

    // sets PID to either Manual (false) or Auto (true)
    void SetEngaged(bool mode, bool bumpless = true);

    // clamps the output to a specific range. 0-255 by default.
    void SetOutputLimits(float, float);

    // this function gives the user the option of changing tunings
    // during runtime for Adaptive control
    void SetTunings(float, float, float);

    void SetDirection(bool);

    void SetTimeout(int t) { Timeout = t; }

    // Instead of saving a copy of the state variables, which wastes
    // memory, we just back-transform the real ones.
    float        GetKp() const { return abs(float(kp)); }
    float        GetKi() const { return abs(ki * 1000.0f / float(GetSampleTime())); }
    float        GetKd() const { return abs(kd * float(GetSampleTime()) / 1000.0f); }
    bool         GetMode() const { return inAuto; }
    bool         GetDirection() const { return posDir; }
    float        GetSetpoint() const { return *mySetpoint; }
    virtual ms_t GetSampleTime() const = 0;

protected:
    // Unconditionally performs the PID calculation.  it should be called every time
    // loop() cycles. ON/OFF and calculation frequency can be set using
    // SetMode SetSampleTime respectively
    void _update();

    // Rescale the gains for a new period
    void _SetSampleTime(ms_t);

    virtual float feedForward() const { return NAN; }
    
private:
    void Initialize(bool bumpless = true);

    bool posDir;
    bool inAuto;

    // Number of samples with invalid inputs before output goes NaN
    int Timeout;
    // Number of invalid samples
    int Ninval;

    float16 outMin, outMax;

    // (P)roportional Tuning Parameter
    float16 kp;
    // (I)ntegral Tuning Parameter
    float16 ki;
    // (D)erivative Tuning Parameter
    float16 kd;

    // * Pointers to the Input, Output, and Setpoint variables
    const float * myInput;
    float *       myOutput;
    const float * mySetpoint;

    float ITerm, lastInput;
};

/* "Classic" PID class, calls millis itself. */
class PID : public PIDCore
{
public:
    PID(const float * Input,
        float *       Output,
        const float * Setpoint,
        float         Kp,
        float         Ki,
        float         Kd,
        bool          PositiveDirection,
        const float * feedforward = nullptr);

    void SetSampleTime(ms_t);

    ms_t GetSampleTime() const override { return SampleTime; }

    // Get the time using millis() and performs the PID calculation if
    // due. Should be called every time loop() cycles. ON/OFF and
    // calculation frequency can be set using SetMode SetSampleTime
    // respectively
    void Compute();

private:
    int           SampleTime;
    unsigned long lastTime;

    // Pointer to an optional feed-forward value
    const float * _feedForward;

    float feedForward() const override { return _feedForward ? *_feedForward : NAN; }
};

/* PID Event class, dispatched by event loop. */
class PIDEvent : public Periodic, public PIDCore
{
public:
    PIDEvent(const float * Input,
             float *       Output,
             const float * Setpoint,
             bool          PositiveDirection);

    void set_period(ms_t period) override;

    ms_t GetSampleTime() const override { return period(); }

    // Performs the PID calculation at the set period. ON/OFF and
    // calculation frequency can be set using SetMode and
    // SetSampleTime, respectively
    void _dispatch(ms_t) override;
};
