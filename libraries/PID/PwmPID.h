#pragma once

#include "PID.h"
#include "serial_data.h"

template<class T>
struct remove_reference
{
    typedef T type;
};
template<class T>
struct remove_reference<T &>
{
    typedef T type;
};
template<class T>
struct remove_reference<T &&>
{
    typedef T type;
};

/*
 * A PID Event that controls a PWM register. Feedforward can be implemented by a subclass.
 */
class PwmPIDBase : public PIDEvent
{
public:
    PwmPIDBase(float const * input, float const * setpoint, prog_uint8_t const address[]);

    void send_data(uint16_t & crc) const;

    float output() const { return _output; }

    /* Set output manually if controller is disabled. */
    void set_output(float o)
    {
        if (!GetMode())
            _output = o;
    }

protected:
    void _dispatch(ms_t) override;

    /* The output from the PID is put here so we can write it to the
     * register. */
    float _output;

    prog_uint8_t const * _address;
};

template<typename Register>
class PwmPID : public PwmPIDBase
{
public:
    PwmPID(Register             reg,
           float const *        input,
           float const *        setpoint,
           prog_uint8_t const * address);

protected:
    void _dispatch(ms_t) override;

    Register _register;
};

template<typename Register>
PwmPID<Register>::PwmPID(Register             reg,
                         float const *        input,
                         float const *        setpoint,
                         prog_uint8_t const * address) :
    PwmPIDBase(input, setpoint, address), _register(reg)
{
}

template<typename Register>
void PwmPID<Register>::_dispatch(ms_t now_ms)
{
    PwmPIDBase::_dispatch(now_ms);

    /* Write output to register, scaling to max. */
    if (_output == _output)
    {
        _register = typename remove_reference<Register>::type(0xffff) * _output;
    }
}
