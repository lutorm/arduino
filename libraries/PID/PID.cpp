/**********************************************************************************************
 * Arduino PID Library - Version 1
 * by Brett Beauregard <br3ttb@gmail.com> brettbeauregard.com
 *
 * This Code is licensed under a Creative Commons Attribution-ShareAlike 3.0 Unported
 *License.
 **********************************************************************************************/

#include <Arduino.h>
#include <PID.h>

/*Constructor (...)*********************************************************
 *    The parameters specified here are those for for which we can't set up
 *    reliable defaults, so we need to have the user set them.
 ***************************************************************************/
PIDCore::PIDCore(const float * Input,
                 float *       Output,
                 const float * Setpoint,
                 bool          PositiveDirection)
{
    PIDCore::SetOutputLimits(0, 255);

    // By default, output goes NaN immediately with NaN input
    Timeout = 0;
    Ninval  = 0;

    PIDCore::SetDirection(PositiveDirection);

    inAuto     = false;
    myOutput   = Output;
    myInput    = Input;
    mySetpoint = Setpoint;
}

/* Compute() **********************************************************************
 *     This, as they say, is where the magic happens.  this function should be called
 *   every time "void loop()" executes.  the function will decide for itself whether a new
 *   pid Output needs to be computed
 **********************************************************************************/
void PIDCore::_update()
{
    if (!inAuto)
        return;

    const float input = *myInput;

    float output;
    if (isnan(input))
    {
        // NaN in input.
        Ninval++;
        if (Ninval >= Timeout)
        {
            // Timeout reached, set output to NaN or, if we have
            // feedforward, to the feedforward value
            auto const ff = feedForward();
            output = isnan(ff) ? input : ff;
            // prevent Ninval from wrapping on long periods of invalid inputs
            Ninval = Timeout;
        }
        else
        {
            // timeout not reached. keep output unchanged
            output = *myOutput;
        }
    }
    else
    {
        // valid input. update control loop
        Ninval = 0;

        // if we're using feed-forward, get the estimated output
        float      ff_bias = 0;
        auto const ff      = feedForward();
        if (!isnan(ff))
        {
            ff_bias = ff;
        }

        const float error  = *mySetpoint - input;
        const float dInput = isnan(lastInput) ? 0.0 : (input - lastInput);

        // Compute PID Output before adding to integral
        output = ff_bias + kp * error + ITerm - kd * dInput;

        // to prevent windup, we do not add the integral term if doing
        // so would bring the output *more* outside. on the other hand,
        // if it would bring the output *less* outside, we do allow it
        // to unwind
        // @FIXME: feedforward can bring integral way outside of limits, we should prevent that
        if (!(((output < outMin) && (error * ki < 0))
              || ((output > outMax) && (error * ki > 0))))
            ITerm += (ki * error);

        output = ff_bias + kp * error + ITerm - kd * dInput;
    }

    if (output > outMax)
        output = outMax;
    else if (output < outMin)
        output = outMin;
    *myOutput = output;

    /*Remember some variables for next time*/
    lastInput = input;
}

/* SetTunings(...)*************************************************************
 * This function allows the controller's dynamic performance to be adjusted.
 * it's called automatically from the constructor, but tunings can also
 * be adjusted on the fly during normal operation
 ******************************************************************************/
void PIDCore::SetTunings(float Kp, float Ki, float Kd)
{
    if (Kp < 0 || Ki < 0 || Kd < 0)
        return;

    float SampleTimeInSec = ((float)GetSampleTime()) / 1000;
    kp                    = Kp;
    ki                    = Ki * SampleTimeInSec;
    kd                    = Kd / SampleTimeInSec;

    if (!posDir)
    {
        kp = -kp;
        ki = -ki;
        kd = -kd;
    }
}

/* SetSampleTime(...) *********************************************************
 * sets the period, in Milliseconds, at which the calculation is performed
 ******************************************************************************/
void PIDCore::_SetSampleTime(ms_t NewSampleTime)
{
    if (NewSampleTime > 0)
    {
        float ratio = (float)NewSampleTime / (float)GetSampleTime();
        ki          = ki * ratio;
        kd          = kd / ratio;
    }
}

/* SetOutputLimits(...)****************************************************
 *     This function will be used far more often than SetInputLimits.  while
 *  the input to the controller will generally be in the 0-1023 range (which is
 *  the default already,)  the output will be a little different.  maybe they'll
 *  be doing a time window and will need 0-8000 or something.  or maybe they'll
 *  want to clamp it from 0-125.  who knows.  at any rate, that can all be done
 *  here.
 **************************************************************************/
void PIDCore::SetOutputLimits(float Min, float Max)
{
    if (Min >= Max)
        return;
    outMin = Min;
    outMax = Max;

    if (inAuto)
    {
        if (*myOutput > outMax)
            *myOutput = outMax;
        else if (*myOutput < outMin)
            *myOutput = outMin;

        if (ITerm > outMax)
            ITerm = outMax;
        else if (ITerm < outMin)
            ITerm = outMin;
    }
}

/* SetMode(...)****************************************************************
 * Allows the controller Mode to be set to manual (0) or Automatic (non-zero)
 * when the transition from manual to auto occurs, the controller is
 * automatically initialized
 ******************************************************************************/
void PIDCore::SetEngaged(bool newAuto, bool bumpless)
{
    if (newAuto && !inAuto)
    { /*we just went from manual to auto*/
        PIDCore::Initialize(bumpless);
    }
    inAuto = newAuto;
}

/* Initialize()****************************************************************
 *	If there is no feedforward, set the integral to ensure a
 *	bumpless transfer. Take care to not poison the integral if
 *	input is Nan. If there is a feedforward, we assume that takes
 *	precedence and the integral term is set to zero.
 ******************************************************************************/
void PIDCore::Initialize(bool bumpless)
{
    ITerm = 0;
    auto const error = isnan(*myInput) ? 0.0 : *mySetpoint - *myInput;
    auto ff = feedForward();
    if (isnan(ff))
    {
        ff = 0;
    }
    if (bumpless)
    {
        /* Clamp output to limits, in case it is outside. This ensures
         * we don't set the integral way outside the limits. */
        auto const clamped_output =
            *myOutput > outMax ? float(outMax)
                               : (*myOutput < outMin ? float(outMin) : *myOutput);

        ITerm = clamped_output - ff - error * kp;

        auto output = ff + kp * error + ITerm;
    }

    /* Update output immediately. */
    *myOutput = ff + kp * error + ITerm;

    lastInput = *myInput;
}

/* SetControllerDirection(...)*************************************************
 * The PID will either be connected to a DIRECT acting process (+Output leads
 * to +Input) or a REVERSE acting process(+Output leads to -Input.)  we need to
 * know which one, because otherwise we may increase the output when we should
 * be decreasing.  This is called from the constructor.
 ******************************************************************************/
void PIDCore::SetDirection(bool Direction)
{
    if (inAuto && Direction != posDir)
    {
        kp = -kp;
        ki = -ki;
        kd = -kd;
    }
    posDir = Direction;
}

void PID::Compute()
{
    unsigned long now        = millis();
    unsigned int  timeChange = (now - lastTime);
    // take care that we update when the millis counter wraps
    if ((timeChange >= SampleTime) || (now < lastTime))
    {
        _update();
        lastTime = now;
    }
}

PID::PID(const float * Input,
         float *       Output,
         const float * Setpoint,
         float         Kp,
         float         Ki,
         float         Kd,
         bool          PositiveDirection,
         const float * feedforward) :
    PIDCore(Input, Output, Setpoint, PositiveDirection),
    SampleTime(100),
    _feedForward(feedforward)
{
    SetTunings(Kp, Ki, Kd);

    lastTime = millis() - GetSampleTime();
}

void PID::SetSampleTime(ms_t NewSampleTime)
{
    if (NewSampleTime > 0)
    {
        _SetSampleTime(NewSampleTime);
        SampleTime = (unsigned long)NewSampleTime;
    }
}

PIDEvent::PIDEvent(const float * Input,
                   float *       Output,
                   const float * Setpoint,
                   bool          PositiveDirection) :
    Periodic(100), PIDCore(Input, Output, Setpoint, PositiveDirection)
{
}

void PIDEvent::set_period(ms_t NewSampleTime)
{
    if (NewSampleTime > 0)
    {
        _SetSampleTime(NewSampleTime);
        Periodic::set_period(NewSampleTime);
    }
}

void PIDEvent::_dispatch(ms_t)
{
    _update();
}
