
#ifndef __EventLoop_h
#define __EventLoop_h

#include <stdint.h>

typedef uint32_t ms_t;

/* The event class is the base class for everything that gets called
 * by the event loop */
class Event
{
public:
    
    virtual ~Event() {}

    virtual void dispatch(ms_t now) = 0;
};


class EventLoop
{
public:

    EventLoop();
    
    bool add_event(Event*);

    bool remove_event(Event*);

    /* Dispatches all events. */
    void dispatch(ms_t now);

    /* Return the max cycle time encountered. This is reset every
     * max_cycle_reset_time ms. */
    ms_t max_cycle_time() const { return _max_cycle_time; }
        
private:

    static uint8_t const max_events = 16;

    uint8_t _n_events;
    
    Event * _events[max_events];

    ms_t _last_now;
    ms_t _max_cycle_time;
    ms_t const _max_cycle_reset_time = 10000;
};

/* Define a global event loop. */
extern EventLoop event_loop;

/* Object representing a periodic event. */
class Periodic : public Event
{
public:
    
    Periodic(ms_t period = 0);

    void dispatch(ms_t now) override final;

    virtual void set_period(ms_t period) { _period = period; }

    ms_t period() const { return _period; }

    ms_t next_time() const { return _last + _period; }
    
protected:

    /* Implemented by the base classes. Called at the specified frequency. */
    virtual void _dispatch(ms_t now) = 0;

    ms_t _period;

    /* The last time the event triggered. */
    ms_t _last;
};

#endif
