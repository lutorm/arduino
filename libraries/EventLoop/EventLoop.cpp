
#include "EventLoop.h"
#include "debug.h"

EventLoop event_loop;

EventLoop::EventLoop() :
    _n_events(0),
    _max_cycle_time(0)
{
}

bool EventLoop::add_event(Event * event)
{
    if (_n_events == max_events)
    {
        sprint_pgm(PSTR("Unable to add event -- no room in table"));
        return false;
    }

    _events[_n_events++] = event;
    return true;
}

void EventLoop::dispatch(ms_t now)
{
    dppgm(3, "EventLoop dispatch: ");
    dpln(3, now);

    /* See if we crossed a cycle_reset_time and if so reset the max
     * cycle time. */
    if (_last_now/_max_cycle_reset_time != now/_max_cycle_reset_time)
    {
        _max_cycle_time = 0;
    }
    
    if (_last_now &&
        (now-_last_now > _max_cycle_time))
    {
        _max_cycle_time = now - _last_now;
    }
        
    _last_now = now;

    for (uint8_t i = 0;
         i < _n_events;
         ++i)
    {
        _events[i]->dispatch(now);
    }

}

bool EventLoop::remove_event(Event * event)
{
    uint8_t i = 0;
    while (i < _n_events &&
           _events[i] != event)
    {
        ++i;
    }

    if (i == _n_events)
    {
        return false;
    }

    /* Found it. Remove it by moving all lower ones up. */
    ++i;
    while (i < _n_events)
    {
        _events[i-1] = _events[i];
        ++i;
    }
    --_n_events;

    return true;
}

Periodic::Periodic(ms_t period) :
    _period(period),
    _last(0)
{
}

void Periodic::dispatch(ms_t now)
{
    if (now - _last >= _period)
    {
        _last += _period;
        _dispatch(now);
        
        //@todo warn if we can't keep up
    }
}

