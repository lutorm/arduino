/* Library for communicating with a Matrix Orbital display over a
   serial device. */

#pragma once

#include <Arduino.h>

/* Base class for Mtxorb class that doesn't depend on the display. */
class mtxorb_base
{
private:
    // functions to communicate with the display, which is the only thing
    // depending on the actual port used to communicate with it.
    virtual void write(byte) const = 0;
    virtual byte read() const      = 0;
    virtual int  available() const = 0;
    virtual void flush() const     = 0;
    virtual bool overflow() const  = 0;

    // virtual forwards for the print methods
    virtual void print_impl(const String &) const = 0;
    // virtual void print_impl(const char[]) const=0;
    virtual void print_impl(const char *) const                 = 0;
    virtual void print_impl(int c, int i = DEC) const           = 0;
    virtual void print_impl(unsigned int c, int i = DEC) const  = 0;
    virtual void print_impl(long c, int i = DEC) const          = 0;
    virtual void print_impl(unsigned long c, int i = DEC) const = 0;
    virtual void print_impl(double c, int i = 2) const          = 0;

    static const byte n_fans_ = 3;

    // last read time for rpms
    unsigned long rpmreadtime_;
    // rpm values for fans
    int rpm_[n_fans_];
    // divider values for fans
    int div_[n_fans_];
    // fan currently being updated
    byte cur_fan_;

    // reads a fan rpm packet from the display and updates the
    // appropriate rpm variable
    void read_drp_packet();
    void read_rpm_packet(byte);
    void read_1wire_packet(byte){};

protected:
    // send commands to the display
    void send_cmd(const char * str) const;
    void send_cmd(byte str) const;
    void send_cmd(byte c1, byte c2) const;
    void send_cmd(byte c1, byte c2, byte c3) const;

public:
    mtxorb_base();

    // redirect print methods to the virtual forwards
    void print(const String & msg) const;
    // void print(const char msg[]) const;
    void print(const char * msg) const;
    void print(int c, int i = DEC) const;
    void print(unsigned int c, int i = DEC) const;
    void print(long c, int i = DEC) const;
    void print(unsigned long c, int i = DEC) const;
    void print(double c, int i = 2) const;

    void auto_line_wrap(bool b) const;
    void auto_scroll(bool b) const;
    void set_cursor_pos(byte col, byte row) const;
    void cursor_home() const;
    void clear() const;
    void underline_cursor(bool b) const;
    void block_cursor(bool b) const;
    void cursor_left() const;
    void cursor_right() const;
    void move_cursor(char c) const;
    void set_gpo(byte output, bool b) const;
    void set_pwm(byte output, byte val) const;
    void save_pwm(byte output, byte val) const;
    void set_pwm_freq(byte freq) const;
    void save_pwm_freq(byte freq) const;

    void        init_wide_vertical_graph() const;
    static void init_wide_vertical_graph(char ** str, int * n);
    void        init_narrow_vertical_graph() const;
    static void init_narrow_vertical_graph(char ** str, int * n);
    void        init_horizontal_graph() const;
    void        draw_vertical_graph(byte col, byte height) const;
    void        draw_horizontal_graph(byte col, byte row, byte dir, byte len) const;
    void        set_contrast(byte c) const;
    void        save_contrast(byte c) const;
    void        set_backlight(bool b) const;
    void        set_brightness(byte br) const;
    void        save_brightness(byte br) const;
    void        set_startup_screen(const char * msg) const;
    byte        read_type() const;
    void        start_remember() const;
    void        stop_remember() const;

    // fan rpm reading functions

    // reads and updates the rpm if enough time has passed.  Note that
    // while the MO uses identifiers 1-3, we are using C and so use 0-2!
    void        update_rpms();
    void        set_fan_divider(byte i, int div) { div_[i] = div; };
    const int * rpm_pointer(byte i) const { return &rpm_[i]; };
};

/* The Mtxorb class represents a connected display. */
template<typename T_serial>
class Mtxorb : public mtxorb_base
{
private:
    // The Serial device the display is connected to (can be hardware or
    // software serial ports.
    T_serial & port_;

    virtual void write(byte b) const { port_.write(b); };
    virtual byte read() const { return port_.read(); };
    virtual int  available() const { return port_.available(); };
    virtual void flush() const { port_.flush(); };
    virtual bool overflow() const { return port_.overflow(); };

    // forward print methods to the port
    virtual void print_impl(const String & msg) const { port_.print(msg); };
    virtual void print_impl(const byte * str) const { port_.print((char *)str); };
    virtual void print_impl(const char msg[]) const { port_.print(msg); };
    virtual void print_impl(int c, int i = DEC) const { port_.print(c, i); };
    virtual void print_impl(unsigned int c, int i = DEC) const { port_.print(c, i); };
    virtual void print_impl(long c, int i = DEC) const { port_.print(c, i); };
    virtual void print_impl(unsigned long c, int i = DEC) const { port_.print(c, i); };
    virtual void print_impl(double c, int i = 2) const { port_.print(c, i); };

public:
    Mtxorb(T_serial & p) : port_(p){};
};
