#include <Mtxorb.h>
#include <avr/pgmspace.h>
#include <memory.h>

const byte mtxorb_base::n_fans_;

mtxorb_base::mtxorb_base() : rpmreadtime_(0), cur_fan_(n_fans_)
{
    rpm_[0] = -1;
    rpm_[1] = -1;
    rpm_[2] = -1;
    div_[0] = -1;
    div_[1] = -1;
    div_[2] = -1;
}

// send a command to the display
void mtxorb_base::send_cmd(const char * str) const
{
    write(byte(254));
    print(str);
};

// send a command to the display
void mtxorb_base::send_cmd(byte str) const
{
    write(byte(254));
    write(str);
};

void mtxorb_base::send_cmd(byte c1, byte c2) const
{
    write(byte(254));
    write(c1);
    write(c2);
};

void mtxorb_base::send_cmd(byte c1, byte c2, byte c3) const
{
    write(byte(254));
    write(c1);
    write(c2);
    write(c3);
};

// redirect print methods to the virtual forwards
void mtxorb_base::print(const String & msg) const
{
    print_impl(msg);
}
// void print(const char msg[]) const {
//   print_impl(msg); }
void mtxorb_base::print(const char * msg) const
{
    print_impl(msg);
}
void mtxorb_base::print(char c, int i) const
{
    print_impl(c, i);
}
void mtxorb_base::print(unsigned char c, int i) const
{
    print_impl(c, i);
}
void mtxorb_base::print(int c, int i) const
{
    print_impl(c, i);
}
void mtxorb_base::print(unsigned int c, int i) const
{
    print_impl(c, i);
}
void mtxorb_base::print(long c, int i) const
{
    print_impl(c, i);
}
void mtxorb_base::print(unsigned long c, int i) const
{
    print_impl(c, i);
}
void mtxorb_base::print(double c, int i) const
{
    print_impl(c, i);
}

void mtxorb_base::auto_line_wrap(bool b) const
{
    send_cmd(b ? byte(67) : byte(68));
}

void mtxorb_base::auto_scroll(bool b) const
{
    send_cmd(b ? byte(81) : byte(82));
}

void mtxorb_base::set_cursor_pos(byte col, byte row) const
{
    send_cmd(byte(71), col, row);
}

void mtxorb_base::cursor_home() const
{
    send_cmd(byte(72));
}

void mtxorb_base::clear() const
{
    send_cmd(byte(88));
}

void mtxorb_base::underline_cursor(bool b) const
{
    send_cmd(b ? byte(74) : byte(75));
}

void mtxorb_base::block_cursor(bool b) const
{
    send_cmd(b ? byte(83) : byte(84));
}

void mtxorb_base::cursor_left() const
{
    send_cmd(byte(76));
}

void mtxorb_base::cursor_right() const
{
    send_cmd(byte(77));
}

void mtxorb_base::move_cursor(char c) const
{
    // void (*movefun)() = (c>0) ? cursor_right : cursor_left;
    // for(; c!=0; c += (c>0) ? -1 : 1)
    //   movefun();
}

void mtxorb_base::set_gpo(byte output, bool b) const
{
    send_cmd(b ? byte(87) : byte(86), output);
}

void mtxorb_base::set_pwm(byte output, byte val) const
{
    send_cmd(byte(192), output, val);
}

void mtxorb_base::save_pwm(byte output, byte val) const
{
    send_cmd(byte(195), output, val);
}

void mtxorb_base::set_pwm_freq(byte freq) const
{
    send_cmd(byte(196), freq);
}

void mtxorb_base::save_pwm_freq(byte freq) const
{
    send_cmd(byte(197), freq);
}

void mtxorb_base::init_wide_vertical_graph() const
{
    send_cmd(byte(118));
}

// add the command to the string
void mtxorb_base::init_wide_vertical_graph(char ** str, int * n)
{
    if (*n < 2)
        return;
    (*str)[0] = 254;
    (*str)[1] = 118;
    (*str) += 2;
    (*n) -= 2;
}

void mtxorb_base::init_narrow_vertical_graph() const
{
    send_cmd(byte(115));
}

// add the command to the string
void mtxorb_base::init_narrow_vertical_graph(char ** str, int * n)
{
    if (*n < 2)
        return;
    (*str)[0] = 254;
    (*str)[1] = 115;
    (*str) += 2;
    (*n) -= 2;
}

void mtxorb_base::init_horizontal_graph() const
{
    send_cmd(byte(104));
}

// A vertical graph has the bars going vertical, starting from the
// bottom
void mtxorb_base::draw_vertical_graph(byte col, byte height) const
{
    send_cmd(byte(61), col, height);
}

void mtxorb_base::draw_horizontal_graph(byte col, byte row, byte dir, byte len) const
{
    char buf[5];
    buf[0] = 61;
    buf[1] = col;
    buf[2] = row;
    buf[3] = dir;
    buf[4] = len;
    send_cmd(buf);
}

void mtxorb_base::set_contrast(byte c) const
{
    send_cmd(byte(80), c);
}

void mtxorb_base::save_contrast(byte c) const
{
    send_cmd(byte(145), c);
}

void mtxorb_base::set_backlight(bool b) const
{
    if (b)
        send_cmd(byte(66), byte(0));
    else
        send_cmd(byte(70));
}

void mtxorb_base::set_brightness(byte br) const
{
    send_cmd(byte(153), br);
}

void mtxorb_base::save_brightness(byte br) const
{
    send_cmd(byte(152), br);
}

void mtxorb_base::set_startup_screen(const char * msg) const
{
    send_cmd(64);
    print(msg);
}

byte mtxorb_base::read_type() const
{
    send_cmd(55);
    while (available() < 1)
        delay(10);
    byte b = read();
    return b;
}

void mtxorb_base::start_remember() const
{
    send_cmd(byte(147), byte(1));
}

void mtxorb_base::stop_remember() const
{
    send_cmd(byte(147), byte(0));
}

void mtxorb_base::read_drp_packet()
{
    if (overflow())
        sprint_pgm(PSTR("Display line serial buffer overflow!\n\r"));

    // look for start of packet
    while (available())
        if (read() == 0x23)
            break;

    // found start code. test rest of bytes
    if (read() != 0x2a)
    {
        // sprint_pgm(PSTR("Did not find packet header\n\r"));
        return;
    }
    // We found a packet header

    const byte sz   = read() & 0x7f;
    const byte type = read();

    switch (type)
    {
    case 0x31:
        read_1wire_packet(sz);
        break;
    case 0x52:
        read_rpm_packet(sz);
        break;
    default:
        Serial.print(PSTR("Unknown DRP packet type "));
        Serial.println(type);
    }
}

/* the rpm packet consists of 3 bytes: fan, period_msb, period_lsb */
void mtxorb_base::read_rpm_packet(byte sz)
{
    if (sz != 3)
        return;

    const byte fan = read() - 1;
    if (fan >= n_fans_)
        return;
    const byte msb = read();
    const byte lsb = read();

    const unsigned int period = msb * 256 + lsb;
    if (period == 65535)
        rpm_[fan] = 0;
    else
        rpm_[fan] = 18750000UL / (period * div_[fan]);

    // sprint_pgm(PSTR(" rpm="));
    // Serial.println(rpm_[fan]);
}

void mtxorb_base::update_rpms()
{
    unsigned long now = millis();

    // we wait 3000 ms between each fan. that will ensure we
    // don't reread a single fan more often than 2s
    if (now - rpmreadtime_ < 3000)
    {
        // aint time to issue new read command
        return;
    }
    rpmreadtime_ = now;

    read_drp_packet();

    // advance to next fan
    cur_fan_++;
    if (cur_fan_ >= n_fans_)
        cur_fan_ = 0;

    // issue read command (to fan+1 since the display knows them by 1-3)
    send_cmd(byte(193), cur_fan_ + 1);
}
