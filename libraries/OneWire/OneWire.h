#pragma once

#include <Arduino.h>
#include <inttypes.h>

// you can exclude onewire_search by defining that to 0
#ifndef ONEWIRE_SEARCH
#define ONEWIRE_SEARCH 1
#endif

// You can exclude CRC checks altogether by defining this to 0
#ifndef ONEWIRE_CRC
#define ONEWIRE_CRC 1
#endif

// Select the table-lookup method of computing the 8-bit CRC
// by setting this to 1.  The lookup table no longer consumes
// limited RAM, but enlarges total code size by about 250 bytes
#ifndef ONEWIRE_CRC8_TABLE
#define ONEWIRE_CRC8_TABLE 0
#endif

// You can allow 16-bit CRC checks by defining this to 1
// (Note that ONEWIRE_CRC must also be 1.)
#ifndef ONEWIRE_CRC16
#define ONEWIRE_CRC16 0
#endif

#define FALSE 0
#define TRUE  1

/** This is an abstract base class for classes representing devices
    connected to the OneWire bus. They are notified about bus resets
    so they can determine whether they are still in overdrive mode or
    whether they can issue a resume command. The OneWire object
    maintains a linked list of devices connected to it for these
    notifications. */
class OneWireDev
{
private:
    friend class OneWire;
    OneWireDev * next_;

protected:
    virtual void reset_notify()           = 0;
    virtual void select_notify()          = 0;
    virtual void overdrive_reset_notify() = 0;
};

class OneWire
{
private:
    uint8_t            bitmask;
    volatile uint8_t * baseReg;

    OneWireDev * devs_;

#if ONEWIRE_SEARCH
    // global search state
    unsigned char ROM_NO[8];
    uint8_t       LastDiscrepancy;
    uint8_t       LastFamilyDiscrepancy;
    uint8_t       LastDeviceFlag;
#endif

    struct normal_timings
    {
        // one-wire standard-speed timings in ns
        static const long t_rl   = 5000;
        static const long t_msr  = 13000;
        static const long t_slot = 65000;
        static const long t_w1l  = 10000;
        static const long t_w0l  = 65000;
        static const long t_rec  = 5000;
        static const long t_rstl = 500000;
        static const long t_pdh  = 60000;
        static const long t_pdl  = 240000;
        static const long t_msp  = 80000;
    };

    struct overdrive_timings
    {
        // one-wire overdrive timings in ns
        static const long t_rl  = 1000;
        static const long t_msr = 1500;
        // slot time is 10us but due to processing we undercut the delay
        // an empirical 3us
        static const long t_slot = 7000;  // 10000;
        // w1l is 1us but we undercut to account for processing
        static const long t_w1l = 700;  // 1000;
        static const long t_w0l = 8000;
        // due to overhead when writing, we can never undercut t_rec
        static const long t_rec  = 0;  // 2000;
        static const long t_rstl = 55000;
        static const long t_pdh  = 7000;
        static const long t_pdl  = 27000;
        static const long t_msp  = 8000;
    };

    void select_addrsend(uint8_t const rom[8], bool overdrive = false);

public:
    OneWire(uint8_t pin);

    // Perform a 1-Wire reset cycle. Returns 1 if a device responds
    // with a presence pulse.  Returns 0 if there is no device or the
    // bus is shorted or otherwise held low for more than 250uS
    uint8_t reset(bool overdrive = false);

    // Issue a 1-Wire rom select command, you do the reset first.
    void select(uint8_t const rom[8], bool overdrive = false);
    void select_and_overdrive(uint8_t const rom[8], bool overdrive = false);

    // Issue a 1-Wire rom skip command, to address all on bus.
    void skip(bool overdrive = false);

    void skip_and_overdrive(bool overdrive = false);

    // Issue a 1-Wire resume command, to address last selected device
    void resume(bool overdrive = false);

    // Write a byte. If 'power' is one then the wire is held high at
    // the end for parasitically powered devices. You are responsible
    // for eventually depowering it by calling depower() or doing
    // another read or write.
    void write(uint8_t v, bool overdrive = false, bool power = false);

    // Read a byte.
    uint8_t read(bool overdrive = false);

    // Write a bit. The bus is always left powered at the end, see
    // note in write() about that.
    template<typename T_timing>
    void write_bit(uint8_t v, uint8_t loval, uint8_t hival);

    template<typename T_timing>
    void write_bit(uint8_t v);

    // Read a bit.
    template<typename T_timing>
    uint8_t read_bit();

    // Stop forcing power onto the bus. You only need to do this if
    // you used the 'power' flag to write() or used a write_bit() call
    // and aren't about to do another read or write. You would rather
    // not leave this powered if you don't have to, just in case
    // someone shorts your bus.
    void depower(void);

    void mark();

    void attach_device(OneWireDev * d);
    void detach_device(OneWireDev * d);

#if ONEWIRE_SEARCH
    // Clear the search state so that if will start from the beginning again.
    void reset_search();

    // Look for the next device. Returns 1 if a new address has been
    // returned. A zero might mean that the bus is shorted, there are
    // no devices, or you have already retrieved all of them.  It
    // might be a good idea to check the CRC to make sure you didn't
    // get garbage.  The order is deterministic. You will always get
    // the same devices in the same order.
    uint8_t search(uint8_t * newAddr, bool overdrive = false);
#endif

#if ONEWIRE_CRC
    // Compute a Dallas Semiconductor 8 bit CRC, these are used in the
    // ROM and scratchpad registers.
    static uint8_t crc8(uint8_t * addr, uint8_t len);

#if ONEWIRE_CRC16
    // Compute a Dallas Semiconductor 16 bit CRC. Maybe. I don't have
    // any devices that use this so this might be wrong. I just copied
    // it from their sample code.
    static unsigned short crc16(unsigned short * data, unsigned short len);
#endif
#endif
};

template<uint64_t N>
struct nopper
{
    __attribute__((always_inline)) static void delay()
    {
        __asm__ volatile("nop");
        nopper<N - 1>::delay();
    };
};

template<>
struct nopper<0>
{
    __attribute__((always_inline)) static void delay(){};
};

template<uint64_t NS, bool use_nops>
struct delayer
{
    __attribute__((always_inline)) static void delay()
    {
        const uint64_t cycles =
            NS * F_CPU / 1000000000L + (NS * F_CPU % 1000000000L != 0 ? 1 : 0);
#ifdef __BUILTIN_AVR_DELAY_CYCLES
        __builtin_avr_delay_cycles(cycles);
#else
        nopper<cycles>::delay();
#endif
    };
};

template<uint64_t NS>
struct delayer<NS, false>
{
    __attribute__((always_inline)) static void delay()
    {
        const long us = NS / 1000 + (NS % 1000 != 0 ? 1 : 0);
        delayMicroseconds(us);
    };
};

// Handy function for delaying a fixed number of nanoseconds. For very
// short delays it will be instantiated as a number of nops, while for
// longer delays it falls back to delayMicroseconds.
template<uint64_t NS>
__attribute__((always_inline)) void delayNanoseconds()
{
    delayer < NS, NS<3000>::delay();
}
