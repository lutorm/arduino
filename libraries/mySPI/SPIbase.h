/* Patrik Jonsson 2012 added an abstract base class for SPI busses.
 */

#ifndef __SPIbase__
#define __SPIbase__

#include <stdint.h>

#define SPI_MODE0 0x00
#define SPI_MODE1 0x04
#define SPI_MODE2 0x08
#define SPI_MODE3 0x0C


/** Abstract base class for SPI busses. This includes the native
    Arduino SPI capability as well as the SPI bus attached to a DS2408
    1-wire switch. */
class SPIbus {
public:
  virtual ~SPIbus() {};

  virtual char transfer(char)=0;
  virtual uint8_t transfer(uint8_t)=0;
  virtual int transfer(int)=0;
  virtual uint16_t transfer(uint16_t)=0;

  virtual void setBitOrder(uint8_t)=0;
  virtual void setDataMode(uint8_t)=0;
};

#endif
