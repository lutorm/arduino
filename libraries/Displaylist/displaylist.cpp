#include "displaylist.h"
#include <Arduino.h>
#include <math.h>
#include <memory.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

d_string::d_string(const char * c)
{
    s_ = strdup(c);
};

d_string::~d_string()
{
    free((void *)s_);
}

void d_string_base::make_output(char ** str, int * n, const char * mystr)
{
    int l = strlcpy(*str, mystr, *n);
    if (l > *n)
        l = *n;
    *str += l;
    *n -= l;
}

void d_string::output(char ** str, int * n) const
{
    make_output(str, n, s_);
}

d_pstring::d_pstring(const prog_char * s) : s_(s){};

void d_pstring::output(char ** str, int * n) const
{
    // we strlcpy the pgmspace string directly into the buffer
    int l = strlcpy_P(*str, s_, *n);
    if (l > *n)
        l = *n;
    *str += l;
    *n -= l;
}

d_varbase::d_varbase(const void * v, const prog_char * f) : val_(v), fmt_(f) {}

d_varbase::~d_varbase() {}

void d_varbase::output(char ** str, int * n) const
{
    // first we need to copy the format string into ram so we can use it
    const int fmtlen = 7;
    char      fmt[fmtlen];
    size_t    ncpy = strlcpy_P(fmt, fmt_, fmtlen);
    if (ncpy >= fmtlen)
    {
        **str = 'T';
        *str++;
        *n--;
    }

    // pn is the number of characters, including \0, that WOULD have been
    // written
    int pn = printf_it(*str, *n, fmt);
    if (pn > *n)
        pn = *n;
    *str += pn;
    *n -= pn;
}

d_var<float16>::d_var(const float16 * v, const char * f) :
    d_varbase(reinterpret_cast<const void *>(v), f)
{
}

int d_var<float16>::printf_it(char * str, int n, const char * fmt) const
{
    // cast the value pointer to our type
    const int pn = snprintf(str, n, fmt, float(*reinterpret_cast<const float16 *>(val_)));
    return pn;
}

d_hist::d_hist(hist_base * h, byte l) : line_(l), h_(h){};

// f is the value between 0-1, n the number of lines, and l the line
// we want the data for. Returns the correct character for the
// bar. While the bar graph characters are 1-7, 0 doesn't seem to do
// anything and the full bar is 0xff, so we make the appropriate
// substitions.
byte d_hist::get_hist_char(float f, byte n, byte l)
{
    if (isnan(f))
    {
        // NaN point.
        return 'X';
    }

    const byte v = byte(floor(constrain(n * f - l, 0.0f, 1.0f) * plotres_));
    if (v == 0)
        return 32;
    if (v == 8)
        return 0xff;
    return v;
}

void d_hist::output(char ** str, int * n) const
{
    // the display graph characters are 0 -- plotres_.  Except that 0
    // doesn't seem to erase what's there before... so I guess it needs
    // to be replaced with a 32

    // put the graph init chars in the string (this means we need more
    // chars than are in the display)
    mtxorb_base::init_wide_vertical_graph(str, n);
    for (int i = 0; i < h_->n(); ++i)
    {
        if (*n == 0)
            break;
        **str = get_hist_char(h_->point(i), n_lines_, line_);

        (*str)++;
        (*n)--;
    }
}

void displayline_base::make_output(char * str, int n) const
{
    int       left = n;
    char *    s    = str;
    const int ni   = n_items();
    for (int i = 0; i < ni; ++i)
    {
        if (get_item(i))
            get_item(i)->output(&s, &left);
    }

    // fill rest with blanks
    memset(s, ' ', left);
    str[n] = 0;
}

void displayline_base::output_str(char * c, int n) const
{
    make_output(c, n);
}

displaylist::displaylist(int fps, int page_period, byte backlight, byte contrast) :
    backlight_(backlight),
    contrast_(contrast),
    first_(0),
    last_(0),
    cur_(0),
    fperiod_(1000 / fps),
    frames_per_page_(page_period / fperiod_),
    utime_(0),
    f_counter_(frames_per_page_){};

void displaylist::append(displayline_base * l)
{
    if (!last_)
    {
        first_ = last_ = l;
        cur_           = first_;
    }
    else
    {
        last_->append(l);
        last_ = l;
    }
}

bool displaylist::time_for_update()
{
    unsigned long now = millis();
    if (utime_ > now)
    {
        // aint time yet
        return false;
    }

    // update time to next frame
    utime_ = now + fperiod_;

    if (f_counter_-- == 0)
    {
        f_counter_ = frames_per_page_;
        cur_       = get_next();
        cur_       = get_next();
    }

    return true;
}

displayline_base * displaylist::get_next() const
{
    if (cur_ == last_)
        return first_;
    else
        return cur_->next();
}

void displaylist::update_serial()
{
    if (!time_for_update())
    {
        return;
    }

    const int maxlen = 30;
    char      buf[maxlen];

    cur_->output_str(buf, maxlen);
    sprint_pgm(PSTR("\n\rL1: "));
    Serial.println(buf);

    get_next()->output_str(buf, maxlen);
    sprint_pgm(PSTR("L2: "));
    Serial.println(buf);
}

float displaylist::transition() const
{
    // if we are on the last or first frame, it's time for transition
    if ((f_counter_ == 0) || (f_counter_ == frames_per_page_))
    {
        unsigned long now = millis();
        // fractional time to page switch, 1->0 before, 0->-1 after
        float f =
            float(int(utime_ - now) - ((f_counter_ == frames_per_page_) ? fperiod_ : 0))
            / fperiod_;
        f = constrain(f, -1, 1);

        return f;
    }

    // no transition
    return NAN;
}
