#pragma once

#include "Mtxorb.h"
#include "float16.h"
#include "hist.h"
#include "memory.h"
#include <avr/pgmspace.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

/* The d_item class is the baseclass for all objects that we can put
   into a display line. They know how to output themselves to a
   string. */
class d_item
{
public:
    virtual ~d_item(){};

    // output the item to the string c, which has n characters
    // available. Updates c and n to point *to* the terminating null
    // after the output has been added
    virtual void output(char **, int *) const = 0;
};

class d_string_base : public d_item
{
protected:
    static void make_output(char ** str, int * n, const char * mystr);
};

// represents a dynamically allocated string
class d_string : public d_string_base
{
private:
    char * s_;

public:
    d_string(const char * c);
    ~d_string();

    virtual void output(char ** str, int * n) const;
};

// represents a string in program memory
class d_pstring : public d_string_base
{
private:
    const prog_char * s_;

public:
    d_pstring(const prog_char *);

    virtual void output(char ** str, int * n) const;
};

// represents a fixed-length string
template<int N>
class d_fstring : public d_string_base
{
private:
    char s_[N];

public:
    d_fstring(const char * c);

    virtual void output(char ** str, int * n) const;
};

template<int N>
d_fstring<N>::d_fstring(const char * c)
{
    strncpy(s_, c, N - 1);
    // make sure we are null-terminated
    s_[N - 1] = 0;
};

template<int N>
void d_fstring<N>::output(char ** str, int * n) const
{
    make_output(str, n, s_);
}

// represents a variable in memory. baseclass uses void* to avoid
// duplicating code
class d_varbase : public d_item
{
protected:
    // format string
    const prog_char * fmt_;
    // pointer to the variable to output
    const void * const val_;

    virtual int printf_it(char *, int, const char *) const = 0;

public:
    d_varbase(const void * v, const prog_char *);
    ~d_varbase();

    void output(char **, int *) const;
};

template<typename T>
class d_var : public d_varbase
{
private:
    virtual int printf_it(char *, int, const char *) const;

public:
    d_var(const T *, const char *);
};

template<typename T>
d_var<T>::d_var(const T * v, const char * f) :
    d_varbase(reinterpret_cast<const void *>(v), f)
{
}

template<typename T>
int d_var<T>::printf_it(char * str, int n, const char * fmt) const
{
    // cast the value pointer to our type
    const int pn = snprintf(str, n, fmt, *reinterpret_cast<const T *>(val_));
    return pn;
}

template<>
class d_var<float16> : public d_varbase
{
private:
    virtual int printf_it(char *, int, const char *) const;

public:
    d_var(const float16 *, const char *);
};

// represents a 2-line hist plot
class d_hist : public d_item
{
private:
    // number of lines in the plot
    static const byte n_lines_ = 2;
    // the resolution of one line in the plot
    static const byte plotres_ = 8;

    // for multiline plots, the line to display
    byte line_;
    // pointer to the hist object
    hist_base * h_;

public:
    d_hist(hist_base *, byte);

    // calculates the fractional line height of a multiline display
    static byte get_hist_char(float v, byte n, byte l);

    virtual void output(char ** str, int * n) const;
};

// the displayline class represents one line on the display. It holds
// a number of d_items that are all output on the same line. It's also
// a linked list to the next line.
class displayline_base
{

    displayline_base * next_;

    void make_output(char *, int) const;

    virtual d_item * get_item(byte i) const = 0;
    virtual int      n_items() const        = 0;

public:
    displayline_base() : next_(0){};

    // output the line to the specified display, with max n characters
    template<typename T_disp, int N>
    void output(T_disp &) const;

    // return output string
    void output_str(char *, int) const;

    void               append(displayline_base * next) { next_ = next; };
    displayline_base * next() const { return next_; };
};

template<typename T_disp, int N>
void displayline_base::output(T_disp & d) const
{
    char str[N + 1];
    make_output(str, N);
    d.print(str);
}

// we can have lines with different number of items, so to save ram we
// make that a template parameter and only use as many as we need
template<int N_items>
class displayline : public displayline_base
{
private:
    d_item * item_[N_items];

    virtual d_item * get_item(byte i) const
    {
        if (i < N_items)
            return item_[i];
        else
            return 0;
    };
    virtual int n_items() const { return N_items; };

public:
    displayline(d_item * = 0,
                d_item * = 0,
                d_item * = 0,
                d_item * = 0,
                d_item * = 0,
                d_item * = 0);
    ~displayline();
};

// TODO: constructor should only accept N_items items
template<int N_items>
displayline<N_items>::displayline(
    d_item * a, d_item * b, d_item * c, d_item * d, d_item * e, d_item * f)
{
    item_[0] = a;
    if (N_items >= 2)
        item_[1] = b;
    if (N_items >= 3)
        item_[2] = c;
    if (N_items >= 4)
        item_[3] = d;
    if (N_items >= 5)
        item_[4] = e;
    if (N_items >= 6)
        item_[5] = f;
}

template<int N_items>
displayline<N_items>::~displayline()
{
    for (int i = 0; i < N_items; ++i)
        delete item_[i];
}

// the displaylist keeps a series of display lines and outputs them
// sequentially to the display
class displaylist
{
private:
    // values for the display
    byte backlight_, contrast_;

    // the linked list of lines to display
    displayline_base *first_, *last_, *cur_;

    // frame update period
    int fperiod_;
    // number of frames per page
    int frames_per_page_;
    // time for next frame update
    unsigned long utime_;
    // number of frames left for current page
    int f_counter_;

    /* returns the number of ticks until the next screen update. Updates
       the frame counter, and if it's time to switch to the next page,
       updates cur_. */
    bool time_for_update();

    // update cur_ to next line. wraps when it gets to the end
    displayline_base * get_next() const;

    float transition() const;

    template<typename T_disp>
    void contrast_transition(T_disp & d);

    template<typename T_disp>
    void backlight_transition(T_disp & d);

public:
    displaylist(int, int, byte, byte);

    void append(displayline_base *);

    template<typename T_disp, int N>
    void update(T_disp &);

    void update_serial();
};

template<typename T_disp, int N>
void displaylist::update(T_disp & d)
{
    contrast_transition(d);
    // backlight_transition(d);

    if (!time_for_update())
        return;

    d.auto_line_wrap(false);
    d.set_backlight(true);
    d.cursor_home();
    cur_->output<T_disp, N>(d);

    d.set_cursor_pos(1, 2);
    get_next()->output<T_disp, N>(d);
}

template<typename T_disp>
void displaylist::contrast_transition(T_disp & d)
{
    float transval = transition();
    if (transval == transval)
    {
        d.set_contrast(abs(transval) * contrast_);
        d.set_brightness(backlight_);
    }
}

template<typename T_disp>
void displaylist::backlight_transition(T_disp & d)
{
    float transval = transition();
    if (transval == transval)
    {
        d.set_contrast(contrast_);
        d.set_brightness(abs(transval) * backlight_);
    }
}
