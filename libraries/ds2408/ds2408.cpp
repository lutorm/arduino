#include "ds2408.h"
#include "memory.h"
#include "onewiretemp.h"

ds2408::ds2408(OneWire * w,
               uint8_t   spi_cs_pio,
               uint8_t   spi_mosi_pio,
               uint8_t   spi_clk_pio) :
    flags_(0x01),
    spi_cs_(1 << spi_cs_pio),
    spi_mosi_(1 << spi_mosi_pio),
    spi_clk_(1 << spi_clk_pio),
    pio_output_(0xff),
    pio_state_(0),
    pio_activity_(0),
    w_(w)
{
    w_->attach_device(this);
    rom_[0] = 0;
    // set_control_register(false, false, true);
    // set_output(0xff);
    // clear_activity_latches();
    // update_pios();
}

ds2408::~ds2408(){};

void ds2408::set_control_register(bool pls, bool ct, bool ros)
{
    reset();
    select();
    write(0xcc);
    write(0x8d);
    write(0x00);
    write((pls ? 0x01 : 0x00) | (ct ? 0x02 : 0x00) | (ros ? 0x04 : 0x00));
    reset();
}

void ds2408::select()
{
    if (rom_[0] == 0)
    {
        if (use_overdrive())
        {
            w_->skip_and_overdrive(in_overdrive());
            flags_ |= 0x04;
        }
        else
            w_->skip(in_overdrive());
    }
    else if (selected())
        w_->resume(in_overdrive());
    else
    {
        if (use_overdrive())
        {
            w_->select_and_overdrive(rom_, in_overdrive());
            flags_ |= 0x04;
        }
        else
            w_->select(rom_, in_overdrive());
        flags_ |= 0x02;
    }
}

/** Sets the device in channel_access_mode, if not already. */
void ds2408::channel_access_write()
{
    if (!in_channel_write())
    {
        // Serial.println("DS2408 entering channel access write");
        reset();
        select();
        write(0x5a);
        flags_ |= 0x08;
    }
}

/** Performs one loop of the channel-access write, writing pio_output
    and updating pio_state if successful. */
bool ds2408::channel_access_write_loop()
{
    write(pio_output_);
    write(~pio_output_);
    uint8_t result = read();
    if (result == 0xaa)
    {
        pio_state_ = read();
    }
    else
    {
        Serial.print("Channel access write failed, expected 0xAA, got ");
        Serial.println(result, HEX);
        flags_ &= ~0x08;
    }

    return result == 0xaa;
}

bool ds2408::set_output(uint8_t output, bool state)
{
    channel_access_write();

    pio_output_ &= ~(1 << output);
    if (state)
        pio_output_ |= (1 << output);

    bool success = channel_access_write_loop();
    return success;
}

bool ds2408::set_output(uint8_t state)
{
    channel_access_write();

    uint8_t mask;
    if (use_spi())
        mask = spi_cs_ | spi_mosi_ | spi_clk_;
    else
        mask = 0x00;

    pio_output_ &= mask;
    pio_output_ |= state & ~mask;

    bool success = channel_access_write_loop();
    return success;
}

void ds2408::update_pios()
{
    reset();
    select();
    write(0xf0);
    write(0x88);
    write(0x00);
    pio_state_     = read();
    uint8_t output = read();
    if (output != pio_output_)
    {
        Serial.print("DS2408 output latch mismatch, read ");
        Serial.print(output, HEX);
        Serial.print(" expected ");
        Serial.println(pio_output_, HEX);
        pio_output_ = output;
    }

    pio_activity_ = read();
    // we should do a full read and check CRC16 here
    reset();
}

bool ds2408::clear_activity_latches()
{
    reset();
    select();
    write(0xc3);
    uint8_t result = read();
    if (result == 0xaa)
    {
        pio_activity_ = 0x00;
    }
    else
        Serial.println("Clear latches failed");

    reset();
    return result == 0xaa;
}

bool ds2408::write_spi(char * data, int n)
{
    /*
    Serial.print("SPI send ");
    Serial.print(n, DEC);
    Serial.println(" bytes: ");
    printhex((uint8_t*)data, n);
    Serial.println();
    */

    channel_access_write();

    // transaction start - lower the CS pin
    pio_output_ &= ~spi_cs_;
    if (!channel_access_write_loop())
    {
        Serial.println("SPI send failed at CS set");
        return false;
    }

    // this is the mask to get the current data bit value
    const uint8_t bitmask = lsb_first() ? 0x01 : 0x80;

    // loop over bytes to write. AVRs are little-endian, so the first
    // byte is the LSB.
    for (int8_t bb = 0; bb < n; ++bb)
    {
        const int8_t byt     = lsb_first() ? bb : n - 1 - bb;
        uint8_t      curbyte = data[byt];
        // loop over bits to write.
        for (uint8_t b = 0; b < 8; ++b)
        {
            // we want to know if the current bit being written is different
            // from the current state.
            const uint8_t new_bit = curbyte & bitmask;
            const uint8_t old_bit = pio_output_ & spi_mosi_ ? bitmask : 0x00;

            /*
            Serial.print(byt,HEX);
            Serial.print(" ");
            Serial.print(curbyte,HEX);
            Serial.print(" ");
            Serial.println(new_bit>>(lsb_first()?0:7),HEX);
            */

            // if this bit is different from the currently written, toggle
            // the mosi pin. Otherwise toggle the clk pin
            if (new_bit ^ old_bit)
                pio_output_ ^= spi_mosi_;
            else
                // if xor is zero, they are the same and we toggle clk
                pio_output_ ^= spi_clk_;

            if (!channel_access_write_loop())
            {
                Serial.print("SPI send failed at bit ");
                Serial.println(bb * 8 + b, DEC);
                return false;
            }

            if (lsb_first())
                curbyte >>= 1;
            else
                curbyte <<= 1;
        }
    }

    // transaction end - raise CS
    pio_output_ |= spi_cs_;
    if (!channel_access_write_loop())
    {
        Serial.println("SPI send failed at CS clear");
        return false;
    }

    return true;
}

void ds2408::setBitOrder(uint8_t order)
{
    if (order == LSBFIRST)
        flags_ |= 0x40;
    else
        flags_ &= ~0x40;
};

void ds2408::setDataMode(uint8_t mode)
{
    if (mode != SPI_MODE0)
        Serial.println("DS2408 only supports SPI mode 0");
};

char ds2408::transfer(char data)
{
    return write_spi((char *)&data, sizeof(data));
};

uint8_t ds2408::transfer(uint8_t data)
{
    return write_spi((char *)&data, sizeof(data));
};

int ds2408::transfer(int data)
{
    return write_spi((char *)&data, sizeof(data));
};

uint16_t ds2408::transfer(uint16_t data)
{
    return write_spi((char *)&data, sizeof(data));
};
