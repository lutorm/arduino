#pragma once

#include "OneWire.h"
#include "SPIbase.h"
#include <Arduino.h>

/** This class interfaces with a Maxim DS2408 1-wire switch. It can
    read and write the PIOs, but can also act as an SPI master when
    connected as described in
    http://www.maxim-ic.com/app-notes/index.mvp/id/4505, where the SPI
    CLK signal is created with a series of XOR gates.
 */
class ds2408 : public OneWireDev, public SPIbus
{
    friend class OneWire;

    /// Flags to keep track of the switch mode, based on this behavior
    /// and bus reset/selects.
    uint8_t flags_;

    /// The PIO connected to the spi CS signal
    uint8_t spi_cs_;
    /// The PIO connected to the SPI MOSI signal
    uint8_t spi_mosi_;
    /** The PIO connected to the line used to generate a CLK transition
        if two consecutive MOSI bits are identical. */
    uint8_t spi_clk_;

    /** The current PIO state to write. We need to save the desired state
        since we have to update the entire PIO state register when we
        want to change one output. */
    uint8_t pio_output_;

    /// The PIO state most recently read.
    uint8_t pio_state_;

    /// The PIO activity latch register most recently read.
    uint8_t pio_activity_;

    /// The onewire bus the switch is connected to.
    OneWire * w_;

    /** The address of the switch. If the first byte is 0, the switch is
        assumed to be the only device on the bus and a Skip ROM will be
        used to address it. */
    uint8_t rom_[8];

    /// A standard-speed reset resets the device and puts it in standard speed.
    virtual void reset_notify()
    {
        // Serial.println("DS2408 reset notify");
        flags_ &= ~0x1c;
    };
    /// If a device select is used, we are no longer in selected mode.
    virtual void select_notify()
    {
        // Serial.println("DS2408 select notify");
        flags_ &= ~0x02;
    };
    /// An overdrive reset resets the device but not the comm mode
    virtual void overdrive_reset_notify()
    {
        // Serial.println("DS2408 overdrive reset notify");
        flags_ &= ~0x18;
    };

    // State getters

    /// Returns true if SPI I/O is enabled.
    bool use_spi() const { return flags_ & 0x01; };
    /// Returns true if the device is the last selected device on the bus.
    bool selected() const { return flags_ & 0x02; };
    /// Returns true if the device is in overdrive mode.
    bool in_overdrive() const { return flags_ & 0x04; };
    /// Returns true if the device is in channel write mode.
    bool in_channel_write() const { return flags_ & 0x08; };
    /// Returns true if the device is in channel read mode.
    bool in_channel_read() const { return flags_ & 0x10; };
    /// Returns true if we prefer overdrive mode.
    bool use_overdrive() const { return flags_ & 0x20; };
    /// Returns true if SPI data should be transferred LSB first
    bool lsb_first() const { return flags_ & 0x40; };

    void    write(uint8_t v) { w_->write(v, in_overdrive()); };
    uint8_t read() { return w_->read(in_overdrive()); };
    void    reset() { w_->reset(in_overdrive()); };

    void select();
    bool channel_access_write_loop();
    void channel_access_write();

    /** Writes n bytes to the SPI bus connected to the ds2408. This is
        an internal implementation for the virtual functions. */
    bool write_spi(char * data, int n);

public:
    /** Constructor sets SPI pins. It also clears the latch activity
        register and sets all outputs high (ie off). */
    ds2408(OneWire * w, uint8_t, uint8_t, uint8_t);

    ~ds2408();

    /// Tells the device we should use overdrive
    void set_overdrive() { flags_ |= 0x20; };

    /// Sets a specific output to a specific value. Returns true if
    /// write was successful.
    bool set_output(uint8_t output, bool state);

    /// Sets the entire output vector to the specified value. The SPI
    /// pins are not affected.
    bool set_output(uint8_t state);

    /** Issues a new read to update the PIO registers. */
    void update_pios();

    /** Returns the contents of the PIO logic state register. This
        reflects the last value read. To get a current value, call
        read_pios(). */
    uint8_t pio_state() { return pio_state_; };

    /// Reads the activity latch register
    uint8_t activity_state() { return pio_activity_; }

    bool clear_activity_latches();

    /// Sets the control register
    void set_control_register(bool pls, bool ct, bool ros);

    virtual void setBitOrder(uint8_t order);
    virtual void setDataMode(uint8_t mode);

    virtual char     transfer(char data);
    virtual uint8_t  transfer(uint8_t data);
    virtual int      transfer(int data);
    virtual uint16_t transfer(uint16_t data);
};
