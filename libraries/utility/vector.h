#pragma once

template<typename>
class Vector;

/* A dynamically sized array, like a bare-bones version of
   std::vector. From Patrik's SpaceX hiring coding test... :-) */
template<typename T>
class Vector
{

    // pointer to beginning of vector and storage
    T * begin_;

    // pointer to end of vector
    T * end_;

    // pointer to the end of the allocated storage (one past last legal value)
    T * eos_;

public:
    Vector() : begin_(nullptr), end_(nullptr), eos_(nullptr) {}

    Vector(size_t n) : begin_(nullptr), end_(nullptr), eos_(nullptr)
    {
        reallocate(n, false);
        end_ = begin_ + n;
    }

    Vector(size_t n, const T & val) : begin_(nullptr), end_(nullptr), eos_(nullptr)
    {
        assign(n, val);
    }

    Vector(const Vector & rhs) : begin_(nullptr), end_(nullptr), eos_(nullptr)
    {
        *this = rhs;
    }

    ~Vector() { delete[] begin_; }

    Vector & operator=(const Vector & rhs)
    {
        if (rhs.size() > capacity())
            reallocate(rhs.size(), false);
        end_ = begin_;
        for (size_t i = 0; i < rhs.size(); ++i)
            *(end_++) = rhs[i];
        return *this;
    }

    size_t size() const { return end_ - begin_; }
    size_t capacity() const { return eos_ - begin_; }
    void   reserve(size_t n)
    {
        if (n > capacity())
            reallocate(n, true);
    }

    // reallocate storage to fit n elements, copying existing elements
    // if requested
    void reallocate(size_t n, bool copy);

    // set the vector state to n elements containing val, overwriting
    // existing content
    void assign(size_t n, const T & val)
    {
        if (n > capacity())
            reallocate(n, false);
        end_ = begin_;
        for (size_t i = 0; i < n; ++i)
            *(end_++) = val;
    }

    void push_back(const T & val)
    {
        if (end_ >= eos_)
            reallocate(2 * capacity() + 1, true);
        *(end_++) = val;
    }

    void pop_back() { --end_; }

    T &       front() { return *begin_; }
    const T & front() const { return *begin_; }

    T &       back() { return *(end_ - 1); }
    const T & back() const { return *(end_ - 1); }

    T &       operator[](size_t i) { return begin_[i]; }
    const T & operator[](size_t i) const { return begin_[i]; }

    T *       begin() { return begin_; }
    const T * begin() const { return begin_; }
    T *       end() { return end_; }
    const T * end() const { return end_; }
};

template<typename T>
void Vector<T>::reallocate(size_t n, bool copy)
{
    assert(n == 0 || n > capacity());

    T * newbegin = new T[n];
    eos_         = newbegin + n;

    if (copy)
    {
        for (size_t i = 0; i != size(); ++i)
            newbegin[i] = begin_[i];
        end_ = newbegin + (end_ - begin_);
    }
    else
        end_ = newbegin;

    // delete old storage
    delete[] begin_;
    begin_ = newbegin;
}
