#pragma once

#include <avr/pgmspace.h>

// a grab-bag of random stuff

// Convert Fahrenheit to Celcius.
inline float f2c(float f)
{
    return (f - 32.) * (5. / 9);
}

/* Interpolate a table, stored in pgm memory. */
float interpolate(float                 x,
                  PROGMEM float const * x_table,
                  PROGMEM float const * y_table,
                  size_t                n);
