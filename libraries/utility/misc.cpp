#include "misc.h"

/* Interpolate a table, stored in pgm memory. */
float interpolate(float                 x,
                  PROGMEM float const * x_table,
                  PROGMEM float const * y_table,
                  size_t                n)
{
    int i;

    /* limiting cases. extrapolate. */
    if (x < pgm_read_float_near(x_table + 0))
    {
        i = 1;
    }
    else if (x > pgm_read_float_near(x_table + n - 1))
    {
        i = n - 1;
    }
    else
    {
        /* linear search for now. it's a short table... */
        i = 0;
        while ((x > pgm_read_float_near(x_table + i)) && (i < n - 1))
        {
            ++i;
        }
        /* Come out here with x between i-1 and i in the x-table. */
    }

    /* apply linear relation between x_table[i-1] and
     * x_table[i]. This will automatically extrapolate outside
     * of the table region. */
    int16_t const ymax = pgm_read_float_near(y_table + i);
    int16_t const ymin = pgm_read_float_near(y_table + i - 1);
    int16_t const xmax = pgm_read_float_near(x_table + i);
    int16_t const xmin = pgm_read_float_near(x_table + i - 1);

    int16_t const y = 1.0 * (ymax - ymin) / (xmax - xmin) * (x - xmin) + ymin;

    return y;
}
