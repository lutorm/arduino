#pragma once

#include <inttypes.h>

class xbee
{
public:
    int     xbee_wait;
    uint8_t reset_pin;
    uint8_t sleep_pin;
    uint8_t pwr_led;
    uint8_t comm_led;

    xbee(int w, uint8_t rp, uint8_t sp, uint8_t pl = -1, uint8_t cl = -1);

    void digitalWrite(uint8_t, uint8_t);

    void command(const char * cmd);
    int  response(char * buf, int n);
    bool commandmode();
    void read_serial_and_firmware(char snbuf[17], char fwbuf[10]);

    void hardware_reset();
    void software_reset();
};
