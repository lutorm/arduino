#include "xbee.h"
#include "Arduino.h"

xbee::xbee(int w, uint8_t rp, uint8_t sp, uint8_t pl, uint8_t cl) :
    xbee_wait(w), reset_pin(rp), sleep_pin(sp), pwr_led(pl), comm_led(cl){};

void xbee::digitalWrite(uint8_t pin, uint8_t mode)
{
    if (pin >= 0)
        ::digitalWrite(pin, mode);
}

// Issue an Xbee command. Just outputs the characters to the serial
// line one by one, with a pause between.
void xbee::command(const char * cmd)
{
    int i = 0;
    Serial.flush();
    while (cmd[i] != 0)
    {
        Serial.write(cmd[i++]);
        delay(100);
    }
}

// Reads a command response from the XBee. Max n-1 characters are
// read, and the first character, which normally is a \r, is
// discarded.
int xbee::response(char * buf, int n)
{
    const long int t = millis();
    while (millis() < t + xbee_wait && Serial.available() < n - 1)
        ;

    int i = 0;
    while (Serial.available() && (i < n - 1))
    {
        buf[i++] = Serial.read();
        if ((i == 1) && (buf[0] == '\r'))
        {
            i == 0;
            n--;
        }
    }
    buf[i] = 0;
    return i;
}

// Make the Xbee enter command mode. Returns true if it responds
// successfully
bool xbee::commandmode()
{
    delay(xbee_wait);
    Serial.flush();

    command("+++");

    char buf[3];
    response(buf, 3);
    if (strstr(buf, "OK"))
    {
        digitalWrite(pwr_led, HIGH);
        digitalWrite(comm_led, HIGH);
        return true;
    }
    else
        return false;
}

// Read the Xbee serial number and put it in the buffer provided. This
// function is buggy.
void xbee::read_serial_and_firmware(char snbuf[17], char fwbuf[10])
{
    if (commandmode())
    {
        int n = 0;
        command("ATSH\r");
        n = response(snbuf, 6);
        command("ATSL\r");
        response(snbuf + n, 9);
        snbuf[16] = 0;
        command("ATVR\r");
        response(fwbuf, 9);
        snbuf[8] = 0;
        command("ATFR\r");
        delay(xbee_wait);
        digitalWrite(pwr_led, HIGH);
        digitalWrite(comm_led, LOW);
    }
    else
    {
        strncpy(snbuf, "Fail", 17);
        strncpy(fwbuf, "Fail", 10);
    }
}

// this does a hardware reset by pulling the pin hooked up to the xbee
// reset pin low.
void xbee::hardware_reset()
{
    // wake up
    digitalWrite(sleep_pin, LOW);
    delay(5);
    Serial.flush();
    Serial.println("Resetting Xbee");

    digitalWrite(pwr_led, LOW);
    digitalWrite(comm_led, HIGH);

    delay(xbee_wait);
    digitalWrite(reset_pin, LOW);
    digitalWrite(comm_led, LOW);
    delay(xbee_wait);
    digitalWrite(reset_pin, HIGH);

    // it takes it a while to reboot enough to send stuff. wait again
    delay(xbee_wait);

    // test that it responds
    bool success = commandmode();
    delay(xbee_wait);

    for (int i = 0; i < 25; ++i)
    {
        Serial.println(success ? "Reset successful" : "Reset failed");
        digitalWrite(success ? comm_led : pwr_led, HIGH);
        delay(50);
        digitalWrite(success ? comm_led : pwr_led, LOW);
        delay(50);
    }

    delay(xbee_wait);
    digitalWrite(pwr_led, HIGH);
    digitalWrite(comm_led, LOW);
}

// Sometimes it seems like communications drop out entirely through
// the XBee. If we've retried many times without success, we do an
// XBee reset.
void xbee::software_reset()
{
    const int buflen      = 10;
    char      buf[buflen] = {0};

    // resetting don't seem to work with sleeping... it ends up in an
    // infinite loop of failed resets. how is this possible?
    digitalWrite(sleep_pin, LOW);
    delay(5);
    Serial.flush();
    Serial.println("Resetting Xbee");

    digitalWrite(pwr_led, LOW);
    digitalWrite(comm_led, HIGH);

    if (commandmode())
    {
        // Xbee dropped into command mode, issue reset command and read
        // response
        digitalWrite(pwr_led, HIGH);

        command("ATFR\r");
        response(buf, buflen);
    }

    bool success = strstr(buf, "OK");
    digitalWrite(pwr_led, LOW);
    digitalWrite(comm_led, LOW);

    for (int i = 0; i < 25; ++i)
    {
        Serial.println(success ? "Reset successful" : "Reset failed");
        digitalWrite(success ? comm_led : pwr_led, HIGH);
        delay(50);
        digitalWrite(success ? comm_led : pwr_led, LOW);
        delay(50);
    }

    digitalWrite(pwr_led, HIGH);
    // it takes it a while to reboot enough to send stuff. wait again
    delay(4 * xbee_wait);
}
