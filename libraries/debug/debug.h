#pragma once

#include "memory.h"
#include <Arduino.h>
#include <inttypes.h>

#define dp(level, ...)         \
    if (debug_level > (level)) \
        Serial.print(__VA_ARGS__);
#define dpln(level, ...)       \
    if (debug_level > (level)) \
        Serial.println(__VA_ARGS__);
#define dppgm(level, ...)      \
    if (debug_level > (level)) \
        sprint_pgm(PSTR(__VA_ARGS__));

extern uint8_t debug_level;
