#pragma once

#include "EventLoop.h"
#include <inttypes.h>

extern "C"
{
    void __vector_9();
}

/**
 * This class replaces the Arduino millis function when timer 0 has to
 * be changed. It uses timer 2 overflow interrupt, since Arduino
 * already defines the interrupt handler for timer 0 overflow. The
 * number of ms between timer overflows is a constructor argument, so
 * it's usable for different timer prescalers.
 */
class Timekeeper
{
public:
    /**
     * Create a time keeper with the specified number of clock ticks
     * per timer period. The period mustd be less than 1ms, because
     * the ISR only increments at most 1ms per execution.
     */
    Timekeeper(uint32_t timer_period_ticks);

    ms_t millis() const;

private:
    friend void __vector_9();

    /* Number of clock ticks per ms. */
    static uint32_t constexpr _ticks_per_ms = F_CPU / 1000;

    /* The period of the timer in clock ticks. This is assumed to be
    <1ms, because the ISR only increments at most 1ms per
    execution. */
    static uint32_t _ticks_per_tovf;
};
