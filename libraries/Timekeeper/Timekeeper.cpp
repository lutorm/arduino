#include "Timekeeper.h"
#include <avr/interrupt.h>

namespace
{
    volatile ms_t     _time_ms         = 0;
    volatile uint32_t _remainder_ticks = 0;
}  // namespace

uint32_t Timekeeper::_ticks_per_tovf = 0;

/* Must turn off interrupts while we copy the current value because
 * the increment in the interrupt is not atomic. */
ms_t Timekeeper::millis() const
{
    cli();
    ms_t const now = _time_ms;
    sei();
    return now;
}

Timekeeper::Timekeeper(uint32_t timer_period_ticks)
{
    _ticks_per_tovf = timer_period_ticks;
}

/* Can't use timer 0 overflow interrupt because it's already defined in Arduino. */
ISR(TIMER2_OVF_vect)
{
    uint32_t remainder_ticks = _remainder_ticks;
    remainder_ticks += Timekeeper::_ticks_per_tovf;

    if (remainder_ticks >= Timekeeper::_ticks_per_ms)
    {
        ++_time_ms;
        remainder_ticks -= Timekeeper::_ticks_per_ms;
    }

    _remainder_ticks = remainder_ticks;
}
