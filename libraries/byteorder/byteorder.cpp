#include "byteorder.h"

uint16_t swap_order(uint16_t in)
{
    uint16_t out;
    ((uint8_t *)&out)[0] = ((uint8_t *)&in)[1];
    ((uint8_t *)&out)[1] = ((uint8_t *)&in)[0];
    return out;
}

int16_t swap_order(int16_t in)
{
    int16_t out;
    ((uint8_t *)&out)[0] = ((uint8_t *)&in)[1];
    ((uint8_t *)&out)[1] = ((uint8_t *)&in)[0];
    return out;
}
