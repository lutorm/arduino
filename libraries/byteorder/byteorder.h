#pragma once

/* Byte swapping routines */

#include <inttypes.h>
#if 0
// This appears to not work...
template <typename T>
T swap_order(T in)
{
    T out;
    for (uint8_t i = 0; i < sizeof(T); ++i)
    {
        ((uint8_t*)&out)[i]= ((uint8_t*)&in)[sizeof(T)-i];
    }
    return out;
}
#endif

uint16_t swap_order(uint16_t in);
int16_t  swap_order(int16_t in);
