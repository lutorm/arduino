#include <Arduino.h>
#include <memory.h>

// this function is pretty dumb, but it should work...  From the
// playground: written by David A. Mellis based on code by Rob Faludi
// http://www.faludi.com.
int availableMemory()
{
    int    size = 2048;  // Use 2048 with ATmega328
    char * buf;

    while ((buf = (char *)malloc(--size)) == NULL)
        ;

    free(buf);

    return size;
}

extern void * __bss_end;
extern void * __brkval;

// This function just looks at the difference between the stack
// pointer and the heap.
int get_free_memory()
{
    int free_memory;

    if ((int)__brkval == 0)
        free_memory = ((int)&free_memory) - ((int)&__bss_end);
    else
        free_memory = ((int)&free_memory) - ((int)__brkval);

    return free_memory;
}

void sprint_pgm(Print & print, const prog_char * str)
{
    const prog_char * s = str;
    while (char c = pgm_read_byte(s++))
        print.print(c);
}

void sprint_pgm(const prog_char * str)
{
    sprint_pgm(Serial, str);
}
