#pragma once

#include <avr/pgmspace.h>
#include <stdlib.h>

class Print;

/* operator new/delete are now defined in the core. */
#include <new.h>

int availableMemory();
int get_free_memory();

/* Print a string in progmem. */
void sprint_pgm(Print &, const prog_char * str);
void sprint_pgm(const prog_char * str);
