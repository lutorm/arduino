#pragma once

#include <avr/pgmspace.h>

/* Update the supplied CRC with the supplied data. */
uint16_t update_crc(uint16_t crc, const uint8_t * data, int size);

/* Send header for n_data entries at time tick. */
void send_header(uint8_t n_data, unsigned long tick);

/* Send a data entry for the supplied address and value, updating the
 * crc.
 */
//template<typename T>
void send_data(uint8_t const address[8], float data, uint16_t & crc);
/*{
    float const & d(data);
    
    Serial.write(address, 8);
    Serial.write(reinterpret_cast<uint8_t const *>(&d), sizeof(d));
    Serial.write('\n');

    // update CRC
    crc = update_crc(crc, address, 8);
    crc = update_crc(crc, reinterpret_cast<uint8_t const *>(&d), sizeof(d));
}
*/
/* Send with an address in pgmspace. */
//template<typename T>
    void send_data_pgmaddr(prog_uint8_t const pgmaddr[8], float data, uint16_t & crc);
/*{
    uint8_t addr[8];
    memcpy_P(addr, pgmaddr, 8);
    send_data(addr, data, crc);
}
*/
/* Send the footer and wait ack_wait ms for response. Returns true if
 * a response was received, otherwise false.
 */
bool wait_for_response(uint16_t crc, int ack_wait);
