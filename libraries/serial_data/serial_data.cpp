#include "serial_data.h"
#include "memory.h"
#include <Arduino.h>
#include <util/crc16.h>

uint16_t update_crc(uint16_t crc, const uint8_t * data, int size)
{
    for (int j = 0; j < size; ++j)
        crc = _crc16_update(crc, data[j]);
    return crc;
}

void send_header(uint8_t n_data, unsigned long tick)
{
    sprint_pgm(PSTR("DATA "));
    Serial.print(n_data);
    sprint_pgm(PSTR(" TICK "));
    Serial.println(tick, HEX);
}

bool wait_for_response(uint16_t crc, int ack_wait)
{
    sprint_pgm(PSTR("DONE CRC="));
    Serial.println(crc, HEX);

    // busy loop waiting for serial data for at most ack_wait
    const long int t = millis();
    while (millis() < t + ack_wait && Serial.available() < 3)
        ;

    if (Serial.available() >= 3 && Serial.read() == 'O' && Serial.read() == 'K')
    {
        // Serial.print("Got OK after ");
        // Serial.println(millis()-t);
        return true;
    }
    else
    {
        // did not get OK. reader might be confused. write a newline
        Serial.write('\n');
        return false;
    }
}

void send_data(uint8_t const address[8], float data, uint16_t & crc)
{
    Serial.write(address, 8);
    Serial.write(reinterpret_cast<uint8_t const *>(&data), sizeof(data));
    Serial.write('\n');

    // update CRC
    crc = update_crc(crc, address, 8);
    crc = update_crc(crc, reinterpret_cast<uint8_t const *>(&data), sizeof(data));
}

/* Send with an address in pgmspace. */
//template<typename T>
void send_data_pgmaddr(prog_uint8_t const pgmaddr[8], float data, uint16_t & crc)
{
    uint8_t addr[8];
    memcpy_P(addr, pgmaddr, 8);
    send_data(addr, data, crc);
}
