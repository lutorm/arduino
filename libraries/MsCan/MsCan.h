
/* MegaSquirt CAN-bus protocol marshal and demarshal routines. Based on
   http://www.msextra.com/doc/pdf/Megasquirt_29bit_CAN_Protocol-2015-01-20.pdf.

   Note that MS uses opposite byte order from avr, so all data have to
   be byte-swapped.
*/

#pragma once

#include "EventLoop.h"
#include "mcp_can.h"
#include <inttypes.h>

class MsCan : public Event
{
public:
    /* Create a MsCan reader, using the specified MCP_CAN object for
     * CAN BUS communication. */
    MsCan(MCP_CAN * mcp_2515, uint8_t our_can_id);

    struct Id;
    struct MsgReqData;

    enum /* class */ MsgType
    {
        INVALID    = -1,
        MSG_CMD    = 0,
        MSG_REQ    = 1,
        MSG_RSP    = 2,
        MSG_XSUB   = 3,
        MSG_BURN   = 4,
        OUTMSG_REQ = 5,
        OUTMSG_RSP = 6,
        MSG_XTND   = 7
    };

    /* Allocate n tables. */
    void allocate_tables(uint8_t n);

    /* Allocate n bytes to table tid. */
    void allocate_table(uint8_t tid, uint8_t n);

    /* Return a pointer into an allocated table so data can be read
     * from or written to it. */
    uint8_t * get_table_ptr(uint8_t tid, uint8_t offset);

    /* Allocate room in the table for 11-bit broadcasts to listen for. */
    void allocate_listeners(uint8_t n);

    /* Set listener slot to listen for identifier id. */
    void set_listener(uint8_t slot, uint16_t id);

    /* Retrieve the pointer to the slot where data will be deposited
     * for identifier id. */
    uint16_t * get_listener_ptr(uint16_t id);

    /* Look for messages and handle any that are found. */
    void dispatch(ms_t) override final;

    /* Send a MegaSquirt MSG_REQ message. This requests that req_size
     * bytes starting at the req table/offset be returned to
     * dest_table/dest_offset. */
    bool send_can_msg_req(uint8_t  dest_can_id,
                          uint8_t  requested_table,
                          uint16_t requested_offset,
                          uint8_t  requested_size,
                          uint8_t  dest_table,
                          uint8_t  dest_offset);

    /* Return the status of any failures. This also clears the flag. */
    uint8_t can_status();

private:
    void _handle_can_msg_req(MsCan::Id const & i, uint8_t * buf);

    /* Handle an incoming CAN-bus MSG_CMD or MSG_RSP message. This
     * results in writing the received data into the correct table. */
    void _handle_can_msg_cmd(MsCan::Id const & i, uint8_t * buf, uint8_t len);

    void _handle_can_msg_11bit(uint32_t id, uint8_t len, uint8_t * buf);

    /* The ID of this controller. */
    uint8_t _our_can_id;

    uint8_t _can_status;

    /* CAN-bus tables. All CAN comms consist of reading or writing into
     * these so the comm code doesn't really need to know what they
     * are. It's the application's responsibility to populate them. */
    uint8_t    _n_tables;
    uint8_t ** _tables;
    uint8_t *  _table_len;

    MCP_CAN * _mcp_2515;

    /* Id to listen for in 11-bit broadcasts. In the broadcasts, each
     * 11-bit identifier comes with 4 16-bit quantities. */
    struct Listener
    {
        uint16_t identifier;
        uint16_t data[4];
    };

    uint8_t    _n_listeners;
    Listener * _listeners;

    /* Additional status codes over those provided by mcp_can. */
    enum status_codes
    {
        MSCAN_INCORRECT_MSG_REQ_LEN = 20,
        MSCAN_UNKNOWN_MSG,
        MSCAN_UNKNOWN_TABLE,
        MSCAN_TABLE_OVERFLOW
    };
};

/* The standard 29-bit extended CAN identifier. */
struct MsCan::Id
{
    uint8_t  table;
    uint8_t  to_id;
    uint8_t  from_id;
    MsgType  msg_type;
    uint16_t offset;

    Id();

    /* Demarshal a received 32-bit int into an Id struct. */
    Id(uint32_t id);

    /* Marshal an Id into a 32-bit int for sending. */
    uint32_t marshal() const;
};

/* The data for a MS MSG_REQ message. */
struct MsCan::MsgReqData
{
    static uint8_t const len = 3;

    uint8_t  my_varblk;
    uint8_t  varbyt;
    uint16_t my_varoffset;

    MsgReqData() {}

    /* Demarshal received 32-buit int into a MsgReqData struct. */
    MsgReqData(uint32_t);

    /* Marshal an Id into a 32-bit int for sending. (Only 3 bytes
     * should be sent.) */
    uint32_t marshal() const;
};
