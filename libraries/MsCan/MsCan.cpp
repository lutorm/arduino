#include "MsCan.h"
#include "debug.h"
#include "memory.h"
#include <new.h>

MsCan::MsCan(MCP_CAN * mcp_2515, uint8_t our_can_id) :
    _our_can_id(our_can_id),
    _can_status(CAN_OK),
    _n_tables(0),
    _mcp_2515(mcp_2515),
    _n_listeners(0)
{
}

void MsCan::allocate_tables(uint8_t n)
{
    if (_n_tables > 0)
    {
        /* already allocated */
        sprint_pgm(PSTR("MsCan: tables already allocated: "));
        return;
    }

    _n_tables  = n;
    _tables    = new uint8_t *[n];
    _table_len = new uint8_t[n];
    /* null out data so we know if it's been allocated or not. */
    memset(_tables, 0, sizeof(uint8_t *) * n);
    memset(_table_len, 0, sizeof(uint8_t) * n);
}

void MsCan::allocate_table(uint8_t tid, uint8_t n)
{
    if (tid >= _n_tables)
    {
        sprint_pgm(PSTR("MSCan: table ID outside range:"));
        Serial.println(tid);
        return;
    }

    if (_table_len[tid] > 0)
    {
        /* already allocated */
        sprint_pgm(PSTR("MsCan: table already allocated: "));
        Serial.println(tid);
        return;
    }

    _table_len[tid] = n;
    _tables[tid]    = new uint8_t[n];
}

uint8_t * MsCan::get_table_ptr(uint8_t tid, uint8_t offset)
{
    if ((tid >= _n_tables) || (offset >= _table_len[tid]))
    {
        sprint_pgm(PSTR("MsCan: table ptr out of bounds: "));
        Serial.println(tid);
        return 0;
    }

    return _tables[tid] + offset;
}

void MsCan::allocate_listeners(uint8_t n)
{
    if (_n_listeners > 0)
    {
        /* already allocated */
        sprint_pgm(PSTR("MsCan: listeners already allocated: "));
        return;
    }

    _n_listeners = n;
    _listeners   = new Listener[n];
    /* null out data so we know if it's been allocated or not. */
    memset(_listeners, 0, sizeof(Listener) * n);
}

void MsCan::set_listener(uint8_t slot, uint16_t id)
{
    if (slot >= _n_listeners)
    {
        sprint_pgm(PSTR("MSCan: listener slot outside range:"));
        Serial.println(slot);
        return;
    }

    if (_listeners[slot].identifier > 0)
    {
        /* already allocated */
        sprint_pgm(PSTR("MsCan: listener already allocated: "));
        Serial.println(slot);
        return;
    }

    _listeners[slot].identifier = id;
}

uint16_t * MsCan::get_listener_ptr(uint16_t id)
{
    for (uint8_t i = 0; i < _n_listeners; ++i)
    {
        if (_listeners[i].identifier == id)
        {
            return _listeners[i].data;
        }
    }

    sprint_pgm(PSTR("MsCan: listener id not listened to: "));
    Serial.println(id);
    return nullptr;
}

uint8_t MsCan::can_status()
{
    uint8_t const tmp = _can_status;
    _can_status       = CAN_OK;
    return tmp;
}

void MsCan::dispatch(ms_t)
{
    uint32_t id;
    uint8_t  len;
    /* Max CAN BUS data length is 8 bytes. */
    uint8_t buf[8];

    uint8_t res = _mcp_2515->readMsgBufID(&id, &len, buf);

    switch (res)
    {
    case CAN_NOMSG:

        return;

    case CAN_OK:

        if (_mcp_2515->isExtendedFrame())
        {
            /* 29-bit frame. Decode it. */
            MsCan::Id i(id);

            // see if it's ours
            if (i.to_id == _our_can_id)
            {
                dppgm(2, "Recv CAN 29-bit msg from id ");
                dpln(2, i.from_id);

                switch (i.msg_type)
                {
                case MSG_REQ:

                    if (len != 3)
                    {
                        if (!_can_status)
                        {
                            _can_status = MSCAN_INCORRECT_MSG_REQ_LEN;
                            dppgm(1, "Incorrect MSG_REQ length: ");
                            dpln(1, len);
                        }
                        return;
                    }
                    _handle_can_msg_req(i, buf);
                    break;

                case MSG_CMD:
                case MSG_RSP:

                    _handle_can_msg_cmd(i, buf, len);
                    break;

                default:

                    if (!_can_status)
                    {
                        _can_status = MSCAN_UNKNOWN_MSG;
                        dppgm(1, "Unknown CAN msg type: ");
                        dpln(1, i.msg_type);
                    }
                }
            }
        }
        else
        {
            _handle_can_msg_11bit(id, len, buf);
        }
        break;

    default:

        if (!_can_status)
        {
            _can_status = res;

            /* Only display error once unless the flag is cleared. */
            dppgm(1, "CAN recv fail:");
            dpln(1, res);
        }
    }
}

void MsCan::_handle_can_msg_11bit(uint32_t id, uint8_t len, uint8_t * buf)
{
    /* 11-bit frame. */
    dppgm(2, "Recv 11-bit CAN frame: ");
    dp(2, id);
    dppgm(2, ", ");
    dp(2, len);
    dppgm(2, " bytes: ");
    char tmp[3];
    for (uint8_t i = 0; i < len; ++i)
    {
        sprintf(tmp, "%02hX", buf[i]);
        dp(2, tmp);
        if (i < len - 1)
            dp(2, ":");
    }
    dppgm(2, "\r\n");

    for (uint8_t i = 0; i < _n_listeners; ++i)
    {
        if (id == _listeners[i].identifier)
        {
            dppgm(2, "Depositing data in slot: ");
            dpln(2, i);
            memcpy(_listeners[i].data, buf, len);
            return;
        }
    }
}

/* Handle a CAN-bus MSG_REQ message in buf. The response is a MSG_RSP
 * with the requested data. */
void MsCan::_handle_can_msg_req(MsCan::Id const & i, uint8_t * buf)
{
    // Demarshal the MSG_REQ data.
    MsCan::MsgReqData data(*(uint32_t *)buf);

    if (debug_level > 3)
    {
        sprint_pgm(PSTR("Recv MSQ_REQ: "));
        Serial.print(i.table);
        sprint_pgm(PSTR("/"));
        Serial.print(i.offset);
        sprint_pgm(PSTR(": "));
        Serial.print(data.varbyt);
        sprint_pgm(PSTR(" bytes to "));
        Serial.print(data.my_varblk);
        sprint_pgm(PSTR("/"));
        Serial.println(data.my_varoffset);
    }

    if ((i.table >= _n_tables) || (i.offset >= _table_len[i.table]))
    {
        // unknown table
        if (!_can_status)
        {
            _can_status = MSCAN_UNKNOWN_TABLE;
            dppgm(1, "MSG_REQ for unknown table ");
            dp(1, i.table);
            dppgm(1, "/");
            dpln(1, i.offset);
        }
        return;
    }

    MsCan::Id resp;
    resp.table    = data.my_varblk;
    resp.to_id    = i.from_id;
    resp.from_id  = i.to_id;
    resp.msg_type = MsCan::MSG_RSP;
    resp.offset   = data.my_varoffset;

    int res = _mcp_2515->sendMsgBuf(resp.marshal(),
                                    1,
                                    data.varbyt,
                                    get_table_ptr(i.table, i.offset));
    if (res != CAN_OK)
    {
        if (!_can_status)
        {
            _can_status = res;
            dppgm(1, "send MSG_RSP in response to MSG_REQ failed: ");
            dpln(1, res);
        }
    }
}

bool MsCan::send_can_msg_req(uint8_t  dest_can_id,
                             uint8_t  requested_table,
                             uint16_t requested_offset,
                             uint8_t  requested_size,
                             uint8_t  dest_table,
                             uint8_t  dest_offset)
{
    MsCan::Id id;
    id.table    = requested_table;
    id.offset   = requested_offset;
    id.to_id    = dest_can_id;
    id.from_id  = _our_can_id;
    id.msg_type = MsCan::MSG_REQ;

    /* Put it in the realtime table, table 1, offset 0. */
    MsCan::MsgReqData req_data;
    req_data.my_varblk    = dest_table;
    req_data.my_varoffset = dest_offset;
    req_data.varbyt       = requested_size;

    uint32_t buf = req_data.marshal();

    dppgm(2, "Sending MSG_REQ to ");
    dpln(2, id.to_id);

    if (debug_level > 3)
    {
        sprint_pgm(PSTR("\tsend "));
        Serial.print(req_data.varbyt);
        sprint_pgm(PSTR("b from "));
        Serial.print(id.table);
        sprint_pgm(PSTR("/"));
        Serial.print(id.offset);
        sprint_pgm(PSTR(" to "));
        Serial.print(req_data.my_varblk);
        sprint_pgm(PSTR("/"));
        Serial.print(req_data.my_varoffset);
        Serial.println();
    }

    int res = _mcp_2515->sendMsgBuf(id.marshal(), 1, MsgReqData::len, (uint8_t *)&buf);

    if (res != CAN_OK)
    {
        if (!_can_status)
        {
            _can_status = res;
            dppgm(1, "send MSG_REQ failed: ");
            dpln(1, res);
            return false;
        }
    }

    return true;
}

void MsCan::_handle_can_msg_cmd(MsCan::Id const & i, uint8_t * buf, uint8_t len)
{
    if (debug_level > 3)
    {
        sprint_pgm(PSTR("Recv MSG_"));
        if (i.msg_type == MSG_CMD)
            sprint_pgm(PSTR("CMD"));
        else
            sprint_pgm(PSTR("RSP"));
        sprint_pgm(PSTR(": "));

        Serial.print(i.table);
        sprint_pgm(PSTR("/"));
        Serial.print(i.offset);
        sprint_pgm(PSTR(": "));
        Serial.print(len);
        sprint_pgm(PSTR(" bytes:"));
        for (int i = 0; i < len; ++i)
        {
            Serial.print(buf[i], HEX);
            sprint_pgm(PSTR(" "));
        }
        Serial.println();
    }

    if ((i.table >= _n_tables) || (i.offset >= _table_len[i.table]))
    {
        if (!_can_status)
        {
            _can_status = MSCAN_UNKNOWN_TABLE;
            // unknown table
            dppgm(1, "MSG_CMD/RSP for unknown table ");
            dp(1, i.table);
            dppgm(1, "/");
            dpln(1, i.offset);
        }
        return;
    }

    if (i.offset + len > _table_len[i.table])
    {
        if (!_can_status)
        {
            _can_status = MSCAN_TABLE_OVERFLOW;
            dppgm(1, "MSG_CMD/RSP past table end: ");
            dp(1, i.table);
            dppgm(1, "/");
            dp(1, i.offset);
            dppgm(1, "/");
            dpln(1, len);
        }
        return;
    }

    uint8_t * dest = get_table_ptr(i.table, i.offset);

    memcpy(dest, buf, len);
}

MsCan::Id::Id() : table(0), to_id(0), from_id(0), msg_type(INVALID), offset(0) {}

MsCan::Id::Id(uint32_t id)
{
    table    = (id >> 3) & 0x0f;  // what about table bit 4?
    to_id    = (id >> 7) & 0x0f;
    from_id  = (id >> 11) & 0x07;
    msg_type = MsgType((id >> 15) & 0x07);
    offset   = (id >> 18) & 0x03ff;
}

uint32_t MsCan::Id::marshal() const
{
    uint32_t id = 0;

    id |= uint32_t(table & 0x0f) << 3;
    id |= uint32_t(to_id & 0x0f) << 7;
    id |= uint32_t(from_id & 0x07) << 11;
    id |= (uint32_t(msg_type) & 0x07) << 15;
    id |= uint32_t(offset & 0x03ff) << 18;

    return id;
}

MsCan::MsgReqData::MsgReqData(uint32_t buf32)
{
    uint8_t * buf = (uint8_t *)&buf32;

    my_varblk    = buf[0] & 0x1f;
    my_varoffset = (buf[2] >> 5) | (uint16_t(buf[1]) << 3);
    varbyt       = buf[2] & 0x0f;
}

uint32_t MsCan::MsgReqData::marshal() const
{
    uint32_t  buf32 = 0;
    uint8_t * buf   = (uint8_t *)&buf32;
    buf[0]          = my_varblk & 0x1f;
    buf[1]          = my_varoffset >> 3;
    buf[2]          = (varbyt & 0x0f) | (my_varoffset << 5);
    return buf32;
}
