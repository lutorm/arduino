#pragma once

#include "SPIbase.h"
#include <stdint.h>

/** This class defines an object representing a MAX7221 SPI-connected
    LED display driver. It only sends updates to the digit registers
    if necessary, which saves bandwidth. */

class max7221
{

    /// The SPI bus the display is connected to.
    SPIbus * bus_;

    /// The current state of the 8 digit registers. Used to determine if
    /// an update is necessary.
    uint8_t state_[8];

public:
    /** Constructor initializes the registers to a known state so the
        update detection will work correctly. */
    max7221(SPIbus * b);

    static int b_encode_number(char * fmt, char * str, int n, float v);

    /** Sets the digit state. The meaning of val depends on the decode
        register. */
    void set_digit(uint8_t d, uint8_t val);

    /** Sets the decode register to the specified value. A one means the
        digit is code-B decoded, while a zero means direct addressing of
        the 8 elements. */
    void set_decode(uint8_t val) { bus_->transfer(0x0900 | val); };

    /** Sets the intensity register. A zero puts the display in
        shutdown, 1-16 sets the intensity to progressively higher
        values. */
    void set_intensity(uint8_t val);

    /** Sets the scan limit to display n digits. */
    void set_scanlimit(uint8_t n) { bus_->transfer(0x0b00 | (n - 1)); };

    /** Controls the display shutdown state. */
    void shutdown(bool val) { bus_->transfer(0x0c00 | (val ? 0x00 : 0x01)); };

    /** Controls the display test state. */
    void test(bool val) { bus_->transfer(0x0f00 | (val ? 0x01 : 0x00)); };
};
