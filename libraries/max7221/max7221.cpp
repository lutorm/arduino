#include "max7221.h"
#include "SPI.h"
#include "memory.h"

max7221::max7221(SPIbus * b) : bus_(b)
{
    bus_->setBitOrder(MSBFIRST);
    for (uint8_t i = 0; i < 8; ++i)
    {
        state_[i] = 0;
        bus_->transfer(i + 1);
        bus_->transfer(state_[i]);
    }
}

void max7221::set_digit(uint8_t d, uint8_t val)
{
    if (d > 7)
        return;
    if (val != state_[d])
    {
        bus_->transfer(d + 1);
        bus_->transfer(val);
        state_[d] = val;
    }
}

void max7221::set_intensity(uint8_t val)
{
    if (val == 0)
        shutdown(true);
    else
    {
        shutdown(false);
        bus_->transfer(0x0a00 | (val - 1));
    }
}

/** B-encodes a float value, according to the format string. Note that
    because the decimal point is subsumed into a character, the
    returned string is one character shorter than that indicated by
    the format string and n if the formatted string contains a decimal
    point. Returns the number of character in the encoded string. */
int max7221::b_encode_number(char * fmt, char * str, int n, float v)
{
    const bool debug = false;

    if (isnan(v))
    {
        // if the value is NaN we have to override, because NaN can't be
        // B-encoded. Instead, we set the string to EE.
        if (n == 1)
        {
            str[0] = 0x0b;
            return 1;
        }
        else if (n == 2)
        {
            str[0] = 0x0b;
            str[1] = 0x0b;
            return 2;
        }
        str[0] = 0x0f;
        str[1] = 0x0b;
        str[2] = 0x0b;
        return 3;
    }

    uint8_t l = snprintf(str, n, fmt, v);

    if (debug)
    {
        Serial.print("val: ");
        Serial.print(v);
        Serial.print("disp:");
        Serial.println(str);
    }
    uint8_t j = 0;
    // transform string to b-code
    for (uint8_t i = 0; i < l;)
    {
        if (debug)
        {
            Serial.print(j, DEC);
            sprint_pgm(PSTR(" \'"));
            Serial.print(str[i]);
            sprint_pgm(PSTR("\' "));
        }
        uint8_t c = str[i++];
        if (c == '-')
            c = 0x0a;
        else if (c == ' ')
            c = 0x0f;
        else
            c -= '0';
        if (str[i] == '.')
        {
            c |= 0x80;
            ++i;
        }
        str[j++] = c;
        if (debug)
            Serial.println(c, HEX);
    }
    return j;
}
