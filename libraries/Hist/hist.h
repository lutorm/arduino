/* A class for accumulating data into a plot vs time. */

#pragma once

#include "float16.h"
#include <Arduino.h>

class hist_base
{
protected:
    // The direction of the plot.
    const bool grow_on_right_;

    // number of seconds per data point
    const uint8_t period_;

    // number of data points in current point
    uint16_t cur_n_;

    // plot ymin and range. To save memory, they are 16-bit floats.
    const float16 min_, range_;

    // the data point being currently accumulated
    float cur_val_;

    // timer value at last update
    long int utime_;

private:
    virtual float        read_source() const       = 0;
    virtual byte &       data(byte i)              = 0;
    virtual const byte & data(byte i) const        = 0;
    virtual byte         point_index(byte i) const = 0;

protected:
public:
    hist_base(uint8_t period, float min, float max, bool grow_on_right);

    // read the input and update data
    void update();

    virtual int n() const = 0;

    // returns a point in the plot as a number between 0-1
    float point(byte) const;
};

template<typename T, int N_points>
class hist : public hist_base
{
private:
    // the variable that's the source
    const T * source_;

    // data points, most current first. Data are stored as 0-254. 255 is
    // a reserved value that means there were no valid data points during
    // the interval.
    byte data_[N_points];

    virtual float read_source() const { return float(*source_); };
    // get a data point. The most recent point is either 0 or N-1
    // depending on the direction setting.
    virtual byte &       data(byte i) { return data_[i]; };
    virtual const byte & data(byte i) const { return data_[i]; };

    // transforms the storage order where 0 is the most recent to the
    // one where 0 is the one on the left
    virtual byte point_index(byte i) const
    {
        return grow_on_right_ ? N_points - 1 - i : i;
    };

public:
    hist(const T * v, uint8_t period, float min, float max, bool grow_on_right = true);

    virtual int n() const { return N_points; };
};

template<typename T, int N_points>
hist<T, N_points>::hist(
    const T * v, uint8_t period, float min, float max, bool grow_on_right) :
    hist_base(period, min, max, grow_on_right), source_(v)
{
    for (int i = 0; i < n(); ++i)
        data(i) = 0;
};
