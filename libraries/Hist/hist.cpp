#include <avr/pgmspace.h>
#include <hist.h>
#include <memory.h>

hist_base::hist_base(uint8_t period, float min, float max, bool grow_on_right) :
    grow_on_right_(grow_on_right),
    period_(period),
    cur_val_(0),
    cur_n_(0),
    min_(min),
    range_(max - min),
    utime_(0)
{
}

float hist_base::point(byte x) const
{
    if (x > n())
        return NAN;
    byte v = data(point_index(x));
    if (v == 255)
    {
        return NAN;
    }
    else
        return float(v) / 254;
}

void hist_base::update()
{
    const float s = read_source();
    if (s == s)
    {
        // if the input is NaN, we ignore it
        cur_val_ += s;
        ++cur_n_;
    }

    long int now = millis();
    // take care that we update if the counter wraps
    if ((now - utime_ < period_ * 1000L) && (now > utime_))
        return;
    utime_ = now;

    // move existing data down
    for (int i = n() - 1; i > 0; --i)
        data(i) = data(i - 1);
    // and add current point and reset
    if (cur_n_ > 0)
        data(0) = constrain((cur_val_ / cur_n_ - min_) / range_, 0.0f, 1.0f) * 254;
    else
    {
        // all data points invalid
        data(0) = 255;
    }

    cur_val_ = 0;
    cur_n_   = 0;
}
