#include "version.h"

// GITVERSION is set by the makefile

#define STRINGIFY2(X) #X
#define STRINGIFY(X) STRINGIFY2(X)

const prog_char version[15] PROGMEM = STRINGIFY(GITVERSION);
