#pragma once

#include "float16.h"

/* Class that measures the frequency of signals on  an input pin using
   an interrupt. */
class frequency
{
private:
    // The update frequency. Need enough time to record
    const static unsigned long int min_ticks_ = 100;

    // The interrupt to measure. This is determined by the pin the
    // signal is measured on.
    uint8_t interrupt_;
    // The divisor applied to the number of pulses. Fans typically give
    // 2 or 4 pulses per revolution.
    uint8_t div_;
    // The time counter at the last
    unsigned long int last_;

public:
    frequency(uint8_t, uint8_t);

    void update_frequency();

    float16 freq_;
};

// avoid polluting the global namespace with the global counters
namespace frequency_impl
{
    const int n_interrupts = 2;
    // counters for the interrupt functions.
    extern unsigned int ctr[n_interrupts];

    void int0();
    void int1();
};  // namespace frequency_impl
