#include "frequency.h"
#include "Arduino.h"

// we can't feed any parameters to the interrupt function, and I don't
// know if it's possible to determine what interrupt a function was
// triggered by, so we just hardcode it by attaching two different
// functions.
frequency::frequency(uint8_t pin, uint8_t div) : interrupt_(0xff), div_(div), freq_(0)
{
    if (pin == 2)
        interrupt_ = 0;
    else if (pin == 3)
        interrupt_ = 1;
    else
    {
        // illegal pin
        return;
    }

    frequency_impl::ctr[interrupt_] = 0;
    last_                           = millis();

    switch (interrupt_)
    {
    case 0:
        pinMode(2, INPUT);
        attachInterrupt(0, frequency_impl::int0, RISING);
        break;
    case 1:
        pinMode(3, INPUT);
        attachInterrupt(1, frequency_impl::int1, RISING);
        break;
    };
};

// We update when we have enough ticks. This ensures we always have
// enough to accurately measure a frequency, and gives faster updates
// for higher frequencies. However, we need a max dt otherwise we can
// never get zero.
void frequency::update_frequency()
{
    unsigned long int now = millis();
    unsigned long int dt;
    if (now < last_)
        // counter wrapped.
        dt = now + 1 + (0xFFFFFFFF - last_);
    else
        dt = now - last_;

    if ((frequency_impl::ctr[interrupt_] < min_ticks_) && (dt < 20000UL))
        return;

    freq_ = 1000.0f * frequency_impl::ctr[interrupt_] / (dt * div_);
    last_ = now;
    frequency_impl::ctr[interrupt_] = 0;

    last_ = now;
};

unsigned int frequency_impl::ctr[frequency_impl::n_interrupts];

// interrupt 0 function
void frequency_impl::int0()
{
    ctr[0]++;
};

// interrupt 1 function
void frequency_impl::int1()
{
    ctr[1]++;
};
