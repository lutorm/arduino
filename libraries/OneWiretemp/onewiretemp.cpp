#include <onewiretemp.h>

const byte        OneWireBus::OneWireFamilyCodes_[] = {0x10, 0x28, 0x3b};
const prog_char   f1[] PROGMEM                      = "DS18S20";
const prog_char   f2[] PROGMEM                      = "DS18B20";
const prog_char   f3[] PROGMEM = "MAX31850";  // MAX31850 Thermocouple amplifier
const prog_char * OneWireBus::OneWireFamilies_[] = {f1, f2, f3};

const prog_char scanning[] PROGMEM      = "\n\rScanning 1-wire bus on pin ";
const prog_char coloncr[] PROGMEM       = ":\n\r";
const prog_char found[] PROGMEM         = "\tFound ";
const prog_char devonbus[] PROGMEM      = " devices on bus\n\r\n\r";
const prog_char founddev[] PROGMEM      = "Found devices:\n\r";
const prog_char invalid_crc[] PROGMEM   = "Invalid CRC";
const prog_char restartsearch[] PROGMEM = ", restarting search\n\r";
const prog_char devcr[] PROGMEM         = " device\n\r";
const prog_char unknownfamily[] PROGMEM = "unknown device family -- ignoring\n\r";
const prog_char parasite[] PROGMEM      = "\tParasite powered\n\r";
const prog_char externalpower[] PROGMEM = "\tExternally powered\n\r";
const prog_char colon[] PROGMEM         = ":";
const prog_char empty_search[] PROGMEM =
    "Device presence detected but search returned no addresses.\n\r";
const prog_char undefined_device[] PROGMEM = "Found undefined device: ";
const prog_char notfound[] PROGMEM         = "not found\n\r";
const prog_char nodevice[] PROGMEM =
    "\n\r\t1-wire comm error, no devices responding\n\r\t";
const prog_char reading_scratchpad[] PROGMEM = " reading scratchpad\n\r";

bool compare_address(const uint8_t * a, const uint8_t * b)
{
    int i = 0;
    while ((i < 8) && (a[i] == b[i]))
        i++;
    return i == 8;
}

bool compare_pgmaddress(const uint8_t * a, const prog_uint8_t * b)
{
    int i = 0;
    while ((i < 8) && (a[i] == pgm_read_byte(b + i)))
        i++;
    return i == 8;
}

void printhex(const uint8_t * const data, byte len)
{
    char tmp[3];
    for (byte j = 0; j < len; j++)
    {
        sprintf(tmp, "%02hX", data[j]);
        Serial.print(tmp);
        if (j < len - 1)
            sprint_pgm(colon);
    }
}

void printpgmhex(const uint8_t * const data, byte len)
{
    char    tmp[3];
    uint8_t v;
    for (byte j = 0; j < len; j++)
    {
        v = pgm_read_byte(data + j);
        sprintf(tmp, "%02hX", v);
        Serial.print(tmp);
        if (j < len - 1)
            sprint_pgm(colon);
    }
}

bool OneWireBus::any_parasite() const
{
    bool b = true;
    for (byte i = 0; i < ndevice_; ++i)
    {
        if ((device_family(i) >= 0) && (devices_[i].flags & OneWireDevice::PRESENT))
            b &= devices_[i].flags & OneWireDevice::POWERED;
    }
    return b;
}

/* The following functions require the onewire search functions. */
#if ONEWIRE_SEARCH

// discovers the number of devices on the bus
int OneWireBus::get_n_devices()
{
    byte addr[8];
    int  i = 0;
    bool presence;

    presence = com_.reset();
    com_.reset_search();
    while (com_.search(addr))
    {
        ++i;
    }
    if (presence && (i == 0))
    {
        sprint_pgm(empty_search);
        return -1;
    }

    return i;
}

// Fills the array addr with the addresses of the devices on the bus,
// up to maxnum
void OneWireBus::enumerate_devices()
{
    byte i = 0;
    com_.reset();
    com_.reset_search();
    while (com_.search(addresses_ + i * 8) && (i < ndevice_))
    {
        if (OneWire::crc8(addresses_ + i * 8, 7) != addresses_[i * 8 + 7])
        {
            sprint_pgm(invalid_crc);
            sprint_pgm(restartsearch);
            // address is not valid. we must restart the search
            i = 0;
            com_.reset_search();
            continue;
        }
        i++;
    }
}

// Checks whether the predefined devices are present and whether there
// are any additional devices on the bus that will be ignored. This is
// strictly for informational purposes, it doesn't affect the behavior
// in any way.
void OneWireBus::check_device_presence()
{
    uint8_t addr[8];
    com_.reset();
    com_.reset_search();
    while (com_.search(addr))
    {
        if (OneWire::crc8(addr, 7) != addr[7])
        {
            sprint_pgm(invalid_crc);
            sprint_pgm(restartsearch);
            // address is not valid. we must restart the search
            com_.reset_search();
            continue;
        }
        byte i;
        for (i = 0; i < ndevice_; ++i)
            if (compare_pgmaddress(addr, paddresses_[i]))
            {
                // found
                devices_[i].flags |= OneWireDevice::PRESENT;
                break;
            }
        if (i == ndevice_)
        {
            // we found an unknown device on the bus
            sprint_pgm(undefined_device);
            printhex(addr, 8);
            sprint_pgm(PSTR("\n\r"));
        }
    }
}
#endif

// constructor

OneWireBus::OneWireBus(int                   pin,
                       const prog_uint8_t ** addrs,
                       const prog_char **    names,
                       int                   ndev,
                       int                   failtimeout,
                       bool                  v) :
    com_(pin),
    paddresses_(addrs),
    pnames_(names),
    ndevice_(ndev),
    failtimeout_(failtimeout)
{
    flags = USE_PGMADDRESS | (v ? VERBOSE : 0);

    // allocate memory for the devices
    devices_ = new OneWireDevice[ndevice_];

    for (byte i = 0; i < ndevice_; ++i)
    {
        devices_[i].flags  = 0;
        devices_[i].output = 0;
    }

#if ONEWIRE_SEARCH
    /* If the onewire search is defined, do a search to check for presence or additional
     * devices. */
    check_device_presence();
#endif

    /* Loop over all devices, check power status, and print. */
    for (byte i = 0; i < ndevice_; ++i)
    {
        uint8_t addr[8];
        byte data[9];
        memcpy_P(addr, paddresses_[i], 8);

        if (flags & VERBOSE)
        {
            print(i, DEC);
            print(" ");
            printhex(addr, 8);
            sprint_pgm(PSTR(" ("));
            sprint_pgm(pnames_[i]);
            sprint_pgm(PSTR(")\r\n\t"));
        }

        /* Query power status. If device responds, we know it's present. */
        com_.reset();
        com_.select(addr);
        com_.write(0xB4);
        if (com_.read() != 0)
        {
            devices_[i].flags |= OneWireDevice::PRESENT;
            devices_[i].flags |= OneWireDevice::POWERED;
        }
        
        /* If we still don't know if the device is present, attempt to
         * read its scratchpad to see if it responds. */
        if(read_scratchpad(addr, data))
        {
            devices_[i].flags |= OneWireDevice::PRESENT;
        }            

        // find family
        for (byte f = 0; f < nFamilies_; ++f)
        {
            if (addr[0] == OneWireFamilyCodes_[f])
            {
                devices_[i].flags |= OneWireDevice::KNOWN;
                sprint(OneWireFamilies_[f]);
                sprint(devcr);
                break;
            }
            if (f == nFamilies_)
            {
                devices_[i].flags &= OneWireDevice::KNOWN;
                sprint(unknownfamily);
                continue;
            }
        }
        
        if (!(devices_[i].flags & OneWireDevice::PRESENT))
        {
            sprint(notfound);
            continue;
        }

        if (devices_[i].flags & OneWireDevice::POWERED)
        {
            sprint(externalpower);
        }
        else
        {
            sprint(parasite);
        }
    }

    // see if any devices are parasite powered
    flags |= (any_parasite() ? ANY_PARASITE : 0);
    conversion_start_ = -1;
}

#if ONEWIRE_SEARCH
/* This constructor scans for devices on the bus. */
OneWireBus::OneWireBus(int pin, bool v) : com_(pin)
{
    flags = (v ? VERBOSE : 0);

    sprint(scanning);
    print(pin);
    sprint(coloncr);

    ndevice_ = get_n_devices();

    sprint(found);
    print(ndevice_);
    sprint(devonbus);

    // allocate memory for the devices
    devices_   = new OneWireDevice[ndevice_];
    addresses_ = new uint8_t[8 * ndevice_];
    names_     = new char *[ndevice_];

    enumerate_devices();

    // examine the device list
    if (ndevice_ > 0)
        sprint(founddev);
    for (byte i = 0; i < ndevice_; ++i)
    {
        // print("\t");
        if (flags & VERBOSE)
            printhex(addresses_ + 8 * i, 8);
        print("  ");

        devices_[i].flags = 0;

        // find family
        for (byte f = 0; f < nFamilies_; ++f)
        {
            if (addresses_[8 * i + 0] == OneWireFamilyCodes_[f])
            {
                devices_[i].flags |= OneWireDevice::KNOWN;
                // print('\t');
                sprint(OneWireFamilies_[f]);
                sprint(devcr);
                break;
            }
            if (f == nFamilies_)
            {
                devices_[i].flags &= OneWireDevice::KNOWN;
                sprint(unknownfamily);
                continue;
            }
        }

        // initialize device entry
        devices_[i].output = 0;
        devices_[i].flags |= OneWireDevice::PRESENT;

        // query power status
        com_.reset();
        com_.select(addresses_ + 8 * i);
        com_.write(0xB4);
        if (com_.read() == 0)
        {
            sprint(parasite);
            devices_[i].flags &= OneWireDevice::POWERED;
        }
        else
        {
            sprint(externalpower);
            devices_[i].flags |= OneWireDevice::POWERED;
        }
    }

    // see if any devices are parasite powered
    flags |= (any_parasite() ? ANY_PARASITE : 0);
    conversion_start_ = -1;
}
#endif

// decodes the temp from the 18B20 and returns it as a float
float OneWireBus::decode_temp(byte * data, char family) const
{
    // the readout value is a 2's complement sign-extended number in the two first bytes.
    const int raw_temp = (int(data[1]) << 8) + data[0];

    // The resolution depends on the type of device
    if (family == 0x28)  // DS18B20
    {
        return raw_temp * 0.0625f;
    }
    else if (family == 0x10)  // DS18S20
    {
        // for the DS18S20, we get extended resolution by looking at byte 6 and 7.
        const uint8_t count_remain = data[6];
        const uint8_t count_per_c  = data[7];
        // the base temp for the correction is obtained by *truncating*
        // bit 0. does this mean setting it to zero?
        return (raw_temp & 0xfffe) * 0.5 - 0.25f
               + 1.0f * (count_per_c - count_remain) / count_per_c;
    }
    else if (family == 0x3b)  // MAX31850
    {
        // For MAX31850, the LSB of the first byte is a fault indicator.
        if (data[0] & 0x01)
        {
            return NAN;
        }

        /* The two LSBs are zero but this makes it appear as if the
         * resolution is the same as the DS18B20. */
        return raw_temp * 0.0625f;
    }
    else
    {
        return NAN;
    }
}

void OneWireBus::set_name(int i, const char * name)
{
    if (flags & USE_PGMADDRESS)
        return;

    // free existing name, if any, and copy string
    if ((i < 0) || (i >= ndevice_))
        return;
    if (names_[i])
        free(names_[i]);
    names_[i] = strdup(name);
}

void OneWireBus::start_conversion()
{
    // issue start conversion command to all devices
    com_.reset();
    com_.skip();
    if (flags & ANY_PARASITE)
        com_.write(0x44, 1);  // keep bus high
    else
        com_.write(0x44);
    conversion_start_ = millis();
}

bool OneWireBus::read_scratchpad(uint8_t const addr[8], byte data[9])
{
    uint8_t const presence = com_.reset();
    if (!presence)
    {
        sprint(nodevice);
        return false;
    }
    
    com_.select(addr);
    com_.write(0xBE);  // Read Scratchpad
    
    // read 9 bytes
    for (byte i = 0; i < 9; i++)
    {
        data[i] = com_.read();
    }

    // check crc
    if (OneWire::crc8(data, 8) != data[8])
    {
        if (flags & VERBOSE)
        {
            sprint_pgm(invalid_crc);
            sprint_pgm(reading_scratchpad);
        }
        
        return false;
    }

    // return true for success
    return true;
}

void OneWireBus::readout()
{
    byte    data[9];
    uint8_t addr[8];

    for (byte d = 0; d < ndevice_; ++d)
    {
        if ((device_family(d) < 0) || !(devices_[d].flags & OneWireDevice::PRESENT))
        {
            if (devices_[d].output)
                *devices_[d].output = NAN;
            continue;
        }

        if (flags & USE_PGMADDRESS)
            memcpy_P(addr, paddresses_[d], 8);
        else
            memcpy(addr, addresses_ + 8 * d, 8);

        if (flags & VERBOSE)
        {
            sprint_pgm(PSTR("Reading probe "));
            printhex(addr, 8);
            print(": ");
        }

        bool const success = read_scratchpad(addr, data);
        float temp;
        if (success)
        {
            temp = decode_temp(data, addr[0]);
        }
        else
        {
            temp = NAN;
        }
        if (flags & VERBOSE)
        {
            if (!isnan(temp))
                print(temp);
            else
                sprint_pgm(PSTR("NaN"));
            sprint_pgm(PSTR("\r\n"));
        }

        if (temp == temp)
        {
            devices_[d].failcount = 0;
            if (devices_[d].output)
                *devices_[d].output = temp;
        }
        else
        {
            if (devices_[d].failcount < failtimeout_)
                devices_[d].failcount++;
            else if (devices_[d].output)
                *devices_[d].output = temp;
        }
    }
}

// Returns true if we are ready to read out
bool OneWireBus::update_ready() const
{
    if (ndevice_ == 0)
        return true;

    // see if millis counter has wrapped, in that case we must start
    // over to make sure conversion is done (we could be more intelligent)
    if (millis() < conversion_start_)
    {
        conversion_start_ = millis();
        return false;
    }

    if (conversion_start_ == 0)
        return false;

    return ((conversion_start_ + 760UL) < millis());
}

// returns true if the temps were updated, false if we are still
// waiting for conversion to complete.
bool OneWireBus::update(bool block)
{
    // there are two options: wait 750ms to make sure all devices are
    // done converting, or, if they are all externally powered, poll
    // them all (actually it seems you can't poll them unless you do so
    // immediately after issuing the conversion command). Since we don't
    // know how many there are and we're not really concerned with
    // speed, we just wait 750ms.

    // if first conversion, just start it
    if (conversion_start_ == 0)
        start_conversion();

    if (!update_ready())
    {
        if (block)
            while (!update_ready())
                ;
        else
            return false;
    }

    readout();
    start_conversion();
    return false;
}

uint8_t OneWireBus::device_family(byte i) const
{
    if (devices_[i].flags & OneWireDevice::KNOWN)
    {
        if (flags & USE_PGMADDRESS)
            return pgm_read_byte(paddresses_[i]);
        else
            return addresses_[8 * i + 0];
    }
    else
        return -1;
};

bool OneWireBus::any_fail() const
{
    bool fail = false;
    for (int i = 0; i < n_devices(); ++i)
        fail |= (devices_[i].failcount == failtimeout_);
    return fail;
}
