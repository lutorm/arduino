/* OneWireBus -- class to keep track of a number of 1-wire temp probes
   and read out temperatures. */

#pragma once

#include <Arduino.h>
#include <OneWire.h>
#include <avr/pgmspace.h>
#include <memory.h>

// struct keeping info about connected devices
struct OneWireDevice
{
    enum Flags
    {
        PRESENT = 4,
        POWERED = 2,
        KNOWN   = 1
    };

    byte flags;
    int  failcount;
    // for temp probes, the address to save output
    float * output;
};

// helper function to print a number as hex
void printhex(const uint8_t * const data, byte len);
void printpgmhex(const prog_uint8_t * const data, byte len);
bool compare_address(const uint8_t * a, const uint8_t * b);
bool compare_pgmaddress(const uint8_t * a, const prog_uint8_t * b);

class OneWireBus
{
private:
    // family id codes
    static const int    nFamilies_ = 3;
    static const byte   OneWireFamilyCodes_[];
    static const char * OneWireFamilies_[];

    enum Flags { VERBOSE = 1, ANY_PARASITE = 2, USE_PGMADDRESS = 4};
    uint8_t flags;
    int  failtimeout_;

    OneWire com_;  // the onewire communications object

    // identified devices on the bus
    int             ndevice_;
    OneWireDevice * devices_;

    // The behavior of these two is determined by the use_pgmaddress_ flag.
    union {
        // Array of predefined device addresses.
        const prog_uint8_t ** paddresses_;
        // Array of device addresses found by search. Since they are
        // always 8 long they are just stacked in there.
        uint8_t * addresses_;
    };
    union {
        // Array of predefined device names.
        const prog_char ** pnames_;
        // Array of device names
        char ** names_;
    };

    // for parasite powered devices, we need to know when we issued
    // start conversion command
    mutable unsigned long conversion_start_;

/* The following functions require the onewire search functions. */
#if ONEWIRE_SEARCH
    int   get_n_devices();
    void  check_device_presence();
    void  enumerate_devices();
#endif

    /* Select device and read out scratchpad. Return true if data with
     * correct CRC was read, false is CRC mismatch or no presence
     * pulse on bus reset. */
    bool read_scratchpad(uint8_t const addr[8], byte data[9]);
    
    float decode_temp(byte * data, char family) const;

    template<typename T>
    void print(const T & item) const
    {
        if (flags & VERBOSE)
            Serial.print(item);
    };
    template<typename T, typename R>
    void print(const T & item, const R & type) const
    {
        if (flags & VERBOSE)
            Serial.print(item, type);
    };
    void sprint(const prog_char * s) const
    {
        if (flags & VERBOSE)
            sprint_pgm(s);
    };

    uint8_t device_family(byte i) const;

public:
    // Constructor, takes pin to read devices on, scans for devices.
# if ONEWIRE_SEARCH    
    OneWireBus(int, bool = true);
#endif
    
    // Constructor, takes pin but also pgmspace arrays of device addresses and names to
    // monitor
    OneWireBus(int, const prog_uint8_t **, const prog_char **, int, int, bool = true);

    // returns true if there are any parasite powered probes on the bus
    bool any_parasite() const;

    int             n_devices() const { return ndevice_; }
    
    const uint8_t * address(int i) const
    {
        if (flags & USE_PGMADDRESS)
            return 0;
        else
            return addresses_ + 8 * i;
    }
    bool is_temp_probe(int i) const { return device_family(i) >= 0; }

    const prog_char * devicetype(int i) const
    {
        return (device_family(i) >= 0) ? OneWireFamilies_[device_family(i)] : "";
    }

    // give the device a name
    void set_name(int i, const char * name);

    // get the device name
    const char * get_name(int i) const
    {
        if (flags & USE_PGMADDRESS)
            return 0;
        else
            return names_[i];
    }

    // set the verbose mode
    void set_verbose(bool v) { if (v) flags |= VERBOSE; else flags &= ~VERBOSE;
    }
    
    // define the location of the output variable
    void set_output(int i, float * var)
    {
        if ((i >= 0) && (i < ndevice_))
            devices_[i].output = var;
    }

    // Returns true if an update is ready to process.
    bool update_ready() const;

    // this updates the outputs with the read temps. if a new value is
    // available, it returns true otherwise false. If block is true, it
    // will block waiting for the update to be done. After reading,
    // starts a new conversion.
    bool update(bool block = false);

    /// Reads all devices, without starting a new conversion.
    void readout();

    /// Issues CONVERT_TEMP command to all devices (without reading out)
    void start_conversion();

    /// Returns true if any of the temperature probes have reached the max number fo fails
    bool any_fail() const;
};
