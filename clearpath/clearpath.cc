/*
 * This code interfaces the feedback from the Clearpath servo motors
 * with the G2 running on the Due. The main function is to monitor the
 * status feedback from the servos and command a rapid feedhold in
 * case one of them shuts down for some reason. It can also drive a
 * few status LEDs.
 *
 * Unlike with steppers, where a lost step will just lose position
 * slightly but largely keep going, the servos will not lose
 * position. However, if they for some reason can't keep the position
 * within specified limits (due to torque saturation), they will
 * stop. Since having one motor stop and the others keep going is a
 * sure recipe for a big crash, it is imperative that the motion for
 * all motors be stopped in this situation.
 *
 * The program will monitor the enable output to the servos, and the
 * feedback from the servos. If the motor enable is high but the
 * feedback indicates the motor is disabled, for any of the motors,
 * the code will raise a pin hooked up to a feedhold input on G2.
 *
 * Input pins:
 * M1 enable: D5 / PD5 / PCINT21 (the G2 output pin so is high even if 12V power is off)
 * M2 enable: D6 / PD6 / PCINT22
 * M3 enable: D7 / PD7 / PCINT23
 * M1 feedback: D2 / PD2 / INT0 / PCINT18
 * M2 feedback: D3 / PD3 / INT1 / PCINT19 / OC2B
 * M3 feedback: D8 / PB0 / PCINT0 / ICP1
 * Motor power: A1 / PC1 / PCINT9
 *
 * Output pins:
 * G2 Feedhold: D4 / PD4 hooked up to G2 pin 82/A4 / G2 pin "Amin"
 * M1 shutdown LED: D9 / PB1 / OC1A
 * M2 shutdown LED: D10 / PB2 / OC1B
 * M3 shutdown LED: D11 / PB3 / OC2A
 * M1 torque LED: D12 / PB4
 * M2 torque LED: D13 / PB5
 * M3 torque LED: A3 / PC3
 */

#include <Arduino.h>
#include <avr/interrupt.h>
#include <avr/power.h>
#include <avr/sleep.h>
#include <avr/wdt.h>
#include <inttypes.h>
#include <memory.h>

bool verbose=false;

/* State machine state of the motors:
 * 0: Poweroff. Motor power is off.
 * 1: Poweron. Motor power has just turned on and we're waiting for feedback.
 * 2: Disabled. Enable is low but power is on.
 * 3: Just enabled. Enable went high but feedback has not yet gone high.
 * 4: Enabled_fbhi. Motor is enabled, feedback is high.
 * 5: Enabled_fblo. Motor is enabled, feedback is low.
 * 6: Alarm. Enable is high but feedback has gone low (for too long, or never went high
 * initially.)
 * 7: ASG. Enable is high and feedback has timed out high. This means
 * the motor is idle. This is a special state because we stop timing
 * at that point because the counter will overflow.
 */
volatile uint8_t motor_state[3];
uint8_t  last_motor_state[3];
uint8_t const    POWEROFF     = 0;
uint8_t const    POWERON      = 1;
uint8_t const    DISABLED     = 2;
uint8_t const    JUST_ENABLED = 3;
uint8_t const    ENABLED_FBHI = 4;
uint8_t const    ENABLED_FBLO = 5;
uint8_t const    ALARM        = 6;
uint8_t const    ASG          = 7;

char * const  state_names[] = {"POWEROFF",
                              "POWERON",
                              "DISABLED",
                              "JUST_ENABLED",
                              "ENABLED_FBHI",
                              "ENABLED_FBLO",
                              "ALARM",
                              "ASG"};
uint8_t const nstates = sizeof(state_names) / sizeof(char *);

// Current states of the pins, for recognizing changes.
volatile bool enable_state[3];
volatile bool feedback_state[3];
volatile bool power_state;

// Counts how many times the timer has overflowed. This is the tick
// used to time the feedback PWM and is checked by the pin change interrupts.
volatile uint16_t tick;

// Tick counts when the feedback pin went high and low. Used to back
// out PWM percentage. Note that because we have to both detect a 45Hz
// PWM and output a PWM, we don't use the timer counts, the timer
// counts faster and we count overflows.
volatile uint16_t fbhi_tick[3];
volatile uint16_t fblo_tick[3];

// The calculated motor torque from PWM measurement. The PWM gives a
// signed torque value but we don't care about the sign so this is 0 -
// 255 for max torque.
volatile uint8_t motor_torque[3];
volatile float   float_torque[3];

// State machine messages, set by the ISRs so they can be printed in the loop.
// -1 means no message.
volatile int8_t sm_msg[3];

// Timer ticks before we conclude feedback is no longer 45Hz PWM.
uint16_t const motor_timeout = 350;
// Timer ticks before we raise alarm when enable comes on. Motors seem to be slow to respond sometimes so allow three periods.
uint16_t const enable_timeout = motor_timeout*3;
// Timer ticks before we raise alarm when power comes on. Allow 2 second.
uint16_t const poweron_timeout = 15700*2;

// Min LED PWM when the motor is enabled.
uint8_t const min_led_pwm = 20;

/* Error strings */
char * const sm_msgs[] =
    {"Enable was supposed to be low but enable went low now",
     "In poweron/just_enabled/enabled/alarm but enable went high now",
     "Unknown state",
     "In just_enabled but feedback went low now",
     "In enabled_fbhi/ASG but feedback went high now",
     "Feedback state changed in alarm state",
     "Feedback asserted in disabled state",
     "In enabled_fblo but feedback went low now",
     "PWM torque calc returned >1",
     "In poweroff but feedback went high"};

inline void torque_led_on(uint8_t motor)
{
    PORTB |= (1 << (1 + motor));
}

inline void torque_led_off(uint8_t motor)
{
    PORTB &= ~(1 << (1 + motor));  // Clear shutdown LED
}

inline void shutdown_led_on(uint8_t motor)
{
    switch (motor)
    {
    case 0:
        PORTB |= 0x10;
        break;
    case 1:
        PORTB |= 0x20;
        break;
    case 2:
        PORTC |= 0x08;
        break;
    }
}

inline void shutdown_led_off(uint8_t motor)
{
    switch (motor)
    {
    case 0:
        PORTB &= ~0x10;
        break;
    case 1:
        PORTB &= ~0x20;
        break;
    case 2:
        PORTC &= ~0x08;
        break;
    }
}

void calculate_torque(uint8_t motor)
{
    /* 50% PWM = zero torque, 5% and 95% are full peak torque in the two dirs. */
    float frac =
        2
        * (float(fblo_tick[motor] - fbhi_tick[motor]) / (tick - fbhi_tick[motor]) - 0.5);

    if (frac < -1 || frac > 1)
    {
        sm_msg[motor] = 8;
        frac          = 1.0;
    }
    float_torque[motor] = frac;

    /* For the LED PWM, we need to worry about RMS torque as well as
     * peak torque, so we use a proportional scaling. We also want a
     * min amount so the LEDs always indicate that the motor is
     * enabled. */
    frac                = (frac >= 0) ? frac : -frac;
    motor_torque[motor] = frac * (255 - min_led_pwm) + min_led_pwm;
}

void power_change(uint8_t motor, bool new_state)
{
    if (new_state)
    {
        /* Powered on. */
        if (enable_state[motor])
        {
            motor_state[motor] = POWERON;
            /* We count this as "feedback gone lo" for the purpose of timeout. */
            fblo_tick[motor] = tick;
        }
        else
        {
            /* If we're powered on but enable is low, immediately go
             * into disabled. */
            motor_state[motor] = DISABLED;
        }
    }
    else
    {
        /* If we power off, clear any LED and feedholds. */
        motor_state[motor] = POWEROFF;
        shutdown_led_off(motor);
        PORTD &= ~0x10;  // Clear feedhold
        float_torque[motor] = 0.0;
        motor_torque[motor] = 0;
    }
}

void enable_change(uint8_t motor, bool new_state)
{
    switch (motor_state[motor])
    {
    case POWEROFF:
        /* Do nothing. */
        break;
    case DISABLED:
        if (new_state)
        {
            motor_state[motor] = JUST_ENABLED;
            shutdown_led_off(motor);
            /* We count this as "feedback gone lo" for the purpose of timeout. */
            fblo_tick[motor] = tick;
        }
        else
        {
            sm_msg[motor] = 0;
        }
        break;
    case POWERON:
    case JUST_ENABLED:
    case ENABLED_FBHI:
    case ENABLED_FBLO:
    case ALARM:
    case ASG:
        if (!new_state)
        {
            motor_state[motor] = DISABLED;
            PORTD &= ~0x10;  // Clear feedhold
            shutdown_led_off(motor);
            float_torque[motor] = 0.0;
            motor_torque[motor] = 0;
        }
        else
        {
            sm_msg[motor] = 1;
        }
        break;
    default:
        sm_msg[motor] = 2;
    }
}


void feedback_change(uint8_t motor, bool new_state)
{
    switch (motor_state[motor])
    {
    case POWEROFF:
        // In poweroff, we don't care about the feedback pin. It may de-assert
        // shortly after coming here but that's not actionable. It should never assert.
        if (new_state)
        {
            sm_msg[motor] = 9;
        }
        break;
    case DISABLED:
        // In disabled, we don't care about the feedback pin. It may de-assert
        // shortly after coming here but that's not actionable. It should never assert.
        if (new_state)
        {
            sm_msg[motor] = 6;
        }
        break;
    case POWERON:
    case JUST_ENABLED:
        if (new_state)
        {
            /* We're stepping into the PWM measurement, so just save the tick. */
            motor_state[motor] = ENABLED_FBHI;
            fbhi_tick[motor]   = tick;
        }
        else
        {
            sm_msg[motor] = 3;
        }
        break;
    case ENABLED_FBHI:
    case ASG:
        if (!new_state)
        {
            /* Feedback went low, we only use this event to save off the tick. */
            if (motor_state[motor] == ASG)
            {
                /* Feedback went low for the first time in a long
                 * time. Because the count may overflow, we set both
                 * fbhi and fblo here, so we get zero torque on the first
                 * cycle. */
                fbhi_tick[motor] = tick;
            }
            motor_state[motor] = ENABLED_FBLO;
            fblo_tick[motor]   = tick;
        }
        else
        {
            sm_msg[motor] = 4;
        }
        break;
    case ENABLED_FBLO:
        if (new_state)
        {
            /* Feedback went hi in FBLO state. This means we can back out a PWM
             * measurement. */
            motor_state[motor] = ENABLED_FBHI;
            calculate_torque(motor);
            fbhi_tick[motor] = tick;
        }
        else
        {
            sm_msg[motor] = 7;
        }
        break;
    case ALARM:
        sm_msg[motor] = 5;
        break;
    default:
        sm_msg[motor] = 2;
    }

    feedback_state[motor] = new_state;
}

/* External interrupt 0 triggers on M1 feedback. Can't define these as
 * ISRs directly because Arduino library does that to support
 * attachInterrupt(); */
void isr_int0()  // ISR(INT0_vect)
{
    feedback_change(0, PIND & (1 << 2));
}

/* External interrupt 1 triggers on M2 feedback. */
void isr_int1()  // ISR(INT1_vect)
{
    feedback_change(1, PIND & (1 << 3));
}

/* Pin Change interrupt 0, triggers on changes to pins
 * PCINT7..0. We've masked it so it only triggers on M3 feedback on
 * PCINT0. */
ISR(PCINT0_vect)
{
    feedback_change(2, PINB & 1);
}

/* Pin Change interrupt 1, triggers on changes to pins
 * PCINT16..8. We've masked it so it only triggers on power input on
 * PCINT9. */
ISR(PCINT1_vect)
{
    power_state = PINC & 0x02;
    for (uint8_t i = 0; i < 3; ++i)
    {
        power_change(i, power_state);
    }
}

/* Pin change interrupt 2, triggers on changes to pins
 * PCINT23..16. We've masked it so it only triggers on any of the
 * enable pins. */
ISR(PCINT2_vect)
{
    /* Determine which pin changed value. */
    uint8_t const pd = PIND;
    for (uint8_t i = 0; i < 3; ++i)
    {
        bool const new_state = (pd & (1 << (5 + i)));
        if (new_state != enable_state[i])
        {
            enable_change(i, new_state);
            enable_state[i] = new_state;
        }
    }
}

/* Timer timed out. We use this to detect a constant feedback, either
 * high or low. The former is harmless, it means we're idle and torque
 * is zero. The latter means the motor has shut down. */
void check_motor_timeout(uint8_t motor)
{
    uint8_t const current_state = motor_state[motor];
    auto const    timeout = (current_state == POWERON) ? poweron_timeout : (current_state == JUST_ENABLED ? enable_timeout : motor_timeout);

    // check whether feedback is high or low
    if (!feedback_state[motor]
	&&(tick - fblo_tick[motor] > timeout)
        && (current_state == ENABLED_FBLO
	    || current_state == JUST_ENABLED
            || current_state == POWERON))
    {
        // We've timed out with feedback low. Alarm.
        PORTD |= 0x10;  // Command feedhold
        motor_state[motor] = ALARM;
        float_torque[motor] = 0.0;
        motor_torque[motor] = 0.0;
        shutdown_led_on(motor);
    }

    if (feedback_state[motor]
	&& (tick - fbhi_tick[motor] > motor_timeout)
	&& (current_state == ENABLED_FBHI
	    || current_state == JUST_ENABLED
	    || current_state == POWERON))
    {
        // We've timed out with feedback high. This means torque is zero.
        motor_state[motor]  = ASG;
        float_torque[motor] = 0.0;
        motor_torque[motor] = min_led_pwm;
    }
}

/* Timer 2 controls the PWM on the M3 torque LED, in addition to being
 * our tick counter. */
ISR(TIMER2_OVF_vect)
{
    OCR2A = motor_state[2] == DISABLED ? 0 : motor_torque[2];

    check_motor_timeout(0);
    check_motor_timeout(1);
    check_motor_timeout(2);
    ++tick;
}

/* Timer 1 controls the PWM on the M1/M2 torque LEDs. We update the
 * PWM values from the timer overflow interrupt to avoid missing
 * cycles when the value changes. Timer1 is set to 8-bit phase-correct
 * PWM so we write an 8-bit value here. */
ISR(TIMER1_OVF_vect)
{
    OCR1A = motor_state[0] == DISABLED ? 0 : motor_torque[0];
    OCR1B = motor_state[1] == DISABLED ? 0 : motor_torque[1];
}

bool torque_led_state(uint8_t motor)
{
    switch (motor)
    {
    case 0:
        return PINB & 0x10;
    case 1:
        return PINB & 0x20;
    case 2:
        return PINC & 0x08;
    }
    return false;
}

void startup_blink()
{
    int duration = 100;
    shutdown_led_on(0);
    delay(duration);
    shutdown_led_off(0);
    shutdown_led_on(1);
    delay(duration);
    shutdown_led_off(1);
    shutdown_led_on(2);
    delay(duration);
    shutdown_led_off(2);
    torque_led_on(0);
    delay(duration);
    torque_led_off(0);
    torque_led_on(1);
    delay(duration);
    torque_led_off(1);
    torque_led_on(2);
    delay(duration);
    torque_led_off(2);
}

void print_motor_state(uint8_t motor, uint8_t state, uint8_t last_state)
{
    if (verbose || state != last_state)
      {
	sprint_pgm(PSTR("Motor "));
	Serial.print(motor);
	sprint_pgm(PSTR(" power: "));
	Serial.print(power_state);
	sprint_pgm(PSTR(" enable: "));
	Serial.print(enable_state[motor]);
	sprint_pgm(PSTR(" feedback: "));
	Serial.print(feedback_state[motor]);
	if (state == last_state)
	  {
	    sprint_pgm(PSTR(" state "));
	  }
      }
    if (state != last_state)
      {
	sprint_pgm(PSTR(" state changed to "));
      }
    if (verbose || state != last_state)
      {
	Serial.println(state_names[state]);
      }
}

void setup()
{
    Serial.begin(57600);

    sprint_pgm(PSTR("\n\n\n\nPatrik's ClearPath interface booting\n\r"));

    // configure sleep mode
    set_sleep_mode(SLEEP_MODE_IDLE);

    // Set all pins to low. For inputs, we do not want pullups.
    PORTB = 0x0;
    PORTC = 0x0;
    PORTD = 0x0;

    // Define pin directions
    // Port B, PB0 is input, PB1..5 outputs.
    DDRB = 0x3e;
    // Port C, PC3 is output, all other inputs
    DDRC = 0x08;
    // Port D, PD4 is output, all other inputs
    DDRD = 0x10;

    // Make sure digital buffers are enabled on Port C. Disable ADC.
    DIDR0  = 0x00;
    ADCSRA = 0x00;

    // Play LEDs
    startup_blink();

    /* Disable interrupts until everything is set up. */
    cli();

    // Set external interrupts to trigger on any pin change
    EICRA = 0x05;
    EIMSK = 0x03;

    // Enable all 3 pin change interrupts.
    PCICR = 0x07;
    // Mask out all pins except PCINT21..23, 9, and 0.
    PCMSK2 = 0xe0;
    PCMSK1 = 0x02;
    PCMSK0 = 0x01;

    attachInterrupt(0, isr_int0, CHANGE);
    attachInterrupt(1, isr_int1, CHANGE);

    // Set up timer 1 and its ISR.
    // Timer 1 to phase correct 8-bit PWM mode, OC1A/B in "non-inverting PWM mode",
    // prescaler 1. Timer overflow interrupt enabled. This gives a 15.7kHz interrupt which
    // we use to update the PWM.
    TCCR1A = _BV(COM1A1) | _BV(COM1B1) | _BV(WGM10);
    TCCR1B = _BV(CS10);
    TIMSK1 = _BV(TOIE1);
    power_timer1_enable();

    // Set up timer 2 and its ISR.  Timer 2 to phase correct PWM mode,
    // OC2A in "non-inverting PWM mode", OC2B disconnected, prescaler
    // 1.  Timer overflow interrupt enabled. This gives a 15.7kHz
    // interrupt which we use as a tick to measure feedback PWM and
    // check if feedback timed out. We also update the PWM value here.
    TCCR2A = _BV(COM2A1) | _BV(WGM20);
    TCCR2B = _BV(CS20);
    TIMSK2 = _BV(TOIE2);
    power_timer2_enable();

    // Power reduction stuff

    // set the power reduction register to disable timers 0 & 1
    // (timer 0 is for the tick) and SPI.
    power_timer0_disable();
    power_spi_disable();

    // disable analog comparator
    ACSR = 0x80;

    // Turn off watchdog timer
    wdt_disable();

    // Init state machine before we start interrupts
    tick = 0;
    power_state = PINC & 0x02;
    for (uint8_t i = 0; i < 3; ++i)
    {
        enable_state[i] = PIND & (1 << (5 + i));
        switch (i)
        {
        case 0:
            feedback_state[i] = PIND & (1 << 2);
            break;
        case 1:
            feedback_state[i] = PIND & (1 << 3);
            break;
        case 2:
            feedback_state[i] = PINB & 1;
            break;
        }
        sm_msg[i]    = -1;
        fblo_tick[i] = tick;
        fbhi_tick[i] = tick;

        if (power_state)
        {
            if (!enable_state[i])
            {
                motor_state[i]  = DISABLED;
            }
            else
            {
                /* If the motor is enabled, we init the state machine into
                 * POWERON. We don't want to immediately fault out because
                 * we've missed some transition at init. */
                motor_state[i]  = POWERON;
            }
        }
        else
        {
            motor_state[i]  = POWEROFF;
        }
        motor_torque[i] = 0;
        float_torque[i] = 0.0;
    }

    // Print initial states before interrupts are enabled
    for (uint8_t i = 0; i < 3; ++i)
    {
      last_motor_state[i] = motor_state[i];
    }

    sei();
   
    for (uint8_t i = 0; i < 3; ++i)
    {
      print_motor_state(i, last_motor_state[i], last_motor_state[i]);
    }
}

void loop()
{
    // Everything's done by the interrupts. We just spin to log state
    // changes here.
    uint16_t last_tick[3];
    for (uint8_t i = 0; i < 3; ++i)
    {
      last_tick[i]        = tick;
    }

    while (true)
    {
        for (uint8_t i = 0; i < 3; ++i)
        {
            uint16_t const period       = 15686;
            uint16_t const current_tick = tick;

            uint8_t const state = motor_state[i];
            bool const    motor_on =
                state == ENABLED_FBHI || state == ENABLED_FBLO || state == ASG;
            if (((last_motor_state[i] != state) &&
                /* don't log feedback transitions */
                !(motor_on
                  && (
		      (state == ENABLED_FBLO && last_motor_state[i] == ENABLED_FBHI)
		      || (state == ENABLED_FBHI && last_motor_state[i] == ENABLED_FBLO)))) 
		|| (current_tick - last_tick[i] > period))
            {
	      print_motor_state(i, state, last_motor_state[i]);
            }
            last_motor_state[i] = state;

            int8_t const msg = sm_msg[i];
            if (msg >= 0)
            {
                sprint_pgm(PSTR("Motor "));
                Serial.print(i);
                sprint_pgm(PSTR(" SM ERROR: "));
                Serial.println(sm_msgs[msg]);
                sm_msg[i] = -1;
		print_motor_state(i, state, last_motor_state[i]);
            }

            // Print torque values if motors are enabled
            if (verbose && !torque_led_state(i))
            {
                if (current_tick - last_tick[i] > period)
                {
                    if (motor_on)
                    {
                        sprint_pgm(PSTR("Motor "));
                        Serial.print(i);
                        sprint_pgm(PSTR(" torque "));
                        Serial.println(float_torque[i]);
                    }
                }
            }
	    if (current_tick - last_tick[i] > period)
	      {
		last_tick[i] = current_tick;
	      }
	}
    }
}
