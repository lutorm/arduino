#include "AltSoftSerial.h"
#include "memory.h"
#include <Arduino.h>
#include <avr/power.h>
#include <avr/sleep.h>
#include <avr/wdt.h>

AltSoftSerial *  sma_serial;
HardwareSerial * xbee_serial = &Serial;

uint8_t const rs485_dir_pin = 7;
// AltSoftSerial uses external pin interrupts so is fixed to use these
// on 328
uint8_t const sma_rx_pin = 8;
uint8_t const sma_tx_pin = 9;

ISR(BADISR_vect)
{
    for (int i = 0; i < 0xffff; ++i)
    {
        digitalWrite(LED_BUILTIN, true);
        digitalWrite(LED_BUILTIN, false);
    }
}

/* Configure interrupts and setup hardware for minimum power
 * consumption.
 */

void setup_interrupts()
{
    // we want to disable all pin change interrupts
    PCICR  = 0x04;
    PCMSK2 = 0x00;

    // also set the power reduction register to disable timers 2
    // (timer 0 is for the tick and AltSoftSerial uses timer 1) and SPI.
    power_timer2_disable();
    power_spi_disable();

    // disable analog comparator
    ACSR = 0x80;

    // Turn off watchdog timer
    wdt_disable();
}

void setup()
{
    xbee_serial->begin(19200);

    digitalWrite(LED_BUILTIN, true);
    delay(100);
    digitalWrite(LED_BUILTIN, false);
    xbee_serial->print("\n\n\n\nPatrik's inverter relay booting\nfirmware version: ");
    xbee_serial->println();

    setup_interrupts();

    // configure all pins to inputs with pullups to minimize power consumption
    for (uint8_t p = 0; p < 13; ++p)
    {
        pinMode(p, INPUT_PULLUP);
    }
    pinMode(A0, INPUT_PULLUP);
    pinMode(A1, INPUT_PULLUP);
    pinMode(A2, INPUT_PULLUP);
    pinMode(A3, INPUT_PULLUP);
    pinMode(A4, INPUT_PULLUP);
    pinMode(A5, INPUT_PULLUP);
    pinMode(A6, INPUT_PULLUP);
    pinMode(A7, INPUT_PULLUP);

    // set the pins we want as putput (altsoftserial deals with its pins)
    pinMode(rs485_dir_pin, OUTPUT);

    // configure sleep mode
    set_sleep_mode(SLEEP_MODE_IDLE);

    sma_serial = new AltSoftSerial();
    sma_serial->begin(1200);
}

void send_buf(char * buf, int size)
{
    for (int i = 0; i < size; ++i)
    {
        xbee_serial->write(buf[i]);
    }
}

void loop()
{
    const int bufsize = 256;
    char      buf[bufsize];
    int       i;

wait:
    // nothing to send, listen on both ports
    digitalWrite(rs485_dir_pin, false);
    while (true)
    {
        if (sma_serial->available())
            goto receive_inverter;
        if (xbee_serial->available())
            goto receive_xbee;
    }

receive_inverter:
    // incoming data from inverter, read it and send it to
    // xbee. Because it comes in much slower than we send, we buffer
    // it first, then send it. That way it should go in one packet.
    digitalWrite(rs485_dir_pin, false);
    digitalWrite(LED_BUILTIN, HIGH);
    i = 0;
    while (sma_serial->available())
    {
        buf[i++] = sma_serial->read();
        if (i == bufsize)
        {
            send_buf(buf, i);
            i = 0;
        }
    }
    send_buf(buf, i);

    digitalWrite(LED_BUILTIN, LOW);
    goto wait;

receive_xbee:
    // incoming data from xbee, read it and send it to inverter
    digitalWrite(rs485_dir_pin, true);
    // SMAnet mandates no activity on pin for 30ms so just to be sure,
    // we wait 30ms after turning the transmitter on
    delay(31);
    while (xbee_serial->available())
    {
        sma_serial->write(xbee_serial->read());
    }
    // wait until send has completed and then switch rs485 line back to receive. A
    // response will come 30ms after the inquiry so we must turn the transmitter off
    // before then, but we give it a few ms to make sure the recipient doesn't get
    // confused.
    sma_serial->flushOutput();
    delay(5);

    goto wait;
}
