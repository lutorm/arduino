#include "OnewireTemps.h"
#include "serial_data.h"

namespace
{
    const prog_uint8_t a1[] PROGMEM = {0x28, 0x7C, 0x93, 0x72, 0x02, 0x00, 0x00, 0xE2};
    const prog_uint8_t a2[] PROGMEM = {0x28, 0xC2, 0x54, 0x72, 0x02, 0x00, 0x00, 0x85};
    const prog_uint8_t a3[] PROGMEM = {0x28, 0x6C, 0x2B, 0x72, 0x02, 0x00, 0x00, 0xC8};
    const prog_uint8_t a4[] PROGMEM = {0x28, 0x3B, 0xF0, 0x71, 0x02, 0x00, 0x00, 0xC5};
    const prog_uint8_t a5[] PROGMEM = {0x28, 0xC2, 0xBB, 0x72, 0x02, 0x00, 0x00, 0x30};
    const prog_char    n1[] PROGMEM = "Top probe";
    const prog_char    n2[] PROGMEM = "Bottom probe";
    const prog_char    n3[] PROGMEM = "Heater probe";
    const prog_char    n4[] PROGMEM = "Ambient";
    const prog_char    n5[] PROGMEM = "Enclosure";

    const prog_uint8_t * addresses[OnewireTemps::n_devs] = {a1, a2, a3, a4, a5};
    const prog_char *    names[OnewireTemps::n_devs]     = {n1, n2, n3, n4, n5};

}  // namespace

OnewireTemps::OnewireTemps(bool verbose) :
    Periodic(1000), _bus(onewirepin, addresses, names, n_devs, 1, verbose)
//_bus(onewirepin, verbose)
{
    for (int i = 0; i < n_devs; ++i)
    {
        _temps[i] = NAN;
        _bus.set_output(i, &_temps[i]);
    }

    /* @todo There is a bug in any_parasite.
    if (_bus.any_parasite())
    {
        sprint_pgm(PSTR("OnewireTemps can't handle parasite-powered temperature
    probes\n"));
    }
    */
}

void OnewireTemps::_dispatch(ms_t now)
{
    /* Because we know that it'll take 760ms to convert temp, we can
     * safely read a measurement and then start a new conversion at
     * our period. */
    _bus.readout();
    _bus.start_conversion();
}

void OnewireTemps::send_data(uint16_t & crc)
{
    for (int i = 0; i < _bus.n_devices(); ++i)
    {
        uint8_t addr[8];
        memcpy_P(addr, addresses[i], 8);

        ::send_data(addr, _temps[i], crc);
    }
}

const prog_char * name(uint8_t i)
{
    return names[i];
}
