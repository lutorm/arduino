#pragma once

#include "EventLoop.h"
#include <avr/pgmspace.h>
#include <inttypes.h>

/* Class monitors external interrupt 0 and
 * counts pin changes to calculate RPM. The counts are evaluated when
 * timer 1 overflows, so the time for that needs to be supplied in the
 * constructor. Timer 1 overflow interrupt needs to be enabled. */

class Fantach : public Periodic
{
public:
    /* ppr is pulses per revolution, typically 2. */
    Fantach(float t_ovf, int ppr);

    float const & rpm() const { return _rpm; }

    void _dispatch(ms_t) override;

    void send_data(uint16_t & crc);

private:
    /* 64-bit address to send fan RPMS with. fan number is put in the last char. */
    static const prog_uint8_t _address[];

    /* Storage for the calculated RPM. Copied over from the volatile in dispatch. */
    float _rpm;
};
