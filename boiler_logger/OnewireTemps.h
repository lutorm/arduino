#pragma once

#include "EventLoop.h"
#include "onewiretemp.h"
#include <inttypes.h>

/* Class periodically reads temperature probes over 1-wire. Note that
 * this class cannot be initialized statically. */
class OnewireTemps : public Periodic
{
public:
    OnewireTemps(bool verbose);

    void _dispatch(ms_t now) override;

    // Make an enum with symbolic names to the probes
    enum class Probes : uint8_t
    {
        Top = 0,
        Bottom,
        Heater,
        Ambient,
        Enclosure,
        n_devs
    };

    float const & temp(Probes i) { return _temps[uint8_t(i)]; }

    const prog_char * name(Probes i);

    void send_data(uint16_t & crc);

    static uint8_t constexpr n_devs = static_cast<uint8_t>(Probes::n_devs);

private:
    static uint8_t constexpr onewirepin = 9;

    float _temps[n_devs];

    OneWireBus _bus;
};
