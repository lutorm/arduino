#include "EventLoop.h"
#include "Fantach.h"
#include "OnewireTemps.h"
#include "PwmPID.h"
#include "Timekeeper.h"
#include "memory.h"
#include "serial_data.h"
#include <Arduino.h>
#include <math.h>

/* Timer2 runs with prescaler 1 so a period of 256 ticks. */
// Timekeeper time(256);

/* Fan tach uses timer1 at prescaler 64, 2 pulses per rev. */
Fantach fan_tach(65535. * 64 / F_CPU, 2);

OnewireTemps * onewire;

const int commLed = 12;

/* The fan controller tries to keep the heater temperature at this
 * value. This is a compromise between noise and temperature
 * homogeniety. */
float fan_setpoint = 70;
// float dummy =80;

// const bool verbose=true;
const bool verbose = false;

/* Heater runs slow PWM on Arduino pin 10, PB2/OC1B. Fan PWM runs
 * fast PWM on timer 2, Arduino pin 11, PB3/OC2A. */
class HeaterPID : public PwmPID<decltype(OCR1B)>
{
public:
    HeaterPID() : PwmPID(OCR1B, &hotbox_temp, &hotbox_setpoint, heater_pid_name)
    {
        SetTunings(5.0f / 255, 0.1f / 255, 0.0f);
        SetEngaged(true);
        set_period(5000);
        SetTimeout(30);
    }

    static prog_uint8_t const heater_pid_name[];

    /* openloop gain is recalculated from old value of 10 which was
     * for a full power of 255 to this class which uses a full power
     * of 1.  */
    static constexpr float hotbox_openloop_gain = 10. / 255;

    float hotbox_setpoint = 34;

    /* Controller input temperature. */
    float hotbox_temp;

    void _dispatch(ms_t now) override
    {
        /* The controller input is the mean of the two temps, so we
         * calculate this here before dispatching the controller. */
        hotbox_temp = 0.5
                      * (onewire->temp(OnewireTemps::Probes::Top)
                         + onewire->temp(OnewireTemps::Probes::Bottom));

        /* If the heater is too hot, the fan has probably failed, turn off the output. */
        if (onewire->temp(OnewireTemps::Probes::Heater) > 110 && GetMode())
        {
            // Serial.println("HEATER OVERHEATING!");
            SetEngaged(false);
            _register = 0;
        }
        else if (!GetMode() && onewire->temp(OnewireTemps::Probes::Heater) < 35)
        {
            // Serial.println("Heater cooled down, restarting");
            SetEngaged(true);
        }

        PwmPID::_dispatch(now);
    }

    float feedforward() const override
    {
        return hotbox_openloop_gain
               * (hotbox_setpoint - onewire->temp(OnewireTemps::Probes::Ambient));
    }
};

prog_uint8_t const HeaterPID::heater_pid_name[] = "HOTBOXUT";

HeaterPID *        heater_pid;
prog_uint8_t const fan_pid_name[] = "HTBXFANC";
PwmPIDBase *       fan_pid;

/* This is defined inline here so it sees all the global objects and
 * can ask them to send. */
class SerialSender : public Periodic
{
public:
    SerialSender(ms_t period);

private:
    void _dispatch(ms_t) override final;
};

SerialSender::SerialSender(ms_t period) : Periodic(period) {}

void SerialSender::_dispatch(ms_t)
{
    digitalWrite(commLed, HIGH);

    uint16_t   crc  = 0x0000;
    const ms_t tick = millis();
    send_header(8, tick);

    fan_tach.send_data(crc);
    onewire->send_data(crc);
    heater_pid->send_data(crc);
    fan_pid->send_data(crc);

    // don't wait for a response, that's useless.
    bool ack = wait_for_response(crc, 0);

    digitalWrite(commLed, LOW);
}

SerialSender sender(10000);

class Mark : public Periodic
{
public:
    Mark() : Periodic(1000) {}

    void _dispatch(ms_t now) override final
    {
        if (verbose)
        {
            sprint_pgm(PSTR("MARK "));
            Serial.println(now);
            Serial.print("rpm ");
            Serial.println(fan_tach.rpm());
            Serial.print("fan pwm ");
            Serial.print(fan_pid->output());
            Serial.print("hotbox temp ");
            Serial.println(heater_pid->hotbox_temp);
            Serial.print("heater pwm ");
            Serial.println(heater_pid->output());
        }
    }
};

Mark mark;

void setup_timers()
{
    cli();

    /* Timer 0 runs fast pwm on both OC0A and OC0B with prescaler
     * 1. (We keep the overflow interrupt enabled so millis doesn't
     * stall. Instead, it will run about 30x too fast.) */
#if 0    
    OCR0A = 0x00;
    OCR0B = 0x00;
    TCCR0A = _BV(COM0A1) | _BV(COM0B1) | _BV(WGM00) | _BV(WGM01);
    TCCR0B = _BV(CS00);
    TIFR0 = 0xff;
    TIMSK0 = 0;//_BV(TOIE0);
    /* OC0A/B is PD5/6. Set as outputs. */
    DDRC |= 0x60;
#endif

    /* Timer 1 runs 16-bit phase-correct pwm on OC1B with
     * prescaler 64 (0.95Hz). Clear interrupt flags and enable timer
     * overflow interrupt. Set OC1B to outputs. */
    OCR1B  = 0x0000;
    ICR1   = 0xffff;
    TCCR1A = _BV(COM1B1) | _BV(WGM11);
    TCCR1B = _BV(WGM13) | _BV(CS11) | _BV(CS10);
    TIFR1  = 0xff;
    TIMSK1 = _BV(TOIE1);
    /* OC1B is PB2. Set as outputs. */
    DDRB |= 0x04;

    /* Timer 2 runs fast PWM on OC2A with prescaler 1. Because the fan
     * PWM is connected through an open-drain MOSFET, we invert the
     * signal. OC2B is disconnected. Enable timer overflow interrupt
     * (for the time keeper). Ensure timer2 is connected to the IO
     * clock. */
    ASSR &= ~_BV(AS2);
    OCR2A  = 123;
    TCCR2A = _BV(COM2A0) | _BV(COM2A1) | _BV(WGM20) | _BV(WGM21);
    TCCR2B = _BV(CS21);
    TIFR2  = 0xff;
    TIMSK2 = _BV(TOIE2);
    /* OC2B is PB3. Set as output. */
    DDRB |= 0x08;

    // Make sure all timers are enabled in the power reduction register
    PRR &= ~(_BV(PRTIM0) | _BV(PRTIM1) | _BV(PRTIM2));

    /* Set up watchdog timer for 4s. */
    /*
    wdt_reset();
    WDTCSR |= _BV(WDE) | _BV(WDCE);
    WDTCSR = _BV(WDIF) | _BV(WDIE) | _BV(WDE) | _BV(WDP3);
    */

    sei();
}

void setup()
{
    pinMode(commLed, OUTPUT);
    digitalWrite(commLed, LOW);

    Serial.begin(57600);

    setup_timers();

    onewire = new OnewireTemps(verbose);

    heater_pid = new HeaterPID();

    fan_pid = new PwmPID<decltype(OCR2A)>(OCR2A,
                                          &onewire->temp(OnewireTemps::Probes::Heater),
                                          //&dummy,
                                          &fan_setpoint,
                                          fan_pid_name);
    /* Guess at gains, 20 degrees above setpoint should put fan on
     * full. The controller direction is inverted because higher fan
     * means lower temperature. */
    fan_pid->SetDirection(false);
    fan_pid->SetTunings(0.05, 0.01, 0.0);
    fan_pid->SetEngaged(true);
    fan_pid->SetOutputLimits(0.2, 1.0);
    fan_pid->set_period(5000);
    fan_pid->SetTimeout(30);

    event_loop.add_event(&fan_tach);
    event_loop.add_event(onewire);
    event_loop.add_event(heater_pid);
    event_loop.add_event(fan_pid);
    event_loop.add_event(&mark);
    event_loop.add_event(&sender);

    sprint_pgm(PSTR("Drybox controller booted successfully\r\n"));
    Serial.print(get_free_memory());
    sprint_pgm(PSTR(" bytes free\r\n"));
}

void loop()
{
    event_loop.dispatch(millis());
    // dummy = onewire->temp(OnewireTemps::Probes::Heater);
}
