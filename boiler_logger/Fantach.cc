#include "Fantach.h"
#include "serial_data.h"
#include <avr/interrupt.h>

const prog_uint8_t Fantach::_address[] PROGMEM = "HOTBXRPM";

namespace
{
    /* Conversion from pulses to RPM. */
    volatile float    _p2rpm        = 0;
    volatile uint16_t _counter      = 0;
    volatile uint16_t _last_counter = 0;
}  // namespace

/* External interrupt 0 handler. Because this only triggers when our
 * pin changes edge, there's nothing to do but increment the
 * counter. */
void int0()
{
    ++_counter;
}

/* Timer 1 overflow -- copy final counter value. */
ISR(TIMER1_OVF_vect)
{
    _last_counter = _counter;
    _counter      = 0;
}

Fantach::Fantach(float t_ovf, int ppr) : Periodic(100)
{
    _p2rpm   = 60.0f / (t_ovf * ppr);
    _rpm     = 0;
    _counter = 0;

    /* Set INT0/PD2 to input, no pull-up. */
    DDRD &= ~0x04;
    PORTD &= ~0x04;

    /* External interrupts are managed by Arduino so we have to use
     * the Arduino interface. */
    attachInterrupt(digitalPinToInterrupt(2), int0, FALLING);
}

/* Calculate RPM over the last period. Because the interrupt might change
 * the last_counter at any time, we have to protect the reading. */
void Fantach::_dispatch(ms_t)
{
    cli();
    _rpm = _p2rpm * _last_counter;
    sei();
}

void Fantach::send_data(uint16_t & crc)
{
    uint8_t addr[8];
    memcpy_P(addr, _address, 8);
    ::send_data(addr, rpm(), crc);
}
