#include "Arduino.h"
#include <avr/interrupt.h>
#include <avr/io.h>
#include <inttypes.h>

/* Calculated duty cycle, 0-1023. */
volatile uint16_t duty_cycle;

/* Time last positive edge seen. */
volatile uint16_t pos_edge_time;

/* Time last negative edge seen. */
volatile uint16_t neg_edge_time;

volatile bool overflowed = false;
volatile bool have_pos   = false;
volatile bool have_neg   = false;

// timer 1 input capture interrupt
ISR(TIMER1_CAPT_vect)
{
    uint16_t const now = ICR1;

    /* Are we set to capture positive or negative edge? */
    if (TCCR1B & _BV(ICES1))
    {
        /* Positive edge. */

        /* Set next interrupt for negative edge. */
        TCCR1B &= ~_BV(ICES1);

        if (have_pos && have_neg)
        {
            /* If we have a valid edges, calculate new duty
             * cycle. Because these are unsigneds, this works as long
             * as the timer hasn't wrapped since pos_edge_time. */
            duty_cycle =
                float(neg_edge_time - pos_edge_time) / (now - pos_edge_time) * 1023.;
        }

        pos_edge_time = now;
        have_pos      = true;

        /* PB2 high to signal we got positive edge. */
        PORTB |= 0x04;
    }
    else
    {
        /* Negative edge. */

        /* Set interrupt for pos edge. */
        TCCR1B |= _BV(ICES1);

        /* Record time. */
        neg_edge_time = now;
        have_neg      = true;

        /* PB2 low to signal we got negative edge. */
        PORTB &= ~0x04;
    }

    overflowed = false;

    /* Because we have changed the input capture polarity, clear the
     * interrupt flag again. */
    TIFR = _BV(ICF1);
}

/* Timer 1 overflow interrupt. This is used to test if the input is
 * completely on or off, since that will never create input capture
 * interrupts. */
ISR(TIMER1_OVF_vect)
{
    if (overflowed)
    {
        /* This is the second timer overflow that has happened without
         * detecting an edge, so we conclude the output is not
         * switching. Set the output value according to the PB0 input pin
         * value. */
        duty_cycle = (PINB & 0x01) ? 1023 : 0;

        /* Signal to input capture interrupt that it needs to wait for
         * a cycle until updating the duty cycle when the signal comes
         * back. */
        have_pos = false;
        have_neg = false;
    }

    overflowed = true;

    // Flip PB1 to signal timer overflow
    PORTB ^= 0x02;
}

void setup()
{
    /* Set PB0 as input, no pull-up. PB1/2 as output (for
     * debugging). For the others, we set as inputs with enabled
     * pull-up to ensure they are in a definite state. */
    PORTB = ~0x01;
    DDRB  = 0x06;

    /* Set timer 1 to normal mode, OC1A/B disconnected, input capture
       noise canceller disabled, input capture on rising edge,
       prescaler 8. (This gives a timer clock of 16MHz/8 = 2MHz, a
       clock period of 0.5us, so the timer will overflow after
       32ms. This means we can handle an input PWM as slow as 31Hz.)
       Enable input capture interrupts and timer overflow
       interrupts. */
    TCCR1A = 0x00;
    TCCR1B = _BV(ICES1) | _BV(CS11);
    TIMSK  = _BV(TICIE1) | _BV(TOIE1);

    /* Clear input capture interrupt vector (may not be necessary). */
    TIFR = _BV(ICF1);

    /* Hopefully we won't run out of memory using the Arduino serial lib. */
    Serial.begin(19200);
}

void loop()
{
    // Delay appears to not work correctly on atmega8a
    delay(5000);

    Serial.write(0x55);
    cli();
    uint16_t d = duty_cycle;
    sei();
    uint8_t dh = d >> 8;
    uint8_t dl = d & 0xff;
    Serial.write(dh);
    Serial.write(dl);
    Serial.write(dh + dl);
}
