#include <Arduino.h>
#include "memory.h"
#include "displaylist.h"
#include "hist.h"
#include "float16.h"
#include "frequency.h"
#include "SoftwareSerial.h"
#include "Mtxorb.h"

const byte serial_dcd_pin = 4;

// Display rx is connected to pin 12 and tx to pin 11.
SoftwareSerial mySerial(11, 12);

// the disp has state variables so it must be inited here
typedef Mtxorb<SoftwareSerial> T_disp; 
T_disp disp(mySerial);

void setup()  
{
  pinMode(9,OUTPUT);
  analogWrite(9,0);
  pinMode(10,OUTPUT);
  analogWrite(10,192);

  pinMode(serial_dcd_pin, OUTPUT);
  digitalWrite(serial_dcd_pin, HIGH);

  Serial.begin(57600);
  Serial.println("Initializing...");

  // create Mtxorb object and init display
  mySerial.begin(19200);
  disp.clear();
  disp.set_backlight(true);
  disp.auto_line_wrap(false);
  disp.print("Initializing...\n");
  disp.set_pwm_freq(9);
}

unsigned long int last=0;

void loop()
{
  if(millis()-last>5000) {
    //sprint_pgm(PSTR("Free mem : "));
    //Serial.println(get_free_memory());
    sprint_pgm(PSTR("mtxorb packets: "));
    while(mySerial.available()) {
      Serial.print(mySerial.read(),HEX);
      Serial.print(' ');
    }
    
    //for(byte b=0;b<3;++b) {
    //Serial.print(*disp.rpm_pointer(b));
    //}
    Serial.println();
    disp.send_cmd(byte(193), 1);
    last=millis();
  }

  delay(500);
}
