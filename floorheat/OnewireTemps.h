#pragma once

#include "EventLoop.h"
#include "onewiretemp.h"
#include <inttypes.h>

/* Class periodically reads temperature probes over 1-wire. Note that
 * this class cannot be initialized statically. */
class OnewireTemps : public Periodic
{
public:
    OnewireTemps(bool verbose);

    void _dispatch(ms_t now) override;

    void send_data(uint16_t & crc);

    uint8_t n_devices() const { return _n; }
    
private:
    static uint8_t constexpr onewirepin = 4;

    uint8_t _n;
    
    float* _temps;
    
    OneWireBus _bus;
};
