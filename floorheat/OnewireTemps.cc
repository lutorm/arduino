#include "OnewireTemps.h"
#include "serial_data.h"


OnewireTemps::OnewireTemps(bool verbose) :
    Periodic(10000), _bus(onewirepin, verbose)
{
    _n = _bus.n_devices();
    _temps = new float[n_devices()];
    for (int i = 0; i < n_devices(); ++i)
    {
        _temps[i] = NAN;
        _bus.set_output(i, &_temps[i]);
    }
}

void OnewireTemps::_dispatch(ms_t now)
{
    /* Because we know that it'll take 760ms to convert temp, we can
     * safely read a measurement and then start a new conversion at
     * our period. */
    _bus.readout();
    _bus.start_conversion();
}

void OnewireTemps::send_data(uint16_t & crc)
{
    for (int i = 0; i < n_devices(); ++i)
    {
        uint8_t addr[8];
        memcpy(addr, _bus.address(i), 8);

        ::send_data(addr, _temps[i], crc);
    }
}

