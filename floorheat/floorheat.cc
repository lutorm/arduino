#include "EventLoop.h"
#include "OnewireTemps.h"
#include "memory.h"
#include "serial_data.h"
#include <Arduino.h>
#include <math.h>

bool verbose = false;

/* This is defined inline here so it sees all the global objects and
 * can ask them to send. */
class SerialSender : public Periodic
{
public:
    SerialSender(ms_t period);

private:
    void _dispatch(ms_t) override final;
};

SerialSender::SerialSender(ms_t period) : Periodic(period) {}

OnewireTemps * onewire;


void SerialSender::_dispatch(ms_t)
{
    digitalWrite(LED_BUILTIN, HIGH);

    uint16_t   crc  = 0x0000;
    const ms_t tick = millis();
    send_header(onewire->n_devices(), tick);

    onewire->send_data(crc);

    // don't wait for a response, that's useless.
    bool ack = wait_for_response(crc, 0);

    digitalWrite(LED_BUILTIN, LOW);
}

SerialSender sender(30000);

void setup()
{
    pinMode(LED_BUILTIN, OUTPUT);
    digitalWrite(LED_BUILTIN, LOW);

    Serial.begin(19200);

    onewire = new OnewireTemps(verbose);

    event_loop.add_event(onewire);
    event_loop.add_event(&sender);

    sprint_pgm(PSTR("Drybox controller booted successfully\r\n"));
    Serial.print(get_free_memory());
    sprint_pgm(PSTR(" bytes free\r\n"));
}

void loop()
{
    event_loop.dispatch(millis());
}
