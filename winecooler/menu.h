#pragma once

#include "button.h"
#include "ds2408.h"
#include "max7221.h"
#include <EEPROM.h>

class Menu;

class menu_state;
class temp_disp_state;
class err_disp_state;
class hum_disp_state;
class ot_disp_state;
class at_disp_state;
class temp_set_state;

/// Holds the wine cooler settings stored to the eeprom. The
/// constructor loads itself from the eeprom.
class cooler_settings
{
    float   set_temp_;
    uint8_t intensity_;
    float   pid_p_;
    float   pid_i_;
    float   pid_d_;

public:
    cooler_settings()
    {
        for (uint8_t i = 0; i < sizeof(cooler_settings); ++i)
            ((uint8_t *)this)[i] = EEPROM.read(i);
    }

    void save_to_eeprom()
    {
        for (uint8_t i = 0; i < sizeof(cooler_settings); ++i)
            EEPROM.write(i, ((uint8_t *)this)[i]);
    }

    void set_defaults()
    {
        set_temp_  = 13;
        intensity_ = 15;
        pid_p_     = 10;
        pid_i_     = .01;
        pid_d_     = 0;
    }

    const float &   set_temp() const { return set_temp_; };
    const uint8_t & intensity() const { return intensity_; };
    float &         pid_p() { return pid_p_; };
    float &         pid_i() { return pid_i_; };
    float &         pid_d() { return pid_d_; };

    void increase_setpoint();
    void decrease_setpoint();
    void increase_intensity();
    void decrease_intensity();
};

/// Holds the wine cooler current state variables, which includes the
/// settings permanently stored in eeprom.
class cooler_state
{
public:
    float       cur_air_temp_;
    float       cur_wine_temp_;
    float       cur_tempdiff_;
    float       cur_hum_;
    float       cur_ot_;
    bool        lamp_on_;
    float       tec_out_;
    const float tec_openloop_slope_;
    float       tec_feedforward_;
    float       target_air_temp_;
    float       fan_out_;
    float       fan_setpoint_;

    /** Error bits:
        0: can't read wine temp.
        1: can't read outside temp.
        2: cold sink temp out of range, indicating probably fan failure.
    */
    uint8_t errbits_;

    cooler_settings settings_;

    cooler_state() :
        lamp_on_(false),
        tec_out_(0),
        fan_out_(0),
        fan_setpoint_(1.0),
        tec_openloop_slope_(8.7),
        errbits_(0){};

    cooler_settings & settings() { return settings_; };
};

class menu_state : public button_notification
{
    friend class Menu;
    static Menu * m_;

protected:
    unsigned int timeout_;

public:
    cooler_state * state();
    void           transition(menu_state * next);
    max7221 *      display();
    unsigned int   timeout_interval() { return timeout_; };
    virtual void   update()  = 0;
    virtual void   enter()   = 0;
    virtual void   timeout() = 0;
    /// Convenience function to update all digits.
    void set_digits(uint8_t, uint8_t, uint8_t, uint8_t, uint8_t, uint8_t, uint8_t);
    void update_letters_numbers(
        float val, char * fmt, int len, uint8_t * letters, uint8_t nletters);
    void update_temp(float val, char * fmt, int len, uint8_t * letters, uint8_t nletters);
};

const int pagetime = 5000;

class Menu : public button_notification
{
    cooler_state *    state_;
    menu_state *      current_;
    max7221 *         display_;
    unsigned long int last_;
    unsigned long int last_event_;
    int               interval_;

public:
    // Menu must NOT be created statically, because the singletons are
    // statically initialized
    Menu(int interval, cooler_state * s, max7221 * display);
    void           transition(menu_state * n);
    void           update();
    cooler_state * state() { return state_; };
    max7221 *      display() { return display_; };
    void           push(uint8_t b)
    {
        // Serial.print("Push "); Serial.println(b, DEC);
        last_event_ = millis();
        current_->push(b);
    };
    void hold(uint8_t b)
    {
        // Serial.print("Hold "); Serial.println(b, DEC);
        last_event_ = millis();
        current_->hold(b);
    };
};

/// startup_state is initial state and says hello. After 3s it
/// switches to the temp_set_state to show the set point
class startup_state : public menu_state
{
    static startup_state * instance_;
    startup_state() { timeout_ = 2000; };

public:
    static startup_state * get_instance();

    void update();
    void push(uint8_t b){};
    void hold(uint8_t b){};
    void enter();
    void timeout();
};

/** temp_disp_state shows current temperature. up/down switches to
    temp_set_state. Lamp button toggles lamp. Holding lamp button
    enters programming state. */
class temp_disp_state : public menu_state
{
    static temp_disp_state * instance_;
    temp_disp_state() { timeout_ = pagetime; };

public:
    static temp_disp_state * get_instance();

    void update();
    void push(uint8_t b);
    void hold(uint8_t b);
    void enter();
    void timeout();
};

/** hum_disp_state shows current humidity. functions like the
    temp_disp_state. */
class hum_disp_state : public menu_state
{
    static hum_disp_state * instance_;
    hum_disp_state() { timeout_ = pagetime; };

public:
    static hum_disp_state * get_instance();

    void update();
    void push(uint8_t b) { temp_disp_state::get_instance()->push(b); };
    void hold(uint8_t b) { temp_disp_state::get_instance()->hold(b); };
    void enter();
    void timeout();
};

/** ot_disp_state shows current outside temperature. functions like the
    temp_disp_state. */
class ot_disp_state : public menu_state
{
    static ot_disp_state * instance_;
    ot_disp_state() { timeout_ = pagetime; };

public:
    static ot_disp_state * get_instance();

    void update();
    void push(uint8_t b) { temp_disp_state::get_instance()->push(b); };
    void hold(uint8_t b) { temp_disp_state::get_instance()->hold(b); };
    void enter();
    void timeout();
};

/** at_disp_state shows current cabinet air temperature. functions like the
    temp_disp_state. */
class at_disp_state : public menu_state
{
    static at_disp_state * instance_;
    at_disp_state() { timeout_ = pagetime; };

public:
    static at_disp_state * get_instance();

    void update();
    void push(uint8_t b) { temp_disp_state::get_instance()->push(b); };
    void hold(uint8_t b) { temp_disp_state::get_instance()->hold(b); };
    void enter();
    void timeout();
};

/** tec_disp_state shows current cooler PID output in percent (it
    doesn't show the sign, that's given by the main temp page)
    functions like the temp_disp_state. */
class tec_disp_state : public menu_state
{
    static tec_disp_state * instance_;
    tec_disp_state() { timeout_ = pagetime; };

public:
    static tec_disp_state * get_instance();

    void update();
    void push(uint8_t b) { temp_disp_state::get_instance()->push(b); };
    void hold(uint8_t b) { temp_disp_state::get_instance()->hold(b); };
    void enter();
    void timeout();
};

/** err_disp_state shows the errbits value. It's only shown if it's
    nonzero. */
class err_disp_state : public menu_state
{
    static err_disp_state * instance_;
    err_disp_state() { timeout_ = pagetime; };

public:
    static err_disp_state * get_instance();

    void update();
    void push(uint8_t b) { temp_disp_state::get_instance()->push(b); };
    void hold(uint8_t b) { temp_disp_state::get_instance()->hold(b); };
    void enter();
    void timeout() { transition(temp_disp_state::get_instance()); };
};

/** temp_set_state shows current temperature set point. up/down
    changes set point. Lamp button exits, or it times out. */
class temp_set_state : public menu_state
{
    static temp_set_state * instance_;
    temp_set_state() { timeout_ = pagetime; };

public:
    static temp_set_state * get_instance();

    void update();
    void push(uint8_t b);
    void hold(uint8_t b) { push(b); };
    void enter();
    void timeout() { transition(temp_disp_state::get_instance()); };
};

/** prog_state is entered by holding lamp down. From it it's possible
    to cycle through the programmable options. It just shows
    "Prog". */
class prog_state : public menu_state
{
    static prog_state * instance_;
    prog_state() { timeout_ = 0; };

public:
    static prog_state * get_instance();

    void update();
    void push(uint8_t b);
    void hold(uint8_t b);
    void enter();
    void timeout(){};
};

/** brightness_prog_state allows changing the brightness of the display. */
class brightness_prog_state : public menu_state
{
    static brightness_prog_state * instance_;
    brightness_prog_state() { timeout_ = 0; };

    static bool programming_;

public:
    static brightness_prog_state * get_instance();

    void update();
    void push(uint8_t b);
    void hold(uint8_t b){};
    void enter();
    void timeout(){};
};

/** setp_state sets the PID P value. */
class setp_state : public menu_state
{
    static setp_state * instance_;
    setp_state() { timeout_ = 0; };

    static bool programming_;

public:
    static setp_state * get_instance();

    void update();
    void push(uint8_t b);
    void hold(uint8_t b){};
    void enter();
    void timeout(){};
};

/** seti_state sets the PID I value. */
class seti_state : public menu_state
{
    static seti_state * instance_;
    seti_state() { timeout_ = 0; };

    static bool programming_;

public:
    static seti_state * get_instance();

    void update();
    void push(uint8_t b);
    void hold(uint8_t b){};
    void enter();
    void timeout(){};
};

/** setd_state sets the PID D value. */
class setd_state : public menu_state
{
    static setd_state * instance_;
    setd_state() { timeout_ = 0; };

    static bool programming_;

public:
    static setd_state * get_instance();

    void update();
    void push(uint8_t b);
    void hold(uint8_t b){};
    void enter();
    void timeout(){};
};

/** set_defaults_state sets defaults if lamp is pushed. */
class set_defaults_state : public menu_state
{
    static set_defaults_state * instance_;
    set_defaults_state() { timeout_ = 0; };

public:
    static set_defaults_state * get_instance();

    void update();
    void push(uint8_t b);
    void hold(uint8_t b){};
    void enter();
    void timeout(){};
};
