#include "menu.h"
#include "memory.h"

void cooler_settings::increase_setpoint()
{
    if (set_temp_ < 25)
        set_temp_ = int(set_temp_) + 1;
}

void cooler_settings::decrease_setpoint()
{
    if (set_temp_ > 5)
        set_temp_ = int(set_temp_) - 1;
}

void cooler_settings::increase_intensity()
{
    if (intensity_ < 16)
        intensity_++;
}

void cooler_settings::decrease_intensity()
{
    if (intensity_ > 1)
        intensity_--;
}

Menu::Menu(int interval, cooler_state * s, max7221 * display) :
    state_(s), display_(display), last_(0), interval_(interval)
{
    menu_state::m_ = this;
    transition(startup_state::get_instance());
};

void Menu::transition(menu_state * n)
{
    // Serial.print("transition: ");
    last_event_ = millis();
    current_    = n;
    current_->enter();
};

void Menu::update()
{
    if (current_->timeout_interval() > 0
        && millis() - last_event_ > current_->timeout_interval())
        current_->timeout();
    else
        current_->update();
};

cooler_state * menu_state::state()
{
    return m_->state();
};
void menu_state::transition(menu_state * next)
{
    m_->transition(next);
};
max7221 * menu_state::display()
{
    return m_->display();
};

void menu_state::set_digits(
    uint8_t d0, uint8_t d1, uint8_t d2, uint8_t d3, uint8_t d4, uint8_t d5, uint8_t d6)
{
    max7221 * d = display();
    d->set_digit(0, d0);
    d->set_digit(1, d1);
    d->set_digit(2, d2);
    d->set_digit(3, d3);
    d->set_digit(4, d4);
    d->set_digit(5, d5);
    d->set_digit(6, d6);
}

// utility function for displaying a value prepended by letters
void menu_state::update_letters_numbers(
    float val, char * fmt, int len, uint8_t * letters, uint8_t nletters)
{
    max7221 * d = display();

    char    str[7];
    uint8_t j = max7221::b_encode_number(fmt, str, len, val);

    if (j > 6 - nletters)
        j = 6 - nletters;

    for (uint8_t i = 0; i < nletters; ++i)
        d->set_digit(i, letters[i]);

    for (uint8_t i = 0; i < j; ++i)
    {
        d->set_digit(i + nletters, str[i]);
    }
    d->set_digit(6, 0x00);
};

// utility function for displaying a temperature prepended by letters
void menu_state::update_temp(
    float val, char * fmt, int len, uint8_t * letters, uint8_t nletters)
{
    max7221 * d = display();

    char    str[6];
    uint8_t j = max7221::b_encode_number(fmt, str, len, val);

    if (j > 5 - nletters)
        j = 5 - nletters;

    for (uint8_t i = 0; i < nletters; ++i)
        d->set_digit(i, letters[i]);

    for (uint8_t i = 0; i < j; ++i)
    {
        d->set_digit(i + nletters, str[i]);
    }

    d->set_digit(5, 0x4e);
    d->set_digit(6, 0x10);
}

Menu * menu_state::m_;

temp_disp_state * temp_disp_state::instance_ = 0;

temp_disp_state * temp_disp_state::get_instance()
{
    if (instance_ == 0)
        instance_ = new temp_disp_state;
    return instance_;
}

startup_state * startup_state::instance_ = 0;

startup_state * startup_state::get_instance()
{
    if (instance_ == 0)
        instance_ = new startup_state;
    return instance_;
}

// startup_state uses B-code for digits 0-5
void startup_state::enter()
{
    // Serial.println("startup_state");
    display()->set_scanlimit(7);
    display()->test(true);
    delay(1000);
    display()->test(false);
    display()->set_intensity(state()->settings().intensity());
    display()->set_decode(0x3f);
    update();
    display()->shutdown(false);
}

/// "HELLO "
void startup_state::update()
{
    set_digits(0x0c, 0x0b, 0x0d, 0x0d, 0x00, 0x0f, 0x00);
}

void startup_state::timeout()
{
    transition(temp_set_state::get_instance());
}

/// temp_disp_state uses B-code for digits 1-4
void temp_disp_state::enter()
{
    // Serial.println("temp_disp_state");
    display()->shutdown(true);
    display()->set_decode(0x1e);
    update();
    display()->shutdown(false);
}

/// "C  12.3'C" (first digit indicates Cooling, Heating or "-" for at setpoint)
void temp_disp_state::update()
{
    uint8_t     letters[1];
    const float t = state()->cur_wine_temp_;

    if (t > state()->settings().set_temp() + 0.1)
        letters[0] = 0x4e;
    else if (t < state()->settings().set_temp() - 0.1)
        letters[0] = 0x37;
    else
        letters[0] = 0x01;

    update_temp(t, "% 5.1f", 6, letters, 1);
}

void temp_disp_state::timeout()
{
    transition(hum_disp_state::get_instance());
}

void temp_disp_state::push(uint8_t b)
{
    switch (b)
    {
    case 0:
        transition(temp_set_state::get_instance());
        break;
    case 1:
        transition(temp_set_state::get_instance());
        break;
    case 2:
        state()->lamp_on_ = !state()->lamp_on_;
        // Serial.println(state()->lamp_on_,DEC);
    };
}

void temp_disp_state::hold(uint8_t b)
{
    if (b == 2)
        transition(prog_state::get_instance());
    else
        push(b);
}

hum_disp_state * hum_disp_state::instance_ = 0;

hum_disp_state * hum_disp_state::get_instance()
{
    if (instance_ == 0)
        instance_ = new hum_disp_state;
    return instance_;
}

/// hum_disp_state uses B-code for digits 0, 2-5
void hum_disp_state::enter()
{
    // Serial.println("hum_disp_state");
    display()->shutdown(true);
    display()->set_decode(0x3d);
    // set HU here
    display()->set_digit(0, 0x0c);
    display()->set_digit(1, 0x3e);
    display()->set_digit(2, 0x0f);
    display()->set_digit(6, 0x00);
    update();
    display()->shutdown(false);
}

/// "HU  85"
void hum_disp_state::update()
{
    uint8_t letters[] = {0x0c, 0x3e, 0x0f};
    update_letters_numbers(state()->cur_hum_, "%3.0f", 3, letters, 3);
}

void hum_disp_state::timeout()
{
    transition(ot_disp_state::get_instance());
}

ot_disp_state * ot_disp_state::instance_ = 0;

ot_disp_state * ot_disp_state::get_instance()
{
    if (instance_ == 0)
        instance_ = new ot_disp_state;
    return instance_;
}

/// ot_disp_state uses B-code for digits 0, 2-4
void ot_disp_state::enter()
{
    // Serial.println("ot_disp_state");
    display()->shutdown(true);
    display()->set_decode(0x1d);
    update();
    display()->shutdown(false);
}

/// "Ot 22.4'C"
void ot_disp_state::update()
{
    uint8_t letters[] = {0x00, 0x0f};
    update_temp(state()->cur_ot_, "%4.1f", 5, letters, 2);
}

void ot_disp_state::timeout()
{
    transition(at_disp_state::get_instance());
}

at_disp_state * at_disp_state::instance_ = 0;

at_disp_state * at_disp_state::get_instance()
{
    if (instance_ == 0)
        instance_ = new at_disp_state;
    return instance_;
}

/// at_disp_state uses B-code for digits 2-4
void at_disp_state::enter()
{
    // Serial.println("at_disp_state");
    display()->shutdown(true);
    display()->set_decode(0x1c);
    update();
    display()->shutdown(false);
}

/// "At 22.4'C"
void at_disp_state::update()
{
    uint8_t letters[] = {0x77, 0x0f};
    update_temp(state()->cur_air_temp_, "%4.1f", 5, letters, 2);
}

void at_disp_state::timeout()
{
    transition(tec_disp_state::get_instance());
}

tec_disp_state * tec_disp_state::instance_ = 0;

tec_disp_state * tec_disp_state::get_instance()
{
    if (instance_ == 0)
        instance_ = new tec_disp_state;
    return instance_;
}

void tec_disp_state::timeout()
{
    if (state()->errbits_ == 0)
        transition(temp_disp_state::get_instance());
    else
        transition(err_disp_state::get_instance());
};

/// tec_disp_state uses B-code for digits 1, 3-5
void tec_disp_state::enter()
{
    // Serial.println("tec_disp_state");
    display()->shutdown(true);
    display()->set_decode(0x3a);
    update();
    display()->shutdown(false);
}

/// "tEC100". Note that this doesn't work correctly for -100
void tec_disp_state::update()
{
    uint8_t letters[] = {0x0f, 0x0b, 0x4e};
    update_letters_numbers(abs(state()->tec_out_), "%3.0f", 4, letters, 3);
}

err_disp_state * err_disp_state::instance_ = 0;

err_disp_state * err_disp_state::get_instance()
{
    if (instance_ == 0)
        instance_ = new err_disp_state;
    return instance_;
}

/// err_disp_state uses B-code for digits 0, 3-5
void err_disp_state::enter()
{
    // Serial.println("err_disp_state");
    display()->shutdown(true);
    display()->set_decode(0x39);
    update();
    display()->shutdown(false);
}

/// "Err123"
void err_disp_state::update()
{
    uint8_t letters[] = {0x0b, 0x05, 0x05};
    update_letters_numbers(float(state()->errbits_), "%3.0f", 4, letters, 3);
}

temp_set_state * temp_set_state::instance_ = 0;

temp_set_state * temp_set_state::get_instance()
{
    if (instance_ == 0)
        instance_ = new temp_set_state;
    return instance_;
}

// temp_set_state uses B-code for digits 0-1,3-4
void temp_set_state::enter()
{
    // Serial.println("temp_set_state");
    display()->set_decode(0x1b);
    update();
};

/** The temp_set_state displays "SEt" and then the temperature. */
void temp_set_state::update()
{
    uint8_t letters[] = {0x05, 0x0b, 0x0f};
    update_temp(state()->settings().set_temp(), "%2.0f", 3, letters, 3);

    // display()->set_digit(0,0x05);
    // display()->set_digit(1,0x0b);
    // display()->set_digit(2,0x0f);

    // const float t = state()->settings().set_temp();
    // char str[3];
    // uint8_t j = max7221::b_encode_number("%2.0f", str, 3, t);

    // for(uint8_t i=0; i<j; ++i) {
    //   display()->set_digit(i+3, str[i]);
    // }
    // display()->set_digit(5,0x4e);
    // display()->set_digit(6,0x10);
}

void temp_set_state::push(uint8_t b)
{
    switch (b)
    {
    case 0:
        state()->settings().increase_setpoint();
        break;
    case 1:
        state()->settings().decrease_setpoint();
        break;
    case 2:
        transition(temp_disp_state::get_instance());
    };
};

prog_state * prog_state::instance_ = 0;

prog_state * prog_state::get_instance()
{
    if (instance_ == 0)
        instance_ = new prog_state;
    return instance_;
}

// prog_state uses B-code for digits 0
void prog_state::enter()
{
    // Serial.println("prog_state");
    display()->shutdown(true);
    display()->set_decode(0x01);
    update();
    display()->shutdown(false);
};

void prog_state::update()
{
    set_digits(0x0e, 0x05, 0x1d, 0x7b, 0x00, 0x00, 0x00);
};

void prog_state::push(uint8_t b)
{
    switch (b)
    {
    case 0:
        break;
    case 1:
        transition(brightness_prog_state::get_instance());
        break;
    case 2:
        // just a push exits but does not save settings
        transition(temp_disp_state::get_instance());
    };
};

void prog_state::hold(uint8_t b)
{
    switch (b)
    {
    case 0:
        break;
    case 1:
        transition(brightness_prog_state::get_instance());
        break;
    case 2:
        // save settings to eeprom here
        state()->settings().save_to_eeprom();
        transition(temp_disp_state::get_instance());
    };
};

brightness_prog_state * brightness_prog_state::instance_ = 0;
bool                    brightness_prog_state::programming_;

brightness_prog_state * brightness_prog_state::get_instance()
{
    if (instance_ == 0)
        instance_ = new brightness_prog_state;
    return instance_;
}

// prog_state uses B-code for digits 2-5
void brightness_prog_state::enter()
{
    // Serial.println("brightness_prog_state");
    display()->shutdown(true);
    display()->set_decode(0x3c);
    update();
    display()->shutdown(false);
    programming_ = false;
};

void brightness_prog_state::update()
{
    const uint8_t br = state()->settings().intensity();

    char    str[5];
    uint8_t j = max7221::b_encode_number("% 4.0f", str, 5, float(br));
    display()->set_digit(0, 0x1f);
    display()->set_digit(1, 0x05);

    for (uint8_t i = 0; i < j; ++i)
    {
        display()->set_digit(i + 2, str[i]);
    }
    display()->set_digit(6, 0x00);
};

void brightness_prog_state::push(uint8_t b)
{
    switch (b)
    {
    case 0:
        if (programming_)
        {
            state()->settings().increase_intensity();
            display()->set_intensity(state()->settings().intensity());
        }
        else
        {
            transition(prog_state::get_instance());
        }
        break;
    case 1:
        if (programming_)
        {
            state()->settings().decrease_intensity();
            display()->set_intensity(state()->settings().intensity());
        }
        else
        {
            transition(setp_state::get_instance());
        }

        break;
    case 2:
        programming_ = !programming_;
    };
};

setp_state * setp_state::instance_ = 0;
bool         setp_state::programming_;

setp_state * setp_state::get_instance()
{
    if (instance_ == 0)
        instance_ = new setp_state;
    return instance_;
}

void setpar_enter(menu_state * state, max7221 * display)
{
    display->shutdown(true);
    display->set_decode(0x3e);
    state->update();
    display->shutdown(false);
}

// prog_state uses B-code for digits 1-5
void setp_state::enter()
{
    setpar_enter(this, display());
    programming_ = false;
};

void setp_state::update()
{
    uint8_t letters[] = {0x67, 0x0f};
    update_letters_numbers(state()->settings().pid_p(), "% 4.0f", 5, letters, 2);
}

menu_state * setpar_push(uint8_t      b,
                         bool &       programming,
                         float &      var,
                         float        delta,
                         menu_state * prev,
                         menu_state * next)
{
    switch (b)
    {
    case 0:
        if (programming)
        {
            var += delta;
        }
        else
        {
            return prev;
        }
        break;
    case 1:
        if (programming)
        {
            var -= delta;
        }
        else
        {
            return next;
        }

        break;
    case 2:
        programming = !programming;
    };
    return 0;
};

void setp_state::push(uint8_t b)
{
    menu_state * n = setpar_push(b,
                                 programming_,
                                 state()->settings().pid_p(),
                                 1.0,
                                 brightness_prog_state::get_instance(),
                                 seti_state::get_instance());
    if (n)
        transition(n);
};

seti_state * seti_state::instance_ = 0;
bool         seti_state::programming_;

seti_state * seti_state::get_instance()
{
    if (instance_ == 0)
        instance_ = new seti_state;
    return instance_;
}

void seti_state::enter()
{
    setpar_enter(this, display());
    programming_ = false;
};

// I-value is set with 2 decimals
void seti_state::update()
{
    uint8_t letters[] = {0x06, 0x0f};
    update_letters_numbers(state()->settings().pid_i(), "% 4.2f", 6, letters, 2);
};

void seti_state::push(uint8_t b)
{
    menu_state * n = setpar_push(b,
                                 programming_,
                                 state()->settings().pid_i(),
                                 0.01,
                                 setp_state::get_instance(),
                                 setd_state::get_instance());
    if (n)
        transition(n);
};

setd_state * setd_state::instance_ = 0;
bool         setd_state::programming_;

setd_state * setd_state::get_instance()
{
    if (instance_ == 0)
        instance_ = new setd_state;
    return instance_;
}

void setd_state::enter()
{
    setpar_enter(this, display());
    programming_ = false;
};

// D-value is set with 2 decimals
void setd_state::update()
{
    uint8_t letters[] = {0x3d, 0x0f};
    update_letters_numbers(state()->settings().pid_d(), "% 4.2f", 6, letters, 2);
};

void setd_state::push(uint8_t b)
{
    menu_state * n = setpar_push(b,
                                 programming_,
                                 state()->settings().pid_d(),
                                 0.01,
                                 seti_state::get_instance(),
                                 set_defaults_state::get_instance());
    if (n)
        transition(n);
};

set_defaults_state * set_defaults_state::instance_ = 0;

set_defaults_state * set_defaults_state::get_instance()
{
    if (instance_ == 0)
        instance_ = new set_defaults_state;
    return instance_;
}

// prog_state uses B-code for digits 0,1,4
void set_defaults_state::enter()
{
    // Serial.println("set_defaults_state");
    display()->shutdown(true);
    display()->set_decode(0x13);
    update();
    display()->shutdown(false);
};

// SEtdEf
void set_defaults_state::update()
{
    set_digits(0x05, 0x0b, 0x0f, 0x3d, 0x0b, 0x47, 0x00);
};

void set_defaults_state::push(uint8_t b)
{
    switch (b)
    {
    case 0:
        transition(setd_state::get_instance());
        break;
    case 1:
        // transition(set_defaults_state::get_instance());
        break;
    case 2:
        state()->settings().set_defaults();
    };
};
