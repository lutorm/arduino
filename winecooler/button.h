#pragma once

#include "ds2408.h"

/// Abstract base class for objects receiving button callbacks.
class button_notification
{
public:
    // This callback means a button was pressed.
    virtual void push(uint8_t) = 0;
    /// This callback means the button was held down.
    virtual void hold(uint8_t) = 0;
};

/** This class detects button presses on a ds2408 and notifies
    objects. This requires keeping a small amount of state to make
    sure we trigger on correct activity.  There are 4 different
    combinations of old and new states. The only one that does NOT
    imply a button has been pressed is going from down to up, all
    other combinations must include a press. The convention here is
    that an open switch is 1. */
class buttonchecker
{
    const static uint8_t  max_n_ = 3;
    uint8_t               n_, state_;
    uint8_t               masks_[max_n_];
    int                   interval_, hold_time_;
    unsigned long int     last_;
    button_notification * objs_[max_n_];
    int                   hold_[max_n_];

    ds2408 * ds_;

public:
    buttonchecker(ds2408 * ds, int interval, int hold_time) :
        ds_(ds), n_(0), interval_(interval), hold_time_(hold_time)
    {
        state_ = ds_->pio_state();
        last_  = millis();
    };

    /** Registers the object to get notifications when activity is
        detected with the specified mask. */
    void register_callback(uint8_t mask, button_notification * obj)
    {
        if (n_ >= max_n_)
            return;
        masks_[n_] = mask;
        objs_[n_]  = obj;
        ++n_;
    }

    void update();
};
