#include "OneWire.h"
#include "PID.h"
#include "ds2408.h"
#include "max7221.h"
#include "memory.h"
#include "menu.h"
#include "onewiretemp.h"
#include <Arduino.h>

// front panel connected to PD4 = Arduino pin D4
const byte onewirepin = 4;
// temp probes are connected to PD2 = Arduino pin D2
const byte onewirepin2 = 2;
// fan 1 is connected to PD6 (OC0A), Arduino pin D6
// fan 2 is connected to PD5 (OC0B), Arduino pin D5
const byte fan1 = 6;
const byte fan2 = 5;
// bridge input 1 is connected to PB1 (OC1A), Arduino pin D9
// bridge input 2 is connected to PB2 (OC1B), Arduino pin D10
const byte br1 = 9;
const byte br2 = 10;

// brdige disable is connected to PB0, Arduino pin D8
const byte brd = 8;

// humidity sensor is connected to PC0, Arduino pin A0
const byte humpin = 0;

uint8_t ds2408_addr[] = {0x29, 0x88, 0x3e, 0x0F, 0x00, 0x00, 0x00, 0xD4};

// one-wire device addresses
namespace onewiredevs
{
    const prog_uint8_t a1[] PROGMEM = {0x28, 0x70, 0xC1, 0x72, 0x02, 0x00, 0x00, 0x89};
    const prog_uint8_t a2[] PROGMEM = {0x28, 0xEB, 0xAF, 0x72, 0x02, 0x00, 0x00, 0x73};
    const prog_uint8_t a3[] PROGMEM = {0x28, 0x22, 0x1A, 0xF6, 0x01, 0x00, 0x00, 0xD4};
    const prog_uint8_t a4[] PROGMEM = {0x28, 0xB3, 0x74, 0x72, 0x02, 0x00, 0x00, 0xD2};
    const prog_uint8_t a5[] PROGMEM = {0x28, 0xB9, 0xF8, 0x71, 0x02, 0x00, 0x00, 0x7F};
    const prog_uint8_t a6[] PROGMEM = {0x28, 0x87, 0xD1, 0x72, 0x02, 0x00, 0x00, 0x02};

    const prog_char      n1[] PROGMEM      = "Back bottom";
    const prog_char      n2[] PROGMEM      = "Back Top";
    const prog_char      n3[] PROGMEM      = "Lamp";
    const prog_char      n4[] PROGMEM      = "Fan intake";
    const prog_char      n5[] PROGMEM      = "Cold sink";
    const prog_char      n6[] PROGMEM      = "Bottle";
    const int            n_devs            = 6;
    const prog_uint8_t * addresses[n_devs] = {a1, a2, a3, a4, a5, a6};
    const prog_char *    names[n_devs]     = {n1, n2, n3, n4, n5, n6};

    // Make an enum with symbolic names to the probes
    enum
    {
        Bottom,
        Top,
        Lamp,
        Intake,
        Coldsink,
        Bottle
    } probes;

    // State variables for the temp probes
    float temps[n_devs];
}  // namespace onewiredevs

OneWire *    w;
OneWireBus * wb;
ds2408 *     ds;
max7221 *    mx;

long int          i;
uint8_t           btnstate;
unsigned long int m;
buttonchecker *   b;
Menu *            menu;
cooler_state      s;

unsigned long int last_btnchk = 0;
bool              dp          = true;

PID *tec_pid, *fan_pid, *wine_pid;
;

int  pidres;
bool cooling;
int  tccr1a_val;

void update_temps()
{
    using namespace onewiredevs;
    float avgtemp = 0;
    int   n       = 0;
    if (temps[Bottom] == temps[Bottom])
    {
        avgtemp += temps[Bottom];
        n++;
    }
    if (temps[Top] == temps[Top])
    {
        avgtemp += temps[Top];
        n++;
    }
    if (temps[Lamp] == temps[Lamp])
    {
        avgtemp += temps[Lamp];
        n++;
    }
    avgtemp /= n;

    s.cur_air_temp_  = avgtemp;
    s.cur_wine_temp_ = temps[Bottle];
    s.cur_ot_        = temps[Intake];
    s.cur_tempdiff_  = abs(temps[Top] - temps[Bottom]);

    // the open-loop control of the cooler, used as the feed-forward to
    // the pid is set here.
    if (!isnan(s.cur_ot_))
        s.tec_feedforward_ =
            (s.cur_ot_ - s.settings().set_temp()) * s.tec_openloop_slope_;

    // set error bits
    s.errbits_ =
        s.errbits_ & ~0x07 | (isnan(s.cur_wine_temp_) ? 0x01 : 0)
        | (isnan(s.cur_ot_) ? 0x02 : 0)
        | ((temps[Coldsink] < 0 || temps[Coldsink] > s.cur_ot_ || isnan(temps[Coldsink]))
               ? 0x04
               : 0);
}

/** Disables bridge PWM output, setting both outputs low. */
void disable_bridge()
{
    TCCR1A = tccr1a_val;
}

/** Enables bridge PWM output. */
void enable_bridge()
{
    TCCR1A = tccr1a_val | (cooling ? _BV(COM1A1) : _BV(COM1B1));
}

/** Update the PWM registers driving the H-bridge, keeping at least
    one FET on at all times. pid is the output from the control loop,
    in percent, where positive values mean cool and negative values
    mean heat. */
void update_bridge(float pid)
{
    // the min number of ticks the output has to go low each cycle to
    // allow the bootstrap circuit to reload
    const int bootstrap_interval = 5;

    // For very short pulses, the driver doesn't have time to pull the
    // high side up and then down again. It ends up doing some
    // half-assed thing, so we just disable it in these cases. this
    // gives a little deadband for the controller, but whatever.
    const int minval = 2;

    bool newcool;
    int  ocrval;
    if (pid == pid)
    {
        newcool = (pid >= 0);
        ocrval  = (newcool ? pid : -pid) * 0.01 * (pidres - bootstrap_interval);
    }
    else
    {
        newcool = true;
        ocrval  = 0;
    }

    if (ocrval < minval)
    {
        disable_bridge();
    }
    else if (cooling == newcool)
    {
        // same direction, just update the value and set to enabled (in case we disabled)
        (newcool ? OCR1A : OCR1B) = ocrval;
        enable_bridge();
    }
    else
    {
        // control switched direction. first turn off pwm on both outputs
        disable_bridge();

        // set new output value
        (newcool ? OCR1A : OCR1B) = ocrval;

        // wait a little and enable new pwm output, set old value to 0
        delayMicroseconds(100);

        (newcool ? OCR1B : OCR1A) = 0;
        cooling                   = newcool;

        enable_bridge();
    }
}

void front_panel_setup()
{
    // Allocate and initialize front panel objects
    w  = new OneWire(onewirepin);
    ds = new ds2408(w, 0, 1, 2);
    mx = new max7221(ds);

    ds->set_overdrive();

    if (ds->set_output(0xbf))
        Serial.println("set_output succeeded");
    else
        Serial.println("set_output failed");

    Serial.print("PIO status: ");
    Serial.println(ds->pio_state(), HEX);

    btnstate = ds->pio_state();
    ds->clear_activity_latches();
    i = 0;
    m = millis();

    b = new buttonchecker(ds, 200, 1000);

    menu = new Menu(500, &s, mx);

    const uint8_t upmask  = 0x08;
    const uint8_t dnmask  = 0x10;
    const uint8_t lmpmask = 0x20;
    b->register_callback(upmask, menu);
    b->register_callback(dnmask, menu);
    b->register_callback(lmpmask, menu);

    menu->update();
}

void setup()
{
    analogReference(DEFAULT);

    Serial.begin(57600);
    Serial.println("Initializing...");

    // h-bridge driver setup

    // the bridge should be driven in "sign-magnitude drive" mode. this
    // means that one side is always on low and the other is driven with
    // pwm at the desired strength. Cooling or heating is selected by
    // switching which side is low and which is switched.

    // parctically, this means that to have one side constant low, we
    // must disconnect OCR1A/B from the timer, since in fast pwm a value
    // of 0 means the output is high for one tick.

    // THIS IS IMPORTANT. We do NOT want both sides of the bridge to
    // switch off with current flowing, because then the current has
    // nowhere to go. I suspect this is what burns out the driver chips.

    // When switching direction, we must thus make sure we FIRST turn
    // off the side that's switching, and THEN start the switching on
    // the other side.

    pinMode(br1, OUTPUT);
    pinMode(br2, OUTPUT);
    pinMode(brd, OUTPUT);
    digitalWrite(brd, LOW);
    digitalWrite(br1, LOW);
    digitalWrite(br2, LOW);

    analogWrite(fan1, 0);
    analogWrite(fan2, 0);

    // value of TCCR1A/B sets operating mode and clock.  To turn on the
    // pwm outputs, COM1A1/COM1B1 in TCCR1A are set by update_bridge()

    // 10-bit fast pwm, no prescale
    tccr1a_val = _BV(WGM11) | _BV(WGM10);
    TCCR1B     = _BV(WGM12) | _BV(CS10);
    pidres     = 0x3ff;

    // 8-bit fast pwm, no prescale
    // tccr1a_val = _BV(WGM10);
    // TCCR1B = _BV(WGM12) | _BV(CS10);
    // pidres=255;

    // fast pwm counting to ICR1.
    // tccr1a_val = _BV(WGM11) | _BV(WGM10);
    // TCCR1B = _BV(WGM12) | _BV(WGM13) | _BV(CS10);
    // pidres=255;
    // ICR1 = pidres;

    // default to cooling setup
    cooling = true;
    TCCR1A  = tccr1a_val | _BV(COM1A1);

    OCR1A = 0;
    OCR1B = 0;

    update_bridge(0);

    // at outputs above 90%, the supply voltage goes unstable. don't
    // know why so for now keep it below
    // (fixed by 470pF cap across output)
    const float maxout = 100;

    front_panel_setup();

    analogWrite(fan1, 255);
    analogWrite(fan2, 255);
    digitalWrite(brd, HIGH);

    // update_bridge(100);
    // while(true);

    // Allocate 1-wire bus for temperature probes
    // wb = new OneWireBus(onewirepin2, true);
    wb = new OneWireBus(onewirepin2,
                        onewiredevs::addresses,
                        onewiredevs::names,
                        onewiredevs::n_devs,
                        10,
                        true);
    for (byte i = 0; i < onewiredevs::n_devs; ++i)
    {
        // init temp var to NaN, so that will be the value for unmapped entries
        onewiredevs::temps[i] = NAN;
        wb->set_output(i, &onewiredevs::temps[i]);
    }
    wb->update(true);
    wb->update(true);
    update_temps();

    // Allocate and initialize PID controller for bottle
    // temperature. This controls the air temp the cooler loop attempts
    // to hit. We do this 2-phase control because the water bottle has
    // extremely slow response time.
    /*
    wine_pid = new PID(&s.cur_wine_temp_,
                       &s.target_air_temp_,
                       &s.settings().set_temp(),
                       1.5f, 0.001f, 0.00f, true);
    wine_pid->SetOutputLimits(s.settings().set_temp()-1.0, s.settings().set_temp()+6.0);
    s.target_air_temp_ = s.cur_air_temp_;
    */
    // no air temp control seems unstable. control bottle temp directly
    wine_pid = new PID(&s.cur_wine_temp_,
                       &s.tec_out_,
                       &s.settings().set_temp(),
                       &s.tec_feedforward_,
                       s.settings().pid_p(),
                       s.settings().pid_i(),
                       s.settings().pid_d(),
                       false);
    wine_pid->SetOutputLimits(0, maxout);
    // no point in updating faster than we get temp data
    wine_pid->SetSampleTime(10000);
    wine_pid->SetTimeout(30);

    s.tec_out_ = s.tec_feedforward_;
    wine_pid->SetEngaged(true);
    wine_pid->Compute();

    /*
    // Allocate and initialize PID controller
    tec_pid = new PID(&s.cur_air_temp_,
                      &s.tec_out_,
                      &s.target_air_temp_,
                      30.0f, 0.1f, 0.00f, false);
    tec_pid->SetOutputLimits(-maxout, maxout);
    // no point in updating faster than we get temp data
    tec_pid->SetSampleTime(1000);
    tec_pid->SetTimeout(30);

    s.tec_out_ = (s.cur_air_temp_-s.target_air_temp_)*tec_pid->GetKp();
    tec_pid->SetEngaged(true);
    tec_pid->Compute();
    */

    // Allocate and initialize PID controller for internal fan
    fan_pid = new PID(&s.cur_tempdiff_,
                      &s.fan_out_,
                      &s.fan_setpoint_,
                      40.0f,
                      0.1f,
                      0.00f,
                      false);
    fan_pid->SetOutputLimits(0, 100);
    // no point in updating faster than we get temp data
    fan_pid->SetSampleTime(1000);
    fan_pid->SetTimeout(30);
    s.fan_out_ = 100;
    fan_pid->SetEngaged(true);
    fan_pid->Compute();

    sprint_pgm(PSTR("Free mem after initialization: "));
    Serial.println(get_free_memory());
}

long int now = 0;

void loop()
{
    using namespace onewiredevs;

    wb->set_verbose(false);

    if (wb->update_ready())
    {
        wb->readout();
        if (wb->any_fail())
        {
            Serial.println("Probe read fail, disabling cooler temporarily");
            // some probes have reached maximum fail count. this may be due
            // to interference from the bridge switching. disable the bridge
            // output and try to read them again
            disable_bridge();
            wb->readout();
            enable_bridge();
        }
        wb->start_conversion();
    }

    update_temps();
    /*
    // always update the limits of the target air temp in ref to the current set temp
    wine_pid->SetOutputLimits(s.settings().set_temp()-1.0, s.settings().set_temp()+5.0);
    if(isnan(temps[Bottle])) {
      // the bottle probe has failed. then revert to using setpoint for air temp
      wine_pid->SetEngaged(false);
      s.target_air_temp_ = s.settings().set_temp();
    }
    else
      wine_pid->SetEngaged(true);
    */
    wine_pid->SetTunings(s.settings().pid_p(),
                         s.settings().pid_i(),
                         s.settings().pid_d());
    wine_pid->Compute();
    // tec_pid->Compute();
    fan_pid->Compute();

    if (s.errbits_ & 0x04)
    {
        // cold sink temp is weird, probable fan failure. disable tec and
        // do NOT turn it on again until we have user intervention.
        wine_pid->SetEngaged(false);
        s.tec_out_ = 0;
    }

    update_bridge(s.tec_out_);
    const int fan2_speed = abs(s.tec_out_) * 0.01 * 255;
    const int fan1_speed = s.fan_out_ * 0.01 * 255;
    // run internal fan at higher speed of what the cooler runs at or
    // what's required to keep even temp inside
    analogWrite(fan1, fan1_speed > fan2_speed ? fan1_speed : fan2_speed);
    analogWrite(fan2, fan2_speed);

    const int   h         = analogRead(humpin);
    const float sensor_rh = (h / 1024. - 0.16) / .0062;
    const float true_rh   = sensor_rh / (1.0546 - .00216 * temps[Lamp]);
    s.cur_hum_            = true_rh;

    if (millis() - now > 5000)
    {
        now = millis();
        /*
          Serial.println();
          sprint_pgm(PSTR("Wine temp: "));
          Serial.print(s.cur_wine_temp_);
          sprint_pgm(PSTR("C, set temp: "));
          Serial.print(s.settings().set_temp());
          sprint_pgm(PSTR("C\n\r"));
          sprint_pgm(PSTR("Air temps: Top: "));
          Serial.print(temps[Top]);
          sprint_pgm(PSTR("C, Bottom: "));
          Serial.print(temps[Bottom]);
          sprint_pgm(PSTR("C, Lamp: "));
          Serial.print(temps[Lamp]);
          sprint_pgm(PSTR("C, average: "));
          Serial.print(s.cur_air_temp_);
          sprint_pgm(PSTR("C\n\r"));
          sprint_pgm(PSTR("PID out: "));
          Serial.print(s.tec_out_);
          sprint_pgm(PSTR("%, OCR1A: "));
          Serial.print(OCR1A);
          sprint_pgm(PSTR(", OCR1B: "));
          Serial.println(OCR1B);
          sprint_pgm(PSTR("Cold sink: "));
          Serial.print(temps[Coldsink]);
          sprint_pgm(PSTR("C, Intake: "));
          Serial.print(s.cur_ot_);
          sprint_pgm(PSTR("C\n\r"));
          sprint_pgm(PSTR("Humidity sensor: "));
          Serial.print(h);
          sprint_pgm(PSTR(", True RH: "));
          Serial.print(s.cur_hum_);
          sprint_pgm(PSTR("%\n\r"));
          sprint_pgm(PSTR("internal temp diff: "));
          Serial.print(s.cur_tempdiff_);
          sprint_pgm(PSTR("C, fan PID out: "));
          Serial.println(s.fan_out_);
          Serial.println();
        */
        Serial.print("errbits: ");
        Serial.println(int(s.errbits_));
    }
    disable_bridge();
    delayMicroseconds(25);
    b->update();
    menu->update();

    if (bool(ds->pio_state() & 0x40) != s.lamp_on_)
        ds->set_output(6, s.lamp_on_);
    enable_bridge();

    delay(100);
    // if(now>10000) while(true);
}
