#include "button.h"
#include "ds2408.h"

/** Check if any of the buttons have been pressed. */
void buttonchecker::update()
{
    const unsigned long int m = millis();
    if (m - last_ > interval_)
    {
        ds_->update_pios();

        const uint8_t act       = ds_->activity_state();
        const uint8_t new_state = ds_->pio_state();

        ds_->clear_activity_latches();

        for (uint8_t i = 0; i < n_; ++i)
        {

            // if there was no activity but the button is down, see if we've
            // timed out the hold
            if (~new_state & masks_[i] && ~act & masks_[i] && hold_[i] > 0)
            {
                hold_[i] -= m - last_;
                if (hold_[i] < 0)
                    objs_[i]->hold(i);
            }

            if (act & masks_[i])
            {
                // there's been activity on this button. figure out what kind

                // the current state is pushed, which means it must have been
                // a push.  it could be a hold, so start timer
                if (~new_state & masks_[i])
                    hold_[i] = hold_time_;

                // if it was a transition to not pushed, and the hold timer
                // did not go off, it was a push
                if (new_state & masks_[i] && hold_[i] > 0)
                    objs_[i]->push(i);
            }
        }

        state_ = new_state;
        last_  = m;
    }
}
