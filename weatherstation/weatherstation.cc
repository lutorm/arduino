#include "SFE_BMP180.h"
#include "SparkFunHTU21D.h"
#include "SparkFunTSL2561.h"
#include "memory.h"
#include "serial_data.h"
#include <Arduino.h>
#include <avr/power.h>
#include <avr/sleep.h>
#include <avr/wdt.h>

const long int main_loop_delay = 10000;
const int      ack_wait        = 1000;
const int      max_retries     = 32000;

// const bool verbose=true;
const bool verbose = false;

// define inputs we get from not I2C

// digital inputs
uint8_t const xbee_sleep_pin = 2;
uint8_t const fault_pin      = 3;
uint8_t const charge_pin     = 4;
uint8_t const rainmeter_pin  = 5;
uint8_t const windspeed_pin  = 8;

// analog inputs
uint8_t const winddir_pin  = 1;
uint8_t const uvsensor_pin = 2;
uint8_t const vbat_pin     = 3;

// digital outputs
uint8_t const winddir_supply_pin = 10;

const float winddir_reference_resistor = 10e3;

// state variables
float lux                = NAN;
float winddir_resistance = NAN;
int   xbee_wakeups       = 0;

SFE_TSL2561 tsl2561;
SFE_BMP180  bmp180;
HTU21D      htu21d;

// 64-bit "addresses" for sending data
uint8_t luxaddr[]      = "LUXMETER";
uint8_t luxsataddr[]   = "LUXSATUR";
uint8_t luxch0addr[]   = "LUXCH0CT";
uint8_t luxch1addr[]   = "LUXCH1CT";
uint8_t presaddr[]     = "PRESSURE";
uint8_t ptempaddr[]    = "PRESTEMP";  // temperature of pressure meter
uint8_t tempaddr[]     = "OUTSTEMP";
uint8_t humidityaddr[] = "OUTSHUMI";
uint8_t uvfluxaddr[]   = "UVFLUX\0\0";
uint8_t uviaddr[]      = "UVINDEX\0";
uint8_t rainaddr[]     = "RAINMETR";
uint8_t windaddr[]     = "WINDMETR";
uint8_t winddiraddr[]  = "WINDDIRR";
uint8_t vbataddr[]     = "VBATTERY";
uint8_t vccaddr[]      = "AVRVCC\0\0";
uint8_t chargeaddr[]   = "BATCHARG";
uint8_t faultaddr[]    = "BATFAULT";

/* for timing the wind meter, we use the input capture events on timer
 * 1. Every time the input value on ICP1 (PB0, Arduino pin D8)
 * changes, the timer value is copied to ICR1 and the input capture
 * interrupt fires. On the first event, reset the wind counter. For
 * each subsequent event, count up the windcounter, copying the timer
 * value to last_time. When the timer overflows, we can read out the
 * rate as windcounter/last_time.
 */

volatile uint16_t windcounter = 0;
volatile uint16_t last_wind_time;
volatile bool     measurement_in_progress = false;
volatile float    windrate;
float const       timer_prescaler = 1024;
// timer tick in seconds
float const timer_dt = timer_prescaler / F_CPU;

// timer 1 input capture interrupt, triggers on wind meter pin rising edge
ISR(TIMER1_CAPT_vect)
{
    digitalWrite(A0, true);

    if (measurement_in_progress)
    {
        // measurement in progress, increase windcountr and copy
        // timer value
        last_wind_time = ICR1;
        windcounter++;
    }
    else
    {
        /* measurement not in progress, reset timer and start
         * it. (this introduces a small bias since some time will have
         * elapsed since the pin changed, but I doubt it's
         * significant.) */
        windcounter             = 0;
        TCNT1                   = 0;
        last_wind_time          = 0;
        measurement_in_progress = true;
    }

    digitalWrite(A0, false);
}

/* timer 1 overflow interrupt. read out measurement. (At 8MHz with
 * prescaler 1024, counting out the full timer takes 8.39s.) */
ISR(TIMER1_OVF_vect)
{
    digitalWrite(A0, true);
    if (measurement_in_progress)
    {
        windrate = windcounter > 0 ? windcounter / (timer_dt * last_wind_time) : 0.0;
    }
    else
    {
        // we didn't even get the first pulse. conclude rate is zero.
        windrate = 0.0;
    }

    measurement_in_progress = false;

    // turn off timer and disable interrupts
    power_timer1_disable();
    TIMSK1 = 0x00;

    // in case the input capture interrupt has fired while we're
    // executing here, clear the flags (by writing ones). If we don't
    // do this, a pending interrupt would set measurement_in_progress
    // to true but since the interrupt has been turned off, it will
    // never complete.
    TIFR1 = 0xff;

    digitalWrite(A0, false);
}

// to start the wind measurement, we start the timer and enable input
// capture interrupts. The measurement is done when the timer overflow
// interrupt fires and writes the result to windrate. To make sure we
// give the full timer time for the first pulse to arrive, set counter
// to zero. (otherwise we might overflow before getting the first
// pulse right away)
void start_wind_measurement()
{
    digitalWrite(A0, true);
    delay(10);
    digitalWrite(A0, false);

    windrate                = NAN;
    measurement_in_progress = false;
    windcounter             = 0;
    last_wind_time          = 0;
    power_timer1_enable();
    TCNT1 = 0;
    // clear interrupt flag register (by writing ones) and re-enable
    // input capture interrupts
    TIFR1  = 0xff;
    TIMSK1 = 0x21;  // TOIE1 | ICIE1;
}

/* Waits for the wind measurement to finish. In the meanwhile, we
 * sleep.
 */
void wait_for_wind_measurement()
{
    // disable timer 0 so it doesn't wake us up unnecessarily. we
    // don't care about the tick counter being accurate.
    power_timer0_disable();

    cli();
    // loop until measurement is done. disabling the interrupts is
    // done to get atomic access to windrate and to make sure the
    // interrupt doesn't fire and turn off the interrupts before we go
    // to sleep, in which case we'll never wake up.
    while (isnan(windrate))
    {
        digitalWrite(LED_BUILTIN, false);
        sleep_enable();
        sei();
        // put us to sleep.
        sleep_cpu();
        sleep_disable();
        digitalWrite(LED_BUILTIN, true);
        cli();
    }
    sei();
    digitalWrite(LED_BUILTIN, false);

    power_timer0_enable();
}

/* The pin change interrupt takes care of counting the rain meter
 * ticks. */

// the pins are pullups so they are normally high
volatile uint8_t  old_pind_state = 0xff;
volatile uint32_t raincounter    = 0;

void pinchange_interrupt()
{
    digitalWrite(A0, true);

    uint8_t const pin = PIND;
    // look for change
    uint8_t const change = pin ^ old_pind_state;
    old_pind_state       = pin;

    if ((change & 0x20) && (pin & 0x20))
    {
        // pin 5 rising edge - rain meter switch
        raincounter++;
    }

    digitalWrite(A0, false);
}

ISR(PCINT2_vect)
{
    pinchange_interrupt();
}

ISR(BADISR_vect)
{
    for (int i = 0; i < 0xffff; ++i)
    {
        digitalWrite(LED_BUILTIN, true);
        digitalWrite(LED_BUILTIN, false);
    }
}

void xbee_wakeup()
{
    xbee_wakeups++;
}

// Wait for the n-th wakeup of the xbee
void wait_for_xbee(int n = 1)
{
    // disable timer 0 so it doesn't wake us up unnecessarily. we
    // don't care about the tick counter being accurate.
    power_timer0_disable();

    cli();
    xbee_wakeups = 0;

    // sleep until we have triggered the external interrupt the requested
    // number of times. disabling the
    // interrupts and not enabling then until immediately before going
    // to sleep ensures we go to sleep
    while (xbee_wakeups < n)
    {
        digitalWrite(LED_BUILTIN, false);
        sleep_enable();
        sei();
        // put us to sleep.
        sleep_cpu();
        sleep_disable();
        digitalWrite(LED_BUILTIN, true);
        cli();
    }

    sei();
    digitalWrite(LED_BUILTIN, false);
    power_timer0_enable();
}

/* Configure interrupts and setup hardware for minimum power
 * consumption.
 */

void setup_interrupts()
{
    pinMode(A0, OUTPUT);
    digitalWrite(A0, false);

    // enable external interrupt 0 on rising edge
    attachInterrupt(0, xbee_wakeup, RISING);

    // EICRA = _BV(ISC01) | _BV(ISC00);
    // EIMSK = _BV(INT0);

    // we want to disable all pin change interrupts except for pin
    // 5, which is PD5, PCINT21.
    PCICR  = 0x04;
    PCMSK2 = 0x20;

    // also set the power reduction register to disable timers 1 & 2
    // (timer 0 is for the tick) and SPI.
    power_timer2_disable();
    power_spi_disable();

    // disable analog comparator
    ACSR = 0x80;

    // disable input buffers on analog inputs 1-3.
    DIDR0 = 0x0e;

    // set timer 1 to normal mode, OC1A/B disconnected, input capture
    // noise canceller enabled, input capture on rising edge,
    // prescaler 1024.
    TCCR1A = 0x00;
    TCCR1B = 0xc5;
    // enable timer overflow interrupts, but stop timer
    power_timer1_disable();
    TIMSK1 = TOIE1;

    // Turn off watchdog timer
    wdt_disable();
}

void setup()
{
    setup_interrupts();

    // configure all pins to inputs with pullups to minimize power consumption
    for (uint8_t p = 0; p < 13; ++p)
    {
        pinMode(p, INPUT_PULLUP);
    }
    pinMode(A0, INPUT_PULLUP);
    pinMode(A1, INPUT_PULLUP);
    pinMode(A2, INPUT_PULLUP);
    pinMode(A3, INPUT_PULLUP);
    pinMode(A4, INPUT_PULLUP);
    pinMode(A5, INPUT_PULLUP);
    pinMode(A6, INPUT_PULLUP);
    pinMode(A7, INPUT_PULLUP);

    // then configure the ones we want
    pinMode(xbee_sleep_pin, INPUT);
    pinMode(charge_pin, INPUT_PULLUP);
    pinMode(fault_pin, INPUT_PULLUP);
    pinMode(rainmeter_pin, INPUT_PULLUP);
    pinMode(windspeed_pin, INPUT_PULLUP);
    pinMode(LED_BUILTIN, OUTPUT);
    digitalWrite(LED_BUILTIN, false);
    pinMode(winddir_supply_pin, OUTPUT);
    digitalWrite(winddir_supply_pin, false);

    pinMode(A0, OUTPUT);  // for scope debugging
    pinMode(A1, INPUT);
    pinMode(A2, INPUT);
    pinMode(A3, INPUT);

    // set analog reference to internal so it has time to settle
    analogReference(INTERNAL);
    ADCSRA |= _BV(ADSC);  // start a conversion

    // configure sleep mode
    set_sleep_mode(SLEEP_MODE_IDLE);

    Serial.begin(19200);
    digitalWrite(LED_BUILTIN, true);
    delay(100);
    wait_for_xbee();
    digitalWrite(LED_BUILTIN, false);
    Serial.print("\n\n\n\nPatrik's weather station booting\nfirmware version: ");
    // version::print();
    Serial.println();

    tsl2561.begin();
    tsl2561.setPowerUp();

    if (!bmp180.begin())
    {
        Serial.println("BMP180 initialization failed");
    }

    htu21d.begin();
}

void acc_delay(int duration, int & time)
{
    delay(duration);
    time -= duration;
}

double readVcc()
{
    long result;
    // Read 1.1V reference against AVcc
    ADMUX = _BV(REFS0) | _BV(MUX3) | _BV(MUX2) | _BV(MUX1);
    delay(2);             // Wait for Vref to settle
    ADCSRA |= _BV(ADSC);  // Convert
    while (bit_is_set(ADCSRA, ADSC))
        ;
    result = ADCL;
    result |= ADCH << 8;
    double const vcc = 1126.400 / result;  // Back-calculate AVcc in mV
    return vcc;
}

float read_winddir_resistance()
{
    /* because the wind dir voltage divider has comparatively low
     * resistance, we don't want to power it from Vcc. Instead, we
     * power it from an output pin so we can turn off the supply when
     * we're not measuring.
     */
    digitalWrite(winddir_supply_pin, true);
    // mux in the wind dir pin to let it settle
    ADMUX = _BV(REFS0) | winddir_pin;
    delay(2);
    float const winddir_vd = analogRead(winddir_pin) / 1024.;
    // the reference resistor is 10k, so V/Vcc is R/(R+10k)
    // If V/Vcc ~ 1.0, R is an open circuit which we pretend is a huge res.
    float const winddir_res =
        winddir_vd < 1.0 ? winddir_reference_resistor * winddir_vd / (1 - winddir_vd)
                         : 1e10;

    digitalWrite(winddir_supply_pin, false);

    return winddir_res;
}

float fluxratio = NAN;

// read tsl 2561
void read_lux(float & lux, bool & saturated, float & ch0, float & ch1, bool extrapolate)
{
    // 14ms integration time is useless, because the device saturates
    // on count rate already at 5047 counts. At 100ms, it saturates at
    // ~37177, and at 400ms, we get the full range. The exact value
    // depends on the oscillator frequency, so can be a bit below
    // 37177.
    unsigned const saturation_value = 37000;

    // start with 1x gain, 100ms integration, to see what the counts are
    unsigned char gain        = 0;
    uint8_t       integration = 1;

    unsigned integration_time;
    tsl2561.setPowerUp();
    tsl2561.setTiming(gain, 3);  // set manual to not have it go 400
    // then set 100ms.
    tsl2561.setTiming(gain, integration, integration_time);
    delay(integration_time + 10);
    uint16_t ch0cnt, ch1cnt;
    bool     status = tsl2561.getData(ch0cnt, ch1cnt);
    ch0             = ch0cnt;
    ch1             = ch1cnt;

    if (!status)
    {
        // read failed
        lux = NAN;
        tsl2561.setPowerDown();
        return;
    }

    // check saturation.
    saturated = false;
    if (ch0 > saturation_value || ch1 > saturation_value)
    {
        saturated = true;
        if (extrapolate && ch1 <= saturation_value)
        {
            // fake ch0 value based on ch1 and the last good flux ratio
            ch0 = fluxratio * ch1;
        }
        else
        {
            lux = NAN;
            return;
        }
    }
    else
    {
        // save flux ratio for later saturation
        fluxratio = 1.0 * ch0 / ch1;
    }

    // see if it would behoove us to up the integration time and gain
    if (ch0 < 0xfff0 / 4 && ch1 < 0xfff0 / 4)
    {
        // we have room for 400ms
        integration = 2;

        if (ch0 < 0xff00 / 64 && ch1 < 0xff00 / 64)
        {
            // we even have room for 16x gain
            gain = 1;
        }

        tsl2561.setTiming(gain, 3);  // set manual to interrupt current integration
        tsl2561.setTiming(gain, integration, integration_time);
        delay(410);
        status = tsl2561.getData(ch0cnt, ch1cnt);
        ch0    = ch0cnt;
        ch1    = ch1cnt;
    }

    if (status)
    {
        status = tsl2561.getLux(gain, integration_time, ch0, ch1, lux);
    }
    if (!status)
    {
        lux = NAN;
    }
    tsl2561.setPowerDown();
}

void loop()
{
    start_wind_measurement();

    /* Read analog inputs that require the 1.1V reference. */

    ADMUX = _BV(REFS1) | _BV(REFS0) | uvsensor_pin;
    delay(10);

    uint16_t const uvsensor_val = analogRead(uvsensor_pin);
    double const   uv_voltage   = uvsensor_val / 1024. * 1.1 * (101.4 + 208) / 101.4;
    // Serial.print("UV sensor voltage: ");
    // Serial.println(uv_voltage);
    // uv sensor outputs 1.0V in darkness and 2.2V at 10mW/cm^2 = 100W/m^2
    double const uv_flux = max((uv_voltage - 1.0) * 10 / 1.2, 0.0) * 10.0;
    // Serial.print("UV flux: ");
    // Serial.print(uv_flux);
    // Serial.print("W/m^2, UVI ");
    /* UVI approximation from
     * http://media.digikey.com/pdf/Application%20Notes/Rohm%20Application%20Notes/ML8511_UV.pdf
     */
    double const uvi = max((uv_voltage - 1.0) * 12.49, 0.0);
    // Serial.println(uvi);

    ADMUX = _BV(REFS1) | _BV(REFS0) | vbat_pin;
    delay(2);

    uint16_t const vbat_val = analogRead(vbat_pin);
    double const   vbat     = vbat_val / 1024. * 1.1 * (100 + 470) / 100;

    // Switch analog reference to Vcc and mux in the 1.1V reference
    // now so it has time to settle
    ADMUX = _BV(REFS0) | _BV(MUX3) | _BV(MUX2) | _BV(MUX1);

    bool  lux_saturation;
    float tsl2561_ch0 = 0, tsl2561_ch1 = 0;
    read_lux(lux, lux_saturation, tsl2561_ch0, tsl2561_ch1, true);

    // TODO add error handling
    char wait = bmp180.startTemperature();
    delay(wait);
    double temp, pressure;
    bmp180.getTemperature(temp);
    wait = bmp180.startPressure(0);
    delay(wait);
    if (!bmp180.getPressure(pressure, temp))
    {
        // Serial.println("BMP180 pressure measurement failed");
    }
    else
    {
        // Serial.print("BMP180 returned ");
        // Serial.print(pressure);
        // Serial.print("mbar, sealevel pressure ");
        // Serial.print(bmp180.sealevel(pressure, 167));
        // Serial.println("mbar");
    }

    float const outside_temp = htu21d.readTemperature();
    float const outside_rh   = htu21d.readHumidity();

    /* Analog reading using VCC reference starts here. */

    double const vcc = readVcc();

    // read wind direction resistance. if it's open circuit, we're
    // between contacts, so then we keep the old value. The largest
    // real value is 120k, so if we get more than 150k, we ignore it.
    float tmp = read_winddir_resistance();
    if (tmp < 150e3)
    {
        winddir_resistance = tmp;
    }

    // these are open-drain outputs, so a low means they are on
    bool const charge = !digitalRead(charge_pin);
    bool const fault  = !digitalRead(fault_pin);

    // Set analog reference to internal 1.1V reference now, and mux in
    // the UV pin, so it has time to settle
    ADMUX = _BV(REFS1) | _BV(REFS0) | uvsensor_pin;

    // sleep until wind timer has expired
    // wait_for_wind_measurement(); // DISABLE to see if this is where it's hanging

    // send data
    wait_for_xbee(3);
    digitalWrite(LED_BUILTIN, HIGH);

    /*
        Serial.println("wind measurement:");
        Serial.println(windcounter);
        Serial.println(last_wind_time);
        Serial.println(windrate);
    */

    uint16_t            crc  = 0x0000;
    const unsigned long tick = millis();
    send_header(17, tick);

    send_data(luxaddr, lux, crc);
    send_data(luxsataddr, lux_saturation ? 1.0f : 0.0f, crc);
    send_data(luxch0addr, tsl2561_ch0, crc);
    send_data(luxch1addr, tsl2561_ch1, crc);
    send_data(presaddr, pressure, crc);
    send_data(ptempaddr, temp, crc);
    send_data(tempaddr, outside_temp, crc);
    send_data(humidityaddr, outside_rh, crc);
    send_data(uvfluxaddr, uv_flux, crc);
    send_data(uviaddr, uvi, crc);
    cli();
    float current_rain_count = raincounter;
    sei();
    send_data(rainaddr, current_rain_count, crc);
    send_data(windaddr, windrate, crc);
    send_data(winddiraddr, winddir_resistance, crc);
    send_data(vbataddr, vbat, crc);
    send_data(vccaddr, vcc, crc);
    send_data(chargeaddr, charge ? 1.0f : 0.0f, crc);
    send_data(faultaddr, fault ? 1.0f : 0.0f, crc);

    bool ack = wait_for_response(crc, ack_wait);

    digitalWrite(LED_BUILTIN, LOW);

    // delay(10000);

    return;
}
#if 0

  // we send the data repeatedly until we get an OK. 
  while (!ack) {
    n_retries++;

    // if we have exceeded the maximum retries, we try to reset stuff
    if(n_retries>max_retries) {
      n_retries=0;
      xb.software_reset();
    }

    {
      // If new probe data are available, we update the temps
      // too. This will only happen if we've gone through the send
      // cycle several times, so then we want to to minimize staleness
      // of data.
      bool update_ready=true;
      for(int b=0; b<nbus; ++b) {
	update_ready = update_ready && w[b]->update_ready();
      }
      if (update_ready)
	for(int b=0; b<nbus; ++b)
	  w[b]->update();
    }

    // send data  
    uint16_t crc=0x0000;
    const bool include_tick=true;
    digitalWrite(commLed, HIGH);
    Serial.print("DATA ");
    Serial.print(n_tot+ (include_tick? 1:0) );
    Serial.print(" TICK ");
    const unsigned long tick = millis();
    Serial.println(tick, HEX);
    n_bad=0;
    // tick first
    if(include_tick) {
        /*
      Serial.write(tickaddr,8);
      Serial.write(reinterpret_cast<const uint8_t*>(&tick), sizeof(tick));
      Serial.write('\n');
      crc = update_crc(crc, tickaddr, 8);
      crc = update_crc(crc, reinterpret_cast<const uint8_t*>(&tick), sizeof(tick));
        */

      Serial.write(hotboxaddr,8);
      Serial.write(reinterpret_cast<const uint8_t*>(&hotbox_out), sizeof(hotbox_out));
      Serial.write('\n');
      crc = update_crc(crc, hotboxaddr, 8);
      crc = update_crc(crc, reinterpret_cast<const uint8_t*>(&hotbox_out), sizeof(hotbox_out));

      /*
      Serial.write(hotboxinaddr,8);
      Serial.write(reinterpret_cast<const uint8_t*>(&hotbox_temp), sizeof(hotbox_temp));
      Serial.write('\n');
      crc = update_crc(crc, hotboxinaddr, 8);
      crc = update_crc(crc, reinterpret_cast<const uint8_t*>(&hotbox_temp), sizeof(hotbox_temp));
      */
    }
    for(int b=0, n=0; b<nbus; ++b)
      for(int i=0; i<w[b]->n_devices(); ++i) {
	n_bad += isnan(temps[n])?1:0;
	Serial.write(w[b]->address(i),8);
	Serial.write(reinterpret_cast<uint8_t*>(&temps[n]), sizeof(temps[n]));
	Serial.write('\n');
      
	// update CRC
	crc = update_crc(crc, w[b]->address(i), 8);
	crc = update_crc(crc, reinterpret_cast<uint8_t*>(&temps[n]), sizeof(temps[n]));
        
	++n;
      }

    Serial.print("DONE CRC=");
    Serial.println(crc, HEX);
    digitalWrite(commLed, LOW);

    // busy loop waiting for serial data for at most ack_wait
    const long int t=millis();
    while(millis()<t+ack_wait && Serial.available()<3);
      
    if(Serial.available()>=3 &&
       Serial.read()=='O' &&
       Serial.read()=='K') {
      //Serial.print("Got OK after ");
      //Serial.println(millis()-t);
      ack=true;
    }
    else {
      // did not get OK. reader might be confused. write a newline
      Serial.write('\n');
    }

    update_hotbox();
  }
}

#endif
