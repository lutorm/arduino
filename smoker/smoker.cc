#include "Adafruit_MAX31865.h"
#include "EventLoop.h"
#include "SFE_MicroOLED.h"
#include "memory.h"
#include "onewiretemp.h"
#include "serial_data.h"
#include <Arduino.h>
#include <avr/sleep.h>
#include <avr/wdt.h>
#include <util/delay.h>

/* Note: do not use the LED_BUILTIN since it's on the SCK pin and is
 * used by SPI. */

bool verbose = false;

uint8_t const cs_pt100_1 = 9;
uint8_t const cs_pt100_2 = 10;
uint8_t const pin_1wire  = 8;
/* Xbee sleep request pin, high means go to sleep. */
uint8_t const pin_xbee_sleep_rq = 4;
/* Xbee sleep status pin, high means sleeping. */
uint8_t const pin_xbee_sleep = 3;

OneWireBus * onewire;
float        thermocouple_temp;

Adafruit_MAX31865 * pt100_1;
Adafruit_MAX31865 * pt100_2;
float               pt100_temp1;
float               pt100_temp2;
float               ardtemp;

/* Reference resistor for PT100 measurements. Nominally 430 ohm but
 * tweaked to get 0C in an ice bath for both probes. (The reference
 * resistors appear to bevery close to 430 but there are other sources
 * of offsets.) ERROR it was not in 3-wire setting so the result was
 * wring. */
float const RREF1 = 430;
float const RREF2 = 430;

float       vbat;
float const vbat_alarm = 3.6;

// 1-wire devices
namespace
{
    const prog_uint8_t a1[] PROGMEM = {0x3B, 0xF9, 0xD9, 0x41, 0x0B, 0xF4, 0x6D, 0x7F};

    const prog_char n1[] PROGMEM = "Thermocouple";

    const prog_uint8_t * addresses[1] = {a1};
    const prog_char *    names[1]     = {n1};
}  // namespace

const prog_uint8_t pt100address[] PROGMEM   = "SMOKRTPx";
const prog_uint8_t vbataddress[] PROGMEM    = "SMKRVBAT";
const prog_uint8_t ardtempaddress[] PROGMEM = "SMKARDTP";

class Display : public Periodic
{
public:
    Display(ms_t period) : Periodic(period)
    {
        dpy = new MicroOLED_SPI(dpy_rst_pin, dpy_dc_pin, dpy_cs_pin);
        dpy->begin();
        dpy->clear(ALL);
        dpy->clear(PAGE);
        dpy->setCursor(0, 0);
        sprint_pgm(*dpy, PSTR("Smoker\n\rbooting"));
        dpy->display();
    }

private:
    void _dispatch(ms_t) override final
    {
        dpy->clear(PAGE);
        if (pixel)
        {
            dpy->pixel(LCDWIDTH - 1, LCDHEIGHT - 1);
        }
        pixel = !pixel;

        if (screen > 12)
        {
            screen = 0;
        }

        if (screen > 10)
        {
            display_extras();
            screen = 0;
        }
        else
        {
            if (vbat < vbat_alarm && pixel)
            {
                display_vbatalarm();
            }
            else
            {
                display_temp();
            }
            screen++;
        }
        dpy->display();
    }

    void display_temp()
    {
        dpy->setCursor(0, 0);
        float const temp = 0.5 * (pt100_temp1 + pt100_temp2);
        if (temp == temp)
        {
            dpy->setFontType(3);
            dpy->print(int(temp));
        }
        else
        {
            dpy->setFontType(1);
            dpy->print("Temp\nNaN");
        }
    }

    void display_vbatalarm()
    {
        dpy->setCursor(0, 8);
        dpy->setFontType(1);
        sprint_pgm(*dpy, PSTR(" VBAT\n LOW!"));
    }

    void display_extras()
    {
        dpy->setCursor(0, 0);
        dpy->setFontType(0);
        sprint_pgm(*dpy, PSTR("Vbat:\n"));
        dpy->print(vbat);
        sprint_pgm(*dpy, PSTR("V\n"));
        sprint_pgm(*dpy, PSTR("Ard:\n"));
        dpy->print(ardtemp);
        sprint_pgm(*dpy, PSTR("C\n"));
        sprint_pgm(*dpy, PSTR("Burner:\n"));
        dpy->print(thermocouple_temp);
        sprint_pgm(*dpy, PSTR("C\n"));
    }

    const uint8_t dpy_rst_pin = 6;  // PD6, Arduino pin D6
    const uint8_t dpy_dc_pin  = 5;  // PD5, Arduino pin D5
    const uint8_t dpy_cs_pin  = 7;  // PD7, Arduino pin D7

    MicroOLED_SPI * dpy;
    int         screen = 0;
    bool        pixel  = false;  // heartbeat pixel
};

Display * dpy;

/* This is defined inline here so it sees all the global objects and
 * can ask them to send. */
class SerialSender : public Periodic
{
public:
    SerialSender(ms_t period);

private:
    void _dispatch(ms_t) override final;
};

SerialSender::SerialSender(ms_t period) : Periodic(period) {}

void SerialSender::_dispatch(ms_t)
{
    digitalWrite(pin_xbee_sleep, LOW);

    const ms_t tick = millis();

    /* Wait for Xbee to wake, with a 1s timeout. */
    bool timeout = false;
    while ((digitalRead(pin_xbee_sleep) == HIGH) && !timeout)
    {
        timeout = millis() - tick > 1000;
    }

    if (timeout)
    {
        Serial.println("Timeout waiting for Xbee wakeup");
    }

    uint16_t crc = 0x0000;
    send_header(5, tick);

    uint8_t addr[8];
    memcpy_P(addr, addresses[0], 8);
    ::send_data(addr, thermocouple_temp, crc);

    memcpy_P(addr, pt100address, 8);
    addr[7] = '1';
    ::send_data(addr, pt100_temp1, crc);
    memcpy_P(addr, pt100address, 8);
    addr[7] = '2';
    ::send_data(addr, pt100_temp2, crc);

    memcpy_P(addr, vbataddress, 8);
    ::send_data(addr, vbat, crc);

    memcpy_P(addr, ardtempaddress, 8);
    ::send_data(addr, ardtemp, crc);

    // we dont' even wait for a response, that's useless.
    bool ack = wait_for_response(crc, 0);

    /* Put Xbee back to sleep, except in verbose mode since then other
     * things are printed, too. */
    if (!verbose)
    {
        digitalWrite(pin_xbee_sleep, HIGH);
    }
}

SerialSender sender(10000);

bool check_max31865_fault(Adafruit_MAX31865 & max)
{
    // Check and print any faults
    uint8_t const fault = max.readFault();
    if (fault)
    {
        if (verbose)
        {
            Serial.print("Fault 0x");
            Serial.println(fault, HEX);
            if (fault & MAX31865_FAULT_HIGHTHRESH)
            {
                Serial.println("RTD High Threshold");
            }
            if (fault & MAX31865_FAULT_LOWTHRESH)
            {
                Serial.println("RTD Low Threshold");
            }
            if (fault & MAX31865_FAULT_REFINLOW)
            {
                Serial.println("REFIN- > 0.85 x Bias");
            }
            if (fault & MAX31865_FAULT_REFINHIGH)
            {
                Serial.println("REFIN- < 0.85 x Bias - FORCE- open");
            }
            if (fault & MAX31865_FAULT_RTDINLOW)
            {
                Serial.println("RTDIN- < 0.85 x Bias - FORCE- open");
            }
            if (fault & MAX31865_FAULT_OVUV)
            {
                Serial.println("Under/Over voltage");
            }
        }
        max.clearFault();

        return false;
    }

    return true;
}

float measure_max31865(Adafruit_MAX31865 & max, float RREF)
{
    bool     fault;
    uint16_t rtd   = max.readRTD();
    float    ratio = rtd;
    ratio /= 32768;
    float temp = max.temperature(100, RREF, fault);
    if (verbose)
    {
        Serial.print("RTD value: ");
        Serial.println(rtd);
        Serial.print("Ratio = ");
        Serial.println(ratio, 8);
        Serial.print("Resistance = ");
        Serial.println(RREF * ratio, 8);
        Serial.print("Temp = ");
        Serial.println(temp);
    }

    if (fault)
    {
        if (verbose)
        {
            Serial.print("RTD fault detected: ");
        }
        if (!check_max31865_fault(max))
        {
            temp = NAN;
        }
        else
        {
            if (verbose)
            {
                Serial.println();
            }
        }
    }

    return temp;
}

float measure_vbat()
{
    // Select analog input 0
    ADMUX = _BV(REFS1) | _BV(REFS0);
    delay(1);

    uint16_t result;
    ADCSRA |= _BV(ADSC);  // Convert
    while (bit_is_set(ADCSRA, ADSC))
        ;
    result = ADCL;
    result |= ADCH << 8;

    // Voltage at A0.
    float const vadc = 1.1 * result / 1023;
    // RAW is connected to A0 via a 47.1k/10.1k voltage divider
    float const vbat = vadc * 5.577;  //(10.1+47.1)/10.1;

    if (verbose)
    {
        Serial.print("Vbat: ");
        Serial.println(vbat);
    }
    return vbat;
}

float measure_ardtemp()
{
    // Select temperature sensor
    ADMUX = _BV(REFS1) | _BV(REFS0) | _BV(MUX3);
    delay(1);

    uint16_t result;
    ADCSRA |= _BV(ADSC);  // Convert
    while (bit_is_set(ADCSRA, ADSC))
        ;
    result = ADCL;
    result |= ADCH << 8;

    // Temp sensor voltage .
    float const vadc = 1.1 * result / 1023;
    // Conversion to C (1mV/C and observationaly 380mV at +25C)
    float const temp = (vadc - 0.380) / 0.001 + 25;

    if (verbose)
    {
        Serial.print("Ardtemp: ");
        Serial.println(temp);
    }
    return temp;
}

/* This is defined inline here so it sees all the global objects and
 * can ask them to send. */
class MeasurementEvent : public Periodic
{
public:
    MeasurementEvent(ms_t period);

private:
    void _dispatch(ms_t) override final;
};

MeasurementEvent::MeasurementEvent(ms_t period) : Periodic(period) {}

void MeasurementEvent::_dispatch(ms_t)
{
    pt100_temp1 = measure_max31865(*pt100_1, RREF1);
    if (verbose)
    {
        Serial.print("Temperature = ");
        Serial.println(pt100_temp1);
    }

    // Serial.println("\nPT100 probe 2:");
    pt100_temp2 = measure_max31865(*pt100_2, RREF2);
    if (verbose)
    {
        Serial.print("Temperature = ");
        Serial.println(pt100_temp2);
    }

    onewire->update();
    // Serial.print("\nThermocouple temp: ");
    // Serial.println(thermocouple_temp);

    // measure the battery voltage
    vbat    = measure_vbat();
    ardtemp = measure_ardtemp();
}

MeasurementEvent measure(2000);

void setup()
{
    uint8_t mcusr = MCUSR;
    MCUSR         = 0;

    Serial.begin(19200);

    pinMode(pin_xbee_sleep, INPUT);
    pinMode(pin_xbee_sleep_rq, OUTPUT);
    digitalWrite(pin_xbee_sleep, false);

    event_loop.add_event(&measure);

    dpy = new Display(2000);
    event_loop.add_event(dpy);

    onewire = new OneWireBus(pin_1wire, addresses, names, 1, 1, verbose);
    onewire->set_output(0, &thermocouple_temp);

    pt100_1 = new Adafruit_MAX31865(cs_pt100_1);
    pt100_2 = new Adafruit_MAX31865(cs_pt100_2);

    pt100_1->begin(MAX31865_3WIRE);
    pt100_2->begin(MAX31865_3WIRE);

    // enable adc, prescaler 128, no interrupts
    ADCSRA = _BV(ADEN) | _BV(ADPS0) | _BV(ADPS1) | _BV(ADPS2);
    // Read ADC0 against the 1.1V reference
    ADMUX = _BV(REFS1) | _BV(REFS0);

    event_loop.add_event(&sender);

    // Set sleep mode to IDLE. This keeps the timers running.
    set_sleep_mode(SLEEP_MODE_IDLE);

    /* Set up watchdog timer to enable watchdog interrupt every
     * 250ms.  */
    cli();
    wdt_reset();
    WDTCSR |= _BV(WDE) | _BV(WDCE);
    WDTCSR = _BV(WDIF) | _BV(WDIE) | _BV(WDP2);
    sei();

    sprint_pgm(PSTR("Smoker controller booted successfully\r\n"));
    Serial.print(get_free_memory());
    sprint_pgm(PSTR(" bytes free\r\n"));
}

ISR(WDT_vect) {}

void loop()
{
    event_loop.dispatch(millis());

    /* Go to sleep and let watchdog (or anything else) wake us up. In
     * idle mode the timers continue to tick so the time will be
     * correct. */
    cli();
    sleep_enable();
    sei();
    // put us to sleep.
    sleep_cpu();
    sleep_disable();
    sei();
}
