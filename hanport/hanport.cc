#include "AltSoftSerial.h"
#include "memory.h"
#include <Arduino.h>
#include <avr/power.h>
#include <avr/sleep.h>
#include <avr/wdt.h>

// HAN port is hooked up to hardware serial because it needs to receive at 115k.
// we use the usart directly instead of the arduino api.

// AltSoftSerial then writes to Xbee at a lower rate so it can keep up.
AltSoftSerial *  xbee_serial;

// AltSoftSerial uses external pin interrupts so is fixed to use these
// on 328. (These are actually dummys, just to remember what they are.)
uint8_t const xbee_rx_pin = 8;
uint8_t const xbee_tx_pin = 9;

ISR(BADISR_vect)
{
    for (int i = 0; i < 0xffff; ++i)
    {
        digitalWrite(LED_BUILTIN, true);
        digitalWrite(LED_BUILTIN, false);
    }
}

/* Configure interrupts and setup hardware for minimum power
 * consumption.
 */

void setup_interrupts()
{
    // we want to disable all pin change interrupts
    PCICR  = 0x04;
    PCMSK2 = 0x00;

    // also set the power reduction register to disable timers 2
    // (timer 0 is for the tick and AltSoftSerial uses timer 1) and SPI.
    power_timer2_disable();
    power_spi_disable();

    // disable analog comparator
    ACSR = 0x80;

    // Turn off watchdog timer
    wdt_disable();
}

void USART_Transmit( unsigned char data ) {
/* Wait for empty transmit buffer */
    while ( !( UCSR0A & (1<<UDRE0)) );
/* Put data into buffer, sends the data */
    UDR0 = data;
}

void setup()
{
    setup_interrupts();

    // configure all pins to inputs with pullups to minimize power consumption
    for (uint8_t p = 0; p < 13; ++p)
    {
        pinMode(p, INPUT_PULLUP);
    }
    pinMode(A0, INPUT_PULLUP);
    pinMode(A1, INPUT_PULLUP);
    pinMode(A2, INPUT_PULLUP);
    pinMode(A3, INPUT_PULLUP);
    pinMode(A4, INPUT_PULLUP);
    pinMode(A5, INPUT_PULLUP);
    pinMode(A6, INPUT_PULLUP);
    pinMode(A7, INPUT_PULLUP);

    // configure sleep mode
    set_sleep_mode(SLEEP_MODE_IDLE);

    xbee_serial = new AltSoftSerial();
    xbee_serial->begin(9600);


    // configure the usart
    UCSR0A = (1<<U2X0); // double speed mode
    UCSR0B = 0x00;
    UCSR0C = (1<<UCSZ01) | (1<<UCSZ00); // async, 8 bit, 1sb, no parity
    UBRR0L = 16; // 115k baud at 16MHz, double speed (3.5% error)
    UBRR0H = 0;
    UCSR0B = (1<<RXEN0) | (1<<TXEN0); // enable rx and tx
    
    // wait a few sec so xbee link can come up
    //delay(3000);
    digitalWrite(LED_BUILTIN, true);
    xbee_serial->print("\n\n\n\nPatrik's HANport relay booting\n");
    xbee_serial->println();
    USART_Transmit('H');
    USART_Transmit('E');
    USART_Transmit('J');
    USART_Transmit('!');
    
    delay(100);
    digitalWrite(LED_BUILTIN, false);    
}

void increment_buffer_pos(int & pos, int const size)
{
    ++pos;
    if (pos == size)
    {
        pos=0;
    }
}

void loop()
{
    // ring buffer needs to be large enough to accommodate almost the
    // entire burst from the HANport, since it sends much faster than
    // the xbee can.
    const int bufsize = 1024;
    char      buf[bufsize];
    int       rx_pos=0,tx_pos=0;

    // Communication is strictly one-way, we receive from the HAN
    // port, buffer it, and send out to Xbee.
/*
    uint8_t dummy;
    while(true)
    {
        while ( !(UCSR0A & (1<<RXC0)) );
        if (UCSR0A & (1<<FE0))
        {
            USART_Transmit('F');
            USART_Transmit('E');
        }
        USART_Transmit(UDR0);
    }
*/    
    while (true)
    {
        // if a byte is available from HANport, read it
        if (UCSR0A & (1<<RXC0))
        {
            // check for framing error. This must be done before reading out the input
            if (UCSR0A & (1<<FE0))
            {
                // USART signaled a framing error, put a marker in the buffer
                buf[rx_pos] = '$';
                increment_buffer_pos(rx_pos, bufsize);
            }
            
            buf[rx_pos] = UDR0;
            USART_Transmit(buf[rx_pos]);
            increment_buffer_pos(rx_pos, bufsize);

            // If tx and rx positions are equal now, we ran out of
            // buffer space.  In this case we output a
            // marker so we can notice remotely.
            if (tx_pos == rx_pos)
            {
                buf[rx_pos] = '@';
                increment_buffer_pos(rx_pos, bufsize);
            }
        }
        
        // check if there is an unsent byte available
        if (tx_pos != rx_pos)
        {
            digitalWrite(LED_BUILTIN, true);

            // if we can write without blocking, write a byte.
            if (xbee_serial->canWrite())
            {
                xbee_serial->write(buf[tx_pos]);
                increment_buffer_pos(tx_pos, bufsize);
            }
        }
        else
        {
            // no bytes available, turn led off
            digitalWrite(LED_BUILTIN, false);
        }
    }
}
