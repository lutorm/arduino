#include <Arduino.h>
#include <Mtxorb.h>
#include <OneWire.h>
#include <PID.h>
#include <Print.h>
#include <SoftwareSerial.h>
#include <avr/pgmspace.h>
#include <displaylist.h>
#include <frequency.h>
#include <memory.h>
#include <onewiretemp.h>
#include <version.h>

/* TO DO:

 - a way to *get* variable values over the serial line
 - alarms?
 - reading rpm on the mtxorb is screwed up.

*/

// one-wire device addresses
namespace onewiredevs
{
    const prog_uint8_t   a1[] PROGMEM = {0x28, 0x41, 0x0a, 0x72, 0x02, 0x00, 0x00, 0x01};
    const prog_uint8_t   a2[] PROGMEM = {0x10, 0xD3, 0x6A, 0xBE, 0x00, 0x08, 0x00, 0x38};
    const prog_uint8_t   a3[] PROGMEM = {0x28, 0x18, 0x33, 0xD0, 0x01, 0x00, 0x00, 0x84};
    const prog_uint8_t   a4[] PROGMEM = {0x28, 0x1D, 0x12, 0xF6, 0x01, 0x00, 0x00, 0x23};
    const prog_uint8_t   a5[] PROGMEM = {0x28, 0xD4, 0x30, 0xD0, 0x01, 0x00, 0x00, 0x28};
    const prog_uint8_t   a6[] PROGMEM = {0x10, 0x5F, 0x69, 0xBE, 0x00, 0x08, 0x00, 0xE1};
    const prog_uint8_t   a7[] PROGMEM = {0x10, 0x85, 0x31, 0xE4, 0x00, 0x08, 0x00, 0x82};
    const prog_uint8_t   a8[] PROGMEM = {0x10, 0x55, 0x74, 0xE4, 0x00, 0x08, 0x00, 0x7D};
    const prog_uint8_t   a9[] PROGMEM = {0x10, 0x83, 0x56, 0xE4, 0x00, 0x08, 0x00, 0x70};
    const prog_char      n1[] PROGMEM = "Arduino";
    const prog_char      n2[] PROGMEM = "Case intake";
    const prog_char      n3[] PROGMEM = "Rad outlet";
    const prog_char      n4[] PROGMEM = "Rad inlet";
    const prog_char      n5[] PROGMEM = "Rad exhaust";
    const prog_char      n6[] PROGMEM = "Back VRM";
    const prog_char      n7[] PROGMEM = "Top VRM";
    const prog_char      n8[] PROGMEM = "GPU VRM";
    const prog_char      n9[] PROGMEM = "Raptor";
    const int            n_devs       = 9;
    const prog_uint8_t * addresses[n_devs] = {a1, a2, a3, a4, a5, a6, a7, a8, a9};
    const prog_char *    names[n_devs]     = {n1, n2, n3, n4, n5, n6, n7, n8, n9};

    // Make an enum with symbolic names to the probes
    enum
    {
        Arduino,
        Case_intake,
        Rad_out,
        Rad_in,
        Rad_exh,
        Back_vrm,
        Top_vrm,
        Gpu_vrm,
        Raptor
    } probes;

    // State variables for the temp probes
    float temps[n_devs];
}  // namespace onewiredevs

// Display rx is connected to pin 12 and tx to pin 11.
SoftwareSerial mySerial(11, 12);

// the disp has state variables so it must be inited here
typedef Mtxorb<SoftwareSerial> T_disp;
T_disp                         disp(mySerial);
const byte                     disp_waterflow_fan = 0;

frequency case_fan_rpm(3, 2);

// state variables
namespace state
{
    float radiator_setpoint = 24.0;
    // The radiator fan regulator output
    float radiator_fan_pidout = 0;
    ;
    int rad_fan_rpm = 0;

    float case_fan_setpoint = 35.0;
    // The case fan regulator output
    float case_fan_pidout = 0;
    // Case fan rpm (written to from the frequency counter because we
    // want it in rpm not hertz.)
    int case_fan_rpm;

    // tick counter
    long int ticks;

    // derived variables (updated in the mainloop from primitive ones)

    // radiator delta-T values
    float rad_air_dt, rad_coolant_dt;
    // radiator mean temp
    float rad_mean_temp;

    // histogram objects
    hist<float, 18> case_intake_hist(
        &onewiredevs::temps[onewiredevs::Case_intake], 10, 18., 25., true);

    hist<float, 18>
        watertemp_hist(&onewiredevs::temps[onewiredevs::Rad_in], 60, 20., 32., true);
    hist<int, 18>
                    waterflow_hist(disp.rpm_pointer(disp_waterflow_fan), 60, 1800., 2300., true);
    hist<float, 18> radfan_hist(&radiator_fan_pidout, 60, 0., 100., true);
    hist<float, 18> radairdt_hist(&rad_air_dt, 60, 0., 3., true);
    hist<float, 18> radcoolantdt_hist(&rad_coolant_dt, 60, 0., 3., true);

    hist<float, 18> casefan_hist(&case_fan_pidout, 60, 0., 100., true);
    hist<float, 16>
        topvrm_hist(&onewiredevs::temps[onewiredevs::Top_vrm], 60, 30., 50., true);
    hist<float, 16>
        bakvrm_hist(&onewiredevs::temps[onewiredevs::Back_vrm], 60, 30., 50., true);
    hist<float, 16>
        gpuvrm_hist(&onewiredevs::temps[onewiredevs::Gpu_vrm], 60, 50., 70., true);
};  // namespace state

// strings in program memory
namespace strs
{
    const prog_char fmt52f[] PROGMEM         = "%5.2f";
    const prog_char fmt4d[] PROGMEM          = "%4d";
    const prog_char fmt40f[] PROGMEM         = "%4.0f";
    const prog_char rpm[] PROGMEM            = "RPM";
    const prog_char percentopenpar[] PROGMEM = "% (";
    const prog_char percentbar[] PROGMEM     = "%|";
    const prog_char percentspace[] PROGMEM   = "% ";
    const prog_char percent[] PROGMEM        = "%";
    const prog_char arduinotimer[] PROGMEM   = "Arduino timer is";
    const prog_char colonspace[] PROGMEM     = ": ";
    const prog_char spaceC[] PROGMEM         = " C";
    const prog_char Cbar[] PROGMEM           = "C|";
    const prog_char Wbar[] PROGMEM           = "W|";
    const prog_char Fbar[] PROGMEM           = "F|";
    const prog_char bar[] PROGMEM            = "|";
    const prog_char slash[] PROGMEM          = " / ";
    const prog_char fan[] PROGMEM            = "Fan";
    const prog_char vrm[] PROGMEM            = "VRM";
    const prog_char topbar[] PROGMEM         = "TOP|";
    const prog_char bakbar[] PROGMEM         = "BAK|";
    const prog_char radt[] PROGMEM           = "Rad T";
    const prog_char waterflow[] PROGMEM      = "Water flow";
    const prog_char radfan[] PROGMEM         = "Radiator fan";
    const prog_char rdt[] PROGMEM            = "RDT";
    const prog_char air[] PROGMEM            = "AIR";
    const prog_char h2o[] PROGMEM            = "H2O";
};  // namespace strs

// global instances of display items to avoid malloc
namespace items
{
    // general strings
    d_pstring rpm(strs::rpm);
    d_pstring percentopenpar(strs::percentopenpar);
    d_pstring percentbar(strs::percentbar);
    d_pstring percentspace(strs::percentspace);
    d_pstring percent(strs::percent);
    d_pstring colonspace(strs::colonspace);
    d_pstring spaceC(strs::spaceC);
    d_pstring Cbar(strs::Cbar);
    d_pstring Wbar(strs::Wbar);
    d_pstring Fbar(strs::Fbar);
    d_pstring bar(strs::bar);
    d_pstring slash(strs::slash);
    d_pstring fan(strs::fan);
    d_pstring vrm(strs::vrm);
    d_pstring rdt(strs::rdt);
    d_pstring air(strs::air);
    d_pstring h2o(strs::h2o);
    // there's something wrong here, without this buffer, the h2o
    // pstring seems to be corrupted. Does it have something to do with
    // the empty line object?
    d_pstring crap(strs::h2o);

    displayline<0> empty_line;

    d_pstring    intake(onewiredevs::names[onewiredevs::Case_intake]);
    d_var<float> intaketemp(&onewiredevs::temps[onewiredevs::Case_intake], strs::fmt52f);
    displayline<4> intake_l1(&intake, &colonspace, &intaketemp, &spaceC);

    d_hist         intakehist1(&state::case_intake_hist, 1);
    d_hist         intakehist0(&state::case_intake_hist, 0);
    displayline<2> intakehist_l1(&Cbar, &intakehist1);
    displayline<2> intakehist_l2(&Cbar, &intakehist0);

    d_var<float>   backvrmtemp(&onewiredevs::temps[onewiredevs::Back_vrm], strs::fmt52f);
    d_var<float>   topvrmtemp(&onewiredevs::temps[onewiredevs::Top_vrm], strs::fmt52f);
    displayline<6> case_l1(&vrm, &colonspace, &backvrmtemp, &slash, &topvrmtemp, &spaceC);

    d_var<float>   casefan(&state::case_fan_pidout, strs::fmt52f);
    d_var<int>     casefanrpm(&state::case_fan_rpm, strs::fmt4d);
    displayline<6> case_l2(&fan, &colonspace, &casefan, &percentspace, &casefanrpm, &rpm);

    d_hist         casefanhist1(&state::casefan_hist, 1);
    d_hist         casefanhist0(&state::casefan_hist, 0);
    displayline<2> casehist_l1(&Cbar, &casefanhist1);
    displayline<2> casehist_l2(&percentbar, &casefanhist0);

    d_pstring      topbar(strs::topbar);
    d_hist         topvrmhist1(&state::topvrm_hist, 1);
    d_hist         topvrmhist0(&state::topvrm_hist, 0);
    displayline<2> topvrmhist_l1(&topbar, &topvrmhist1);
    displayline<3> topvrmhist_l2(&vrm, &bar, &topvrmhist0);

    d_pstring      bakbar(strs::bakbar);
    d_hist         bakvrmhist1(&state::bakvrm_hist, 1);
    d_hist         bakvrmhist0(&state::bakvrm_hist, 0);
    displayline<2> bakvrmhist_l1(&bakbar, &bakvrmhist1);
    displayline<3> bakvrmhist_l2(&vrm, &bar, &bakvrmhist0);

    d_pstring      radt(strs::radt);
    d_var<float>   radin(&onewiredevs::temps[onewiredevs::Rad_in], strs::fmt52f);
    d_var<float>   radout(&onewiredevs::temps[onewiredevs::Rad_out], strs::fmt52f);
    d_var<float>   radexh(&onewiredevs::temps[onewiredevs::Rad_exh], strs::fmt52f);
    d_pstring      exh(onewiredevs::names[onewiredevs::Rad_exh]);
    displayline<5> radt_l1(&radt, &colonspace, &radin, &slash, &radout);
    displayline<4> radt_l2(&exh, &colonspace, &radexh, &spaceC);

    d_pstring      waterflow(strs::waterflow);
    d_var<int>     flowrate(disp.rpm_pointer(disp_waterflow_fan), strs::fmt4d);
    d_pstring      radfan(strs::radfan);
    d_var<float>   radfanout(&state::radiator_fan_pidout, strs::fmt52f);
    displayline<3> radfan_l1(&waterflow, &colonspace, &flowrate);
    displayline<4> radfan_l2(&radfan, &colonspace, &radfanout, &percent);

    d_hist         radtemphist1(&state::watertemp_hist, 1);
    d_hist         radtemphist0(&state::watertemp_hist, 0);
    displayline<2> radtemphist_l1(&Wbar, &radtemphist1);
    displayline<2> radtemphist_l2(&Cbar, &radtemphist0);

    d_hist         radairdt_hist1(&state::radairdt_hist, 1);
    d_hist         radairdt_hist0(&state::radairdt_hist, 0);
    displayline<3> radairdt_l1(&rdt, &bar, &radairdt_hist1);
    displayline<3> radairdt_l2(&air, &bar, &radairdt_hist0);

    d_hist         radcoolantdt_hist1(&state::radcoolantdt_hist, 1);
    d_hist         radcoolantdt_hist0(&state::radcoolantdt_hist, 0);
    displayline<3> radcoolantdt_l1(&rdt, &bar, &radcoolantdt_hist1);
    displayline<3> radcoolantdt_l2(&h2o, &bar, &radcoolantdt_hist0);

    d_hist         radfan_hist1(&state::radfan_hist, 1);
    d_hist         radfan_hist0(&state::radfan_hist, 0);
    displayline<3> radfanhist_l1(&Wbar, &radfan_hist1);
    displayline<3> radfanhist_l2(&percentbar, &radfan_hist0);

    d_hist         waterflow_hist1(&state::waterflow_hist, 1);
    d_hist         waterflow_hist0(&state::waterflow_hist, 0);
    displayline<3> waterflowhist_l1(&Wbar, &waterflow_hist1);
    displayline<3> waterflowhist_l2(&Fbar, &waterflow_hist0);

};  // namespace items

// rpm reading works, but the radiator fan does not have a hall sensor
// and the case fan gives unreliable outputs at <25% speeds, so not much point.
const byte onewirepin     = 8;
const byte fan1_pwm_pin   = 10;
const byte fan1_tach_pin  = 3;
const byte fan2_pwm_pin   = 9;
const byte fan2_tach_pin  = 2;
const byte serial_dcd_pin = 4;

OneWireBus *  w;
PID *         radiator_pid, *casefan_pid;
displaylist * pages;

// char buf[50];
const byte fan  = 1;
long int   last = 0;

void setup()
{
    Serial.begin(57600);
    sprint_pgm(PSTR("Initing version "));
    sprint_pgm(version);
    sprint_pgm(PSTR("\n\r"));

    // set the data rate for the SoftwareSerial port
    mySerial.begin(19200);

    // create Mtxorb object and init display
    disp.clear();
    disp.set_backlight(true);
    disp.auto_line_wrap(false);
    disp.print("Initializing...\n");
    disp.set_pwm_freq(9);

    // onewire bus is connected to pin 13
    w = new OneWireBus(8,
                       onewiredevs::addresses,
                       onewiredevs::names,
                       onewiredevs::n_devs,
                       true);
    for (byte i = 0; i < onewiredevs::n_devs; ++i)
    {
        // init temp var to NaN, so that will be the value for unmapped entries
        onewiredevs::temps[i] = NAN;
        w->set_output(i, &onewiredevs::temps[i]);
    }
    w->update();
    w->update();
    w->set_verbose(false);

    radiator_pid = new PID(&onewiredevs::temps[onewiredevs::Rad_in],
                           &state::radiator_fan_pidout,
                           &state::radiator_setpoint,
                           40.0f,
                           0.5f,
                           0.01f,
                           false);
    radiator_pid->SetOutputLimits(0.0f, 100.0f);
    // no point in updating faster than we get temp data
    radiator_pid->SetSampleTime(1000);
    radiator_pid->SetTimeout(30);
    radiator_pid->SetEngaged(true);

    casefan_pid = new PID(&onewiredevs::temps[onewiredevs::Top_vrm],
                          &state::case_fan_pidout,
                          &state::case_fan_setpoint,
                          20.0f,
                          .1f,
                          0.01f,
                          false);
    casefan_pid->SetOutputLimits(0.0f, 100.0f);
    casefan_pid->SetSampleTime(1000);
    casefan_pid->SetTimeout(30);
    casefan_pid->SetEngaged(true);

    // init pin modes for the pan pwm outputs and tach inputs
    pinMode(fan1_pwm_pin, OUTPUT);
    digitalWrite(fan1_tach_pin, LOW);
    pinMode(fan1_tach_pin, INPUT);
    pinMode(fan2_pwm_pin, OUTPUT);
    digitalWrite(fan2_tach_pin, LOW);
    pinMode(fan2_tach_pin, INPUT);
    pinMode(serial_dcd_pin, OUTPUT);
    digitalWrite(serial_dcd_pin, HIGH);

    pages = new displaylist(4, 5000, 255, 120);

    pages->append(&items::intake_l1);
    pages->append(&items::empty_line);

    pages->append(&items::intakehist_l1);
    pages->append(&items::intakehist_l2);

    pages->append(&items::case_l1);
    pages->append(&items::case_l2);

    pages->append(&items::casehist_l1);
    pages->append(&items::casehist_l2);

    pages->append(&items::topvrmhist_l1);
    pages->append(&items::topvrmhist_l2);

    pages->append(&items::bakvrmhist_l1);
    pages->append(&items::bakvrmhist_l2);

    pages->append(&items::radt_l1);
    pages->append(&items::radt_l2);

    pages->append(&items::radfan_l1);
    pages->append(&items::radfan_l2);

    pages->append(&items::radtemphist_l1);
    pages->append(&items::radtemphist_l2);

    pages->append(&items::radairdt_l1);
    pages->append(&items::radairdt_l2);

    pages->append(&items::radcoolantdt_l1);
    pages->append(&items::radcoolantdt_l2);

    pages->append(&items::radfanhist_l1);
    pages->append(&items::radfanhist_l2);

    pages->append(&items::waterflowhist_l1);
    pages->append(&items::waterflowhist_l2);

    sprint_pgm(PSTR("Free mem after initialization: "));
    Serial.println(get_free_memory());
    // delay(2000);
}

void loop()
{
    // call the update methods for the 1-wire bus, the pid controllers,
    // and the mtxorb rpm readout
    w->update();
    radiator_pid->Compute();
    casefan_pid->Compute();
    // disp.update_rpms();
    case_fan_rpm.update_frequency();

    state::ticks = millis();

    // write the pwm outputs (pid output is 0-100, want 255-0 for the
    // output). if pid outputs nan, go to max fan speed.
    if (isnan(state::case_fan_pidout))
        analogWrite(fan1_pwm_pin, byte(0));
    else
        analogWrite(fan1_pwm_pin, byte(255 - 2.55 * state::case_fan_pidout));

    if (isnan(state::radiator_fan_pidout))
        analogWrite(fan2_pwm_pin, byte(0));
    else
        analogWrite(fan2_pwm_pin, byte(255 - 2.55 * state::radiator_fan_pidout));

    // update derived variables
    state::rad_mean_temp = 0.5
                           * (onewiredevs::temps[onewiredevs::Rad_in]
                              + onewiredevs::temps[onewiredevs::Rad_out]);
    state::rad_air_dt = state::rad_mean_temp - onewiredevs::temps[onewiredevs::Rad_exh];
    state::rad_coolant_dt = onewiredevs::temps[onewiredevs::Rad_in]
                            - onewiredevs::temps[onewiredevs::Rad_out];
    state::case_fan_rpm = int(case_fan_rpm.freq_ * 60.0f);

    // update histograms
    state::case_intake_hist.update();
    state::watertemp_hist.update();
    state::waterflow_hist.update();
    state::radfan_hist.update();
    state::radairdt_hist.update();
    state::radcoolantdt_hist.update();
    state::casefan_hist.update();
    state::topvrm_hist.update();
    state::bakvrm_hist.update();
    state::gpuvrm_hist.update();

    // Serial.println(disp.rpm_pointer(0));
    // disp.set_backlight(true);
    // disp.print(temp, 2);
    // disp.print("C          \n");

    pages->update<Mtxorb<SoftwareSerial>, 25>(disp);
    // pages->update_serial();

    // Serial.print(onewiredevs::temps[onewiredevs::Rad_in]);
    // Serial.print(" ");
    // Serial.println(state::radiator_fan_pidout);

    if (millis() - last > 5000)
    {
        // sprint_pgm(PSTR("Free mem : "));
        // Serial.println(get_free_memory());
        sprint_pgm(PSTR("mtxorb packets: "));
        while (mySerial.available())
        {
            Serial.print(mySerial.read(), HEX);
            Serial.print(' ');
        }

        // for(byte b=0;b<3;++b) {
        // Serial.print(*disp.rpm_pointer(b));
        //}
        Serial.println();
        disp.send_cmd(byte(193), 2);
        /*
          char buf[50];
          Serial.println("histo outputted");
          char temp[30];
          char temp2[30];
          memset(temp, 0, 30);
          memset(temp2, 0, 30);
          d_hist hh(&state::histo, 0);
          d_hist hh2(&state::histo, 1);
          //displayline dl(&hh);
          char* t=temp;
          int nn=30;
          //strlcpy_P(temp, (PGM_P)&strs::patriksfan, 30);
          hh.output(&t, &nn);
          nn=30; t=temp2;
          hh2.output(&t, &nn);
          //dl.output(t, nn);
          for(int i=0; i<state::histo.n(); ++i) {
              snprintf(buf,100," %d %f %d %d\n\r",i, state::histo.point(i), byte(temp[i]),
          byte(temp2[i])); Serial.print(buf);
          }
        */
        last = millis();
    }

    delay(10);
}
