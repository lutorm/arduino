#pragma once

#include "AltSoftSerial.h"
#include "EventLoop.h"

/* Handles AirTalk communications. Runs as fast as possible because it
 * needs to be able to squeeze in transmissions.  */
class AirTalk : public Event
{
public:
    AirTalk();

    void dispatch(ms_t now) override;

    /* Queue message for transmission. Actual transmission will happen
     * when the bus is free. */
    void tx_message(uint8_t dest, uint8_t len, uint8_t const * msg);

    /* Queue an airtalk ack. */
    void tx_ack();

    /* Return true if a previously queued message has been sent. It's
     * now OK to queue another or to change the buffer. */
    bool message_sent() const { return _out_msg == 0; }

    /* Return buffer to latest message to be received, or null if none
     * has been received since last call. If a message is available,
     * dest and len are also written. (Note that the buffer may be
     * overwritten at any time, so if you are interested in a message,
     * copy it immediately.) */
    uint8_t const * get_message(uint8_t & dest, uint8_t & len);

    void dump_state();

private:
    struct AckMessage
    {
        AckMessage();

        uint8_t const type;
        uint8_t const subtype;
    };

    AckMessage const _ack;

    void _backoff();
    bool _tx_byte(uint8_t);
    void _find_stx();
    bool _rx_message();

    /* Transmit a queued message. */
    bool _tx_message();

    AltSoftSerial * _airtalk;

    static uint8_t const STX    = 0x82;
    static uint8_t const ETX    = 0x83;
    static uint8_t const buflen = 100;
    uint8_t              _recv_buf[buflen];

    bool    _in_msg;
    bool    _in_buf;
    bool    _do_backoff;
    uint8_t _recv_dest;
    uint8_t _recv_len;

    uint8_t const * _out_msg;
    uint8_t         _out_dest;
    uint8_t         _out_len;
    uint16_t        _backoff_delay;

    ms_t _watchdog;
};
