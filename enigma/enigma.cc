#include "AirTalk.h"
#include "memory.h"
#include <Arduino.h>
#include <avr/power.h>
#include <avr/sleep.h>
#include <avr/wdt.h>

class IOExSender : public Periodic
{
public:
    IOExSender(AirTalk * airtalk) : _airtalk(airtalk) {}

private:
    void _dispatch(ms_t) override;

    AirTalk * _airtalk;

    struct IOExDataMessage
    {
        IOExDataMessage();

        uint8_t const type;
        uint8_t       subtype;
        uint16_t      analog_values[8];
        uint32_t      digital_values;
        int16_t       current;
        uint8_t       output_values;
        uint8_t       buffer;
    };
    struct IOExInputChangeMessage
    {
        IOExInputChangeMessage();

        uint8_t const type;
        uint8_t const subtype;
        uint32_t      digital_values;
    };

    uint16_t _readVcc();
    uint16_t _readTemp();
    uint16_t _readAnalog(uint8_t);

    IOExDataMessage        _data_msg;
    IOExInputChangeMessage _change_msg;
};

IOExSender::IOExDataMessage::IOExDataMessage() :
    type(61), subtype(0), digital_values(0), current(0), output_values(0)
{
    memset(analog_values, 0, sizeof(analog_values));

    if (sizeof(*this) != 26)
    {
        Serial.print("Wrong length of IOExDataMessage: ");
        Serial.println(sizeof(*this));
    }
}

IOExSender::IOExInputChangeMessage::IOExInputChangeMessage() :
    type(61), subtype(1), digital_values(0)
{
    if (sizeof(*this) != 7)
    {
        Serial.print("Wrong length of IOExInputChangeMessage: ");
        Serial.println(sizeof(*this));
    }
}

uint16_t IOExSender::_readVcc()
{
    // Read 1.1V reference against AVcc
    ADMUX = _BV(REFS0) | _BV(MUX3) | _BV(MUX2) | _BV(MUX1);
    delay(1);
    ADCSRA |= _BV(ADSC);  // Convert
    while (bit_is_set(ADCSRA, ADSC))
        ;

    // Reading register "ADCW" takes care of how to read ADCL and ADCH.
    uint16_t const adc_val = ADCW;

    /* Because the Enigma can't scale reciprocal values, we need to
     * rescale it. Send voltage directly in units of 0.01V. */

    /* Calculate Vcc. Full scale (1024) corresponds to Vcc=1.1V. */
    float const vcc = 100 * 1.1f / (adc_val / 1024.0f);  // Calculate AVcc in centivolts
    return vcc;
}

uint16_t IOExSender::_readTemp()
{
    // Select 1.1V reference for ADC and mux in the temperature sensor.
    ADMUX = (_BV(REFS1) | _BV(REFS0) | _BV(MUX3));
    delay(5);

    ADCSRA |= _BV(ADSC);  // Convert
    while (bit_is_set(ADCSRA, ADSC))
        ;

    // Reading register "ADCW" takes care of how to read ADCL and ADCH.
    uint16_t const adc_val = ADCW;

    /* Set reference back to AVCC so it can settle until next measurement. */
    ADMUX = _BV(REFS0);
    return adc_val;
}

uint16_t IOExSender::_readAnalog(uint8_t pin)
{
    // Read pin against AVCC. AVCC should already have been selected so we don't need to
    // wait.
    ADMUX = _BV(REFS0) | pin;
    delay(1);
    ADCSRA |= _BV(ADSC);  // Convert
    while (bit_is_set(ADCSRA, ADSC))
        ;

    // Reading register "ADCW" takes care of how to read ADCL and ADCH.
    uint16_t const adc_val = ADCW;

    return adc_val;
}

void IOExSender::_dispatch(ms_t)
{
    if (!_airtalk->message_sent())
    {
        Serial.println("IOEx messages don't seem to be getting out.");
        _airtalk->dump_state();
        return;
    }
    /*
    for(uint8_t i = 0; i < 4; ++i)
    {
        _msg.analog_values[i] = _readAnalog(i);
    }
    _msg.analog_values[4] = _readVcc();
    _msg.analog_values[5] = _readTemp();
    */
    static int count = 0;
    _data_msg.analog_values[0] += 10;
    if (_data_msg.analog_values[0] > 1023)
        _data_msg.analog_values[0] = 0;
    _data_msg.analog_values[1] = 100;
    _data_msg.analog_values[2] += 1;
    if (_data_msg.analog_values[2] > 1023)
        _data_msg.analog_values[2] = 0;
    _data_msg.analog_values[3] = 0;
    _data_msg.analog_values[4] = 100;
    _data_msg.analog_values[5] = 200;

    //_data_msg.digital_values |= 10;
    if (count > 8)
    {
        // don't change digital state so we don't send a digital change msg
        //_data_msg.digital_values ^= 1 << 15;
        count = 0;
    }
    ++count;

    static bool which = false;
    if (_change_msg.digital_values != _data_msg.digital_values)
    {
        _change_msg.digital_values = _data_msg.digital_values;
        _airtalk->tx_message(0xff,
                             sizeof(_change_msg),
                             reinterpret_cast<uint8_t const *>(&_change_msg));
    }
    else
    {
        _airtalk->tx_message(0xff,
                             sizeof(_data_msg),
                             reinterpret_cast<uint8_t const *>(&_data_msg));
    }
    which = !which;

    /* Print */
    static int msgcount = 0;
    if (msgcount > 10)
    {
        for (uint8_t i = 0; i < 4; ++i)
        {
            Serial.print("pin ");
            Serial.print(i);
            Serial.print('\t');
            float const P = (_data_msg.analog_values[i] / 1024.0f - 0.04f) / 0.09f;
            Serial.print(P);
            Serial.println("kPa");
        }

        Serial.print("Vcc\t");
        Serial.println(_data_msg.analog_values[4] * 0.01f);

        Serial.print("Temperature\t");
        float const T = (_data_msg.analog_values[5] - 324.31f) / 1.22f;
        Serial.println(T);
        Serial.println();
        msgcount = 0;
    }
    ++msgcount;
}

void setup_interrupts()
{
    // we want to disable all pin change interrupts
    PCICR  = 0x00;
    PCMSK0 = 0x0;
    PCMSK1 = 0x0;
    PCMSK2 = 0x0;

    // also set the power reduction register to disable timer 2
    // (timer 0 is for the tick and AltSoftSerial uses timer 1) and SPI.
    power_timer2_disable();
    power_spi_disable();

    // disable analog comparator
    ACSR = 0x80;

    // disable input buffers on analog inputs 0-3
    DIDR0 = 0x0f;

    // Turn off watchdog timer
    wdt_disable();
}

AirTalk *    airtalk;
IOExSender * ioex_sender;

void setup()
{
    setup_interrupts();

    // configure all pins to inputs with pullups to minimize power consumption
    for (uint8_t p = 0; p < 13; ++p)
    {
        pinMode(p, INPUT_PULLUP);
    }
    pinMode(A0, INPUT);
    pinMode(A1, INPUT);
    pinMode(A2, INPUT);
    pinMode(A3, INPUT);
    pinMode(A4, INPUT_PULLUP);
    pinMode(A5, INPUT_PULLUP);
    pinMode(A6, INPUT_PULLUP);
    pinMode(A7, INPUT_PULLUP);

    pinMode(LED_BUILTIN, OUTPUT);
    digitalWrite(LED_BUILTIN, false);

    /* Switch ADC to AVCC reference. */
    ADMUX = _BV(REFS0);

    // configure sleep mode
    set_sleep_mode(SLEEP_MODE_IDLE);

    Serial.begin(57600);
    digitalWrite(LED_BUILTIN, true);
    delay(100);
    digitalWrite(LED_BUILTIN, false);
    Serial.println("\n\n\n\nPatrik's pressure logger booting");

    airtalk = new AirTalk();
    event_loop.add_event(airtalk);

    ioex_sender = new IOExSender(airtalk);
    event_loop.add_event(ioex_sender);
    ioex_sender->set_period(500);
}

void loop()
{
    event_loop.dispatch(millis());

    uint8_t const   addr = 0xe8;
    uint8_t         dest, len;
    uint8_t const * buf;
    if ((buf = airtalk->get_message(dest, len)) && dest == 0xe8 && len >= 2)
    {
        /* Message addressed to us. */

        if (buf[0] == 60)
        {
            /*
            Serial.print("Received set_io_extender msg: ");
            Serial.println(buf[1]);
            Serial.print("Data:");
            for(uint8_t i=2; i< len;++i)
            {
                Serial.print(" ");
                Serial.print(buf[i]);
            }
            Serial.println();
            */

            /* ack it */
            airtalk->tx_ack();
        }
        else
        {
            Serial.print("Got IOEX message type: ");
            Serial.println(buf[0]);
        }
    }
    if (buf && dest == 0xff && buf[0] == 0x0a)
    {
        Serial.println("Airtalk received ACK");
    }
}

/* Make enigma set an ioex output when it gets a certain digital and analog input to see
 * if it reacts. Set an alarm on an analog input. */
