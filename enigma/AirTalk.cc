#include "AirTalk.h"

// bool const _verbose = true;
bool const _verbose = false;

AirTalk::AirTalk() : _in_msg(false), _in_buf(false), _out_msg(0), _backoff_delay(350)
{
    _airtalk = new AltSoftSerial();
    _airtalk->begin(19200);
    _watchdog = millis();
}

AirTalk::AckMessage::AckMessage() : type(0x0a), subtype(0x00) {}

void AirTalk::_backoff()
{
    /* Bit time at 19200 baud is 521us, so delay a semi-random sub-byte time. */
    delayMicroseconds(_backoff_delay);
    _backoff_delay += 290;
    if (_backoff_delay > 4000)
        _backoff_delay = 350;
}

void AirTalk::dispatch(ms_t now)
{
    if (now - _watchdog > 5000)
    {
        if (_verbose)
            Serial.println("Watchdog - resetting");

        _in_msg  = false;
        _in_buf  = false;
        _out_msg = 0;
        _airtalk->end();
        _airtalk->begin(19200);
        _watchdog = now;
    }

    bool const rx_success = _rx_message();
    if (rx_success)
    {
        _watchdog = now;
    }

    /* If we don't have anything to send, or we're in the middle of
     * receiving a message, we're done. */
    if (!_out_msg || _in_msg)
        return;

    if (rx_success || _do_backoff)
    {
        /* We just received a message, or we failed to send one. Delay a little. */
        _backoff();
        _do_backoff = false;
    }

    /* If there is no reception in progress, send. */
    if (_airtalk->available() || _airtalk->isReceiving())
    {
        if (_verbose)
        {
            if (_airtalk->available())
            {
                Serial.println("Send blocked, chars came in");
            }
            else
            {
                Serial.println("Send blocked, recv not idle");
            }
        }
        return;
    }

    if (_tx_message())
    {
        _watchdog = now;
    }
}

void AirTalk::tx_message(uint8_t dest, uint8_t len, uint8_t const * msg)
{
    _out_dest = dest;
    _out_len  = len;
    _out_msg  = msg;
}

void AirTalk::tx_ack()
{
    tx_message(0xff, sizeof(_ack), reinterpret_cast<uint8_t const *>(&_ack));
}

uint8_t const * AirTalk::get_message(uint8_t & dest, uint8_t & len)
{
    if (!_in_buf)
        return 0;

    dest    = _recv_dest;
    len     = _recv_len;
    _in_buf = false;
    return _recv_buf;
}

bool AirTalk::_tx_byte(uint8_t b)
{
    _airtalk->write(b);
    /* Wait for byte to go out and ensure we also received it ok. This
     * verifies the bus wasn't clobbered. */
    _airtalk->flushOutput();
    int const rb = _airtalk->read();
    if (rb < 0 && _verbose)
    {
        Serial.println("Could not read in tx_byte");
    }

    return b == rb;
}

void AirTalk::_find_stx()
{
    if (_verbose)
        Serial.println("Lost frame");
    while (_airtalk->available())
    {
        if (_airtalk->peek() == STX)
            break;
        _airtalk->read();
    }
}

bool AirTalk::_rx_message()
{
    if (!_in_msg)
    {
        /* We are not in the process of reading a message. */

        /* See if enough data that we plausibly can get message length is
         * available. If not, wait until next time (the AltSoftSerial
         * buffer is large enough to be able to completely store at least
         * one message.) */
        if (_airtalk->available() < 3)
            return false;

        uint8_t c;

        /* Make sure we are at STX. We should be, unless we've lost framing. */
        c = _airtalk->read();
        if (c != STX)
        {
            /* We're not at STX. Eat all mis-framed bytes we have and start over. */
            _find_stx();
            return false;
        }

        /* Read dest and len. We know they're available. */
        _recv_dest = _airtalk->read();
        _recv_len  = _airtalk->read();

        if (_recv_len > 79)
        {
            /* will overflow buffer, skip it */
            return false;
        }

        _in_msg = true;

        /* Return for now, next call will read the message. */
        return false;
    }

    /* Verify serial buffer isn't full. This should only happen if we mis-read a len. */
    if (_airtalk->overflow())
    {
        /* Abort */
        if (_verbose)
            Serial.println("Serial buffer overflow");

        _in_msg = false;
        return false;
    }

    /* We have read a header. See if the entire message has come
     * in. We need len bytes + checksum and ETX. */
    if (_airtalk->available() < _recv_len + 2)
    {
        return false;
    }

    /* Message is available. */
    uint8_t cksum = 0xa5;
    cksum ^= _recv_dest;
    cksum ^= _recv_len;

    /* Read the message bytes. */
    uint8_t i = 0;
    while (i < _recv_len)
    {
        uint8_t const c = _airtalk->read();
        if (i < buflen)
            _recv_buf[i] = c;
        cksum ^= c;
        ++i;
    }

    /* Read checksum. */
    uint8_t const cksum_in = _airtalk->read();
    uint8_t const etx      = _airtalk->read();

    _in_msg = false;

    if (cksum_in != cksum || etx != ETX)
    {
        /* Something went wrong, either message was corrupted or we lost framing. */
        if (_verbose)
            Serial.println("Recv cksum err");
        return false;
    }

    _in_buf = true;

    return true;
}

bool AirTalk::_tx_message()
{
    if (!_out_msg)
        return true;

    /* assume bus is clear. if any of the sends don't self-receive
     * correctly there's interference on the bus and success will
     * become false, causing an abort of the send due to
     * short-circuiting the and operators. */

    uint8_t cksum = 0xa5;

    bool success = _tx_byte(STX);

    success = success && _tx_byte(_out_dest);
    cksum ^= _out_dest;

    success = success && _tx_byte(_out_len);
    cksum ^= _out_len;

    for (uint8_t i = 0; i < _out_len; ++i)
    {
        uint8_t const c = _out_msg[i];
        success         = success && _tx_byte(c);
        cksum ^= c;
    }

    success = success && _tx_byte(cksum);

    success = success && _tx_byte(ETX);

    if (success)
    {
        /* Message transmission succeeded. Clear buffer pointer. If it
         * was not sent successfully, keep the buffer so we can try
         * again. */
        _out_msg = 0;
    }
    else
    {
        if (_verbose)
            Serial.println("Send fail");
        _do_backoff = true;
    }

    return success;
}

void AirTalk::dump_state()
{
    Serial.println("Airtalk state:");
    Serial.print("in_msg: ");
    Serial.println(_in_msg);
    Serial.print("in_buf: ");
    Serial.println(_in_buf);
    Serial.print("recv_dest: ");
    Serial.println(_recv_dest);
    Serial.print("recv_len: ");
    Serial.println(_recv_len);
    Serial.print("available: ");
    Serial.println(_airtalk->available());
    Serial.print("isReceiving: ");
    Serial.println(_airtalk->isReceiving());
}
