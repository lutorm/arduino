#include "Statemachine.h"
#include "objects.h"

namespace
{
    const prog_char s0[] PROGMEM = "Poweron";
    const prog_char s1[] PROGMEM = "Init";
    const prog_char s2[] PROGMEM = "Safe";
    const prog_char s3[] PROGMEM = "Idle";
    const prog_char s4[] PROGMEM = "Warmup";
    const prog_char s5[] PROGMEM = "Predry";
    const prog_char s6[] PROGMEM = "Dry";
    const prog_char s7[] PROGMEM = "Drydown";
    const prog_char s8[] PROGMEM = "Cooldown";

    const prog_char * names[StateMachine::n_states] = {s0, s1, s2, s3, s4, s5, s6, s7, s8};

    const prog_uint8_t state_address[] PROGMEM = "DRYBOXST";
    const prog_uint8_t state_cycle_address[] PROGMEM = "BOXSTCYC";
}  // namespace

StateMachine::StateMachine() : Periodic(1000) {}

const prog_char * StateMachine::current_state() const
{
    return names[_state];
}

void StateMachine::transition_to(State new_state)
{
    if (_new_state == _state)
    {
        /* No previous transition commanded. */
        _new_state = new_state;
    }
}


/* Enter a new state and do start tasks. */
void StateMachine::_start()
{
    _state_cycles = 0;
    
    switch (_new_state)
    {
    case Init:
        /* Init counters here so we don't safe immediately. */
        _railed_heater_ctr = settings.railed_heater_persistence;
        _bad_sensor_ctr = settings.bad_sensor_persistence;
        
    case Idle:
        /* Heater and cooler off. */
        boxtemp_setpoint = 0;
        boxtemp_pid->SetEngaged(false);
        heater_setpoint=0;
        heater_pid->SetEngaged(false);
        heater_pid->set_output(0);
        cooler_setpoint = 0.0f;
        //cooler_pid->SetEngaged(false);
        //cooler_pid->set_output(0);
        OCR1A = 0;
        fan2_pid->SetEngaged(false);
        fan2_pid->set_output(settings.dehum_fan_speed);

        OCR0A = settings.case_fan_speed_idle;
        OCR2B = settings.hotsink_fan_speed;

        _idle_dewpoint_ctr = settings.idle_dewpoint_persistence;

        break;

    case Warmup:
        /* Heater on, cooler off. Ensure setpoint is updated before controllers are enabled. */
        boxtemp_setpoint = settings.bake_temp;
        boxtemp_pid->SetEngaged(true, false);
        heater_pid->SetEngaged(true, false);
        cooler_setpoint = 0.0f;
        OCR1A = 0;
        //cooler_pid->SetEngaged(false);
        //cooler_pid->set_output(0);
        _last_dewpoint = humidity.dewpoint(0);

        OCR0A = 255;
        OCR2B = settings.hotsink_fan_speed;

        break;

    case Predry:
        _last_temp           = humidity.temperature(0);
        _last_coldsink       = onewire->temp(OnewireTemps::Probes::Coldsink);

        /* fall through to dry. */
        
    case Dry:
        /* cooler on. Heater settings are assumed to carry over. */
        //cooler_pid->SetEngaged(true, false);
        fan2_pid->SetEngaged(true, false);
        OCR1A = 0xffff;

        OCR2B = 255;

        _last_dewpoint = humidity.dewpoint(0);

        break;

    case Drydown:
        /* Heater off, cooler settings carry over. */
        boxtemp_setpoint = 0;
        boxtemp_pid->SetEngaged(false);
        heater_setpoint=0;
        heater_pid->SetEngaged(false);
        heater_pid->set_output(0);
        //cooler_pid->SetEngaged(true, false);
        
        OCR0A = settings.case_fan_speed_cooldown;

        break;

    case Cooldown:

        /* Cooler off. Heater off */
        boxtemp_setpoint = 0;
        boxtemp_pid->SetEngaged(false);
        heater_setpoint=0;
        heater_pid->SetEngaged(false);
        heater_pid->set_output(0);
        cooler_setpoint = 0.0f;
        //cooler_pid->SetEngaged(false);
        //cooler_pid->set_output(0);
        OCR1A = 0;
        fan2_pid->SetEngaged(false);
        fan2_pid->set_output(settings.dehum_fan_speed);

        OCR0A = settings.case_fan_speed_cooldown;
        OCR2B = settings.hotsink_fan_speed;

        break;

    case Safe:
        /* All fans max */
        OCR0A = 255;
        OCR0B = 255;
        OCR2B = 255;

        /* Disable heaters. */
        boxtemp_setpoint = 0;
        boxtemp_pid->SetEngaged(false);
        heater_setpoint=0;
        heater_pid->SetEngaged(false);
        heater_pid->set_output(0);
        cooler_setpoint = 0.0f;
        //cooler_pid->SetEngaged(false);
        //cooler_pid->set_output(0);
        OCR1A = 0;
        fan2_pid->SetEngaged(false);

        break;
    }

    _state = _new_state;
}

void StateMachine::_dispatch(ms_t const now)
{
    /* See if we have a transition requested. */
    if (_state != _new_state)
    {
        _start();
    }

    if (_state != Safe)
    {
        _check_safe();
    }
    
    switch (_state)
    {
    case Init:
        transition_to(Idle);
        break;
        
    case Idle:
        /* If the last warmup resulted in a dry cycle, we assume we
         * still have moisture to pull out. */
        if (_state_cycles > settings.bake_interval)
        {
            transition_to(Warmup);
        }

        /* If dewpoint is above threshold for sufficiently long, trigger a cycle. */
        if (humidity.dewpoint(0) > settings.idle_dewpoint_threshold)
        {
            --_idle_dewpoint_ctr;
        }
        else
        {
            _idle_dewpoint_ctr = settings.idle_dewpoint_persistence;
        }
        
        if (_idle_dewpoint_ctr == 0)
        {
            transition_to(Warmup);
        }

        break;

    case Warmup:
        boxtemp_setpoint = settings.bake_temp;

        /* If dewpoint rises above threshold, start a drying cycle. */
        if (humidity.dewpoint(0) > settings.warmup_dewpoint_threshold)
        {
            transition_to(Predry);
        }
        
        /* Every 10 minutes after an initial 30min check if dewpoint is still rising. */
        if (_state_cycles % (10*60) == 0 && _state_cycles > 30*60)
        {
            float const delta_dewpoint = humidity.dewpoint(0) - _last_dewpoint;
            if (delta_dewpoint < 0.25f)
            {
                /* Dewpoint is no longer rising. If we haven't
                 * triggered a bake, give up. */
                transition_to(Cooldown);
            }
            _last_dewpoint = humidity.dewpoint(0);
        }

        /* If we time out without having started a bake, transition to
         * cooldown. */
        if (_state_cycles > settings.max_baking_cycles)
        {
            transition_to(Cooldown);
        }

        break;

    case Predry:
        boxtemp_setpoint = settings.bake_temp;

        cooler_setpoint = humidity.dewpoint(0) - settings.dewpoint_spread;
        
        /* Once coldsink is cooling down at less than -1C/min, and
         * temperature is not going up, transition to Dry. */
        if (_state_cycles % (5*60) == 0 && _state_cycles > 0)
        {
            float const delta_coldsink =
                onewire->temp(OnewireTemps::Probes::Coldsink) - _last_coldsink;
            float const delta_temp = humidity.temperature(0) - _last_temp;
            if ((delta_coldsink > -1.0f) && (delta_temp < 0.1f))
            {
                transition_to(Dry);
            }
            _last_coldsink = onewire->temp(OnewireTemps::Probes::Coldsink);
            _last_temp = humidity.temperature(0);
        }

        break;
        
    case Dry:
        boxtemp_setpoint = settings.bake_temp;

        cooler_setpoint = humidity.dewpoint(0) - settings.dewpoint_spread;

        if (_state_cycles > settings.max_baking_cycles)
        {
            transition_to(Drydown);
        }

        /* After min time has passes, check every hour whether the
         * dewpoint is going down. We need to have a long interval on
         * this because it is changing slowly and may even increase
         * initially as moisture is baked out. */
        if (_state_cycles % (60*60) == 0 && _state_cycles > settings.min_baking_cycles)
        {
            float const delta_dewpoint = humidity.dewpoint(0) - _last_dewpoint;
            if (delta_dewpoint > -0.5f)
            {
                transition_to(Drydown);
            }
            _last_dewpoint = humidity.dewpoint(0);
        }

        break;

    case Drydown:
        cooler_setpoint = humidity.dewpoint(0) - settings.dewpoint_spread;
        if (cooler_setpoint < settings.min_cooler_setpoint)
        {
            cooler_setpoint = settings.min_cooler_setpoint;
        }
        
        /* Stop drying once either of these conditions happen: we
         * can't keep the cold sink sufficiently below the dew point,
         * or the post-cooler dew point isn't sufficiently below the
         * pre-cool one. Either of these mean we're no longer drying
         * efficiently. Give up and just cool down passively.  */
        if ((humidity.dewpoint(0) - onewire->temp(OnewireTemps::Probes::Coldsink)
            < 0.5 * settings.dewpoint_spread ) ||
            (humidity.dewpoint(0) - humidity.dewpoint(1) < 1.0f))
        {
            transition_to(Cooldown);
        }

        /* fall through to cooldown. */
        
    case Cooldown:
        /* Once we are close enough to ambient temp, go back to Idle. */
        if (humidity.temperature(0) - onewire->temp(OnewireTemps::Probes::Ambient) < 10.0f)
        {
            transition_to(Idle);
        }

        break;

    case Safe:
        break;
    };

    ++_state_cycles;
}

void StateMachine::_check_safe()
{
    if ((*thermistor.temperature() < 10) || (*thermistor.temperature() > 120)
        || (isnan(onewire->temp(OnewireTemps::Probes::Coldsink)))
        || (onewire->temp(OnewireTemps::Probes::Hotsink) > 40
            && OCR1A /*cooler_pid->output()*/ > 0)
        || (isnan(onewire->temp(OnewireTemps::Probes::Hotsink))))
    {
        --_bad_sensor_ctr;
    }
    else
    {
        _bad_sensor_ctr == settings.bad_sensor_persistence;
    }

    if (heater_pid->output() > 0.95)
    {
        --_railed_heater_ctr;
    }
    else
    {
        _railed_heater_ctr = settings.railed_heater_persistence;
    }

    if (_bad_sensor_ctr == 0 || _railed_heater_ctr == 0)
    {
        sprint_pgm(PSTR("Safing "));
        Serial.println(_bad_sensor_ctr);
        Serial.println(_railed_heater_ctr);
        transition_to(Safe);
    }
}

void StateMachine::send_data(uint16_t & crc)
{
    ::send_data_pgmaddr(state_address, _state, crc);
    ::send_data_pgmaddr(state_cycle_address, _state_cycles, crc);
}
