#pragma once

#include "Display.h"
#include "objects.h"
#include "version.h"

const prog_char degCn[] PROGMEM    = " C\n";
const prog_char percentn[] PROGMEM = " %\n";

Display::Display() : Periodic(500), _disp(_rst_pin, _dc_pin, _cs_pin)
{
    _disp.begin();
    _disp.clear(ALL);
    _disp.clear(PAGE);
    _disp.setCursor(0, 0);
    sprint_pgm(_disp, PSTR("Drybox\n\rbooting\n\r"));
    sprint_pgm(_disp, version);

    _disp.display();
}

void Display::_dispatch(ms_t)
{
    _disp.clear(PAGE);

    if (_counter-- == 0)
    {
        _screen++;
        if (_screen == 5)
            _screen = 0;

        _counter = 20;
    }

    _disp.setCursor(0, 0);
    _disp.setFontType(0);

    if (_counter < 10)
    {
        _screen_main();
    }
    else
    {
        switch (_screen)
        {
        case 0:
            _screen0();
            break;
        case 1:
            _screen1();
            break;
        case 2:
            _screen2();
            break;
        case 3:
            _screen3();
            break;
        case 4:
            _screen4();
            break;
        default:
            break;
        }
    }

    _disp.display();
}

/* Main screen. */
void Display::_screen_main()
{
    sprint_pgm(_disp, PSTR("Drybox:\n"));
    _disp.print(boxtemp_input);
    sprint_pgm(_disp, degCn);
    _disp.print(humidity.humidity(0));
    sprint_pgm(_disp, PSTR(" %\nDewpoint:\n"));
    _disp.print(round(humidity.dewpoint(0)*100)/100.);
    sprint_pgm(_disp, degCn);
    sprint_pgm(_disp, state_machine.current_state());
}

/* Screen 0: Temperature and humidity. */
void Display::_screen0()
{
    sprint_pgm(_disp, PSTR("Inside\n"));
    _disp.print(boxtemp_input);
    sprint_pgm(_disp, degCn);
    _disp.print(boxtemp_pid->GetSetpoint());
    sprint_pgm(_disp, degCn);
    _disp.print(humidity.humidity(0));
    sprint_pgm(_disp, PSTR(" %\nDewpoint:\n"));
    _disp.print(int(humidity.dewpoint(0)));
    sprint_pgm(_disp, degCn);
}

/* Screen 1: Heater temp and PWM. */
void Display::_screen1()
{
    sprint_pgm(_disp, PSTR("Heater:\n"));
    _disp.print(heater_pid->output() * 100);
    sprint_pgm(_disp, percentn);
    _disp.print(*thermistor.temperature());
    sprint_pgm(_disp, PSTR(" C\nSetpoint:\n"));
    _disp.print(heater_pid->GetSetpoint());
    sprint_pgm(_disp, degCn);
}

/* Screen 2: TEC temp and PWM. */
void Display::_screen2()
{
    sprint_pgm(_disp, PSTR("Cooler:\n"));
    _disp.print(OCR1A/65535.f/*cooler_pid->output() * 100*/);
    sprint_pgm(_disp, PSTR(" %\nCold sink:"));
    _disp.print(onewire->temp(OnewireTemps::Probes::Coldsink));
    sprint_pgm(_disp, PSTR(" C\nPost-cool\n"));
    _disp.print(humidity.humidity(1));
    sprint_pgm(_disp, percentn);
}

/* Screen 3: Fan RPMS. */
void Display::_screen3()
{
    sprint_pgm(_disp, PSTR("Circ fan:\n"));
    _disp.print(int(fan_tach.rpm(1)));
    sprint_pgm(_disp, PSTR(" rpm\nCold fan:\n"));
    _disp.print(int(fan_tach.rpm(2)));
    sprint_pgm(_disp, PSTR(" rpm\nHot fan:\n"));
    _disp.print(int(fan_tach.rpm(0)));
    sprint_pgm(_disp, PSTR(" rpm"));
}

/* Screen 4: 1-wire probes. */
void Display::_screen4()
{
    sprint_pgm(_disp, PSTR("Interior\nL "));
    _disp.print(onewire->temp(OnewireTemps::Probes::Left));
    sprint_pgm(_disp, PSTR(" C\nR "));
    _disp.print(onewire->temp(OnewireTemps::Probes::Right));
    sprint_pgm(_disp, PSTR(" C\nT "));
    _disp.print(onewire->temp(OnewireTemps::Probes::Top));
    sprint_pgm(_disp, PSTR(" C\nI "));
    _disp.print(humidity.temperature(0));
    sprint_pgm(_disp, PSTR(" C\nPC "));
    _disp.print(humidity.temperature(1));
    sprint_pgm(_disp, degCn);
}
