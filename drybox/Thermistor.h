#pragma once

#include "EventLoop.h"
#include <avr/pgmspace.h>
#include <math.h>

/* Reads bed heater thermistor hooked up to ADC7. */
class ThermistorReader : public Periodic
{
public:
    ThermistorReader(ms_t period);

    float const * temperature() { return &_temp; }

    void send_data(uint16_t & crc);

private:
    void _dispatch(ms_t) override final;

    static const uint8_t pin = 7;

    // Parameters from the TAZ6 Marlin bed heater configuration.
    static constexpr float B    = 3974;  // K
    static constexpr float R0   = 100e3;
    static constexpr float T0   = 298.15;  // +25C
    static constexpr float Rref = 4.7e3;

    /* Rinfinity parameter. */
    static constexpr float Rinf = R0 * expf(-B / T0);

    /* temperature we read. */
    float _temp = NAN;

    /* 64-bit addresses to send temperature with. */
    static const prog_uint8_t address[];
    static const prog_uint8_t address_resistance[];
};
