#include "OnewireTemps.h"
#include "serial_data.h"

namespace
{
    const prog_uint8_t a1[] PROGMEM = {0x28, 0xAA, 0x5A, 0x8F, 0x4A, 0x14, 0x01, 0x34};
    const prog_uint8_t a2[] PROGMEM = {0x28, 0xDC, 0x17, 0x72, 0x02, 0x00, 0x00, 0x6A};
    const prog_uint8_t a3[] PROGMEM = {0x28, 0xAA, 0x72, 0x08, 0x4C, 0x14, 0x01, 0x7C};
    const prog_uint8_t a4[] PROGMEM = {0x28, 0xAA, 0x8E, 0xA6, 0x4B, 0x14, 0x01, 0x3D};
    const prog_uint8_t a5[] PROGMEM = {0x28, 0xB9, 0xF8, 0x71, 0x02, 0x00, 0x00, 0x7F};
    const prog_uint8_t a6[] PROGMEM = {0x28, 0x32, 0x7A, 0xF1, 0x0C, 0x00, 0x00, 0x08};

    const prog_char n1[] PROGMEM = "Interior right";
    const prog_char n2[] PROGMEM = "Interior left";
    const prog_char n3[] PROGMEM = "Internal fan intake";
    const prog_char n4[] PROGMEM = "Hot sink";
    const prog_char n5[] PROGMEM = "Cold sink";
    const prog_char n6[] PROGMEM = "Ambient";

    const prog_uint8_t * addresses[OnewireTemps::n_devs] = {a1, a2, a3, a4, a5, a6};
    const prog_char *    names[OnewireTemps::n_devs]     = {n1, n2, n3, n4, n5, n6};

}  // namespace

OnewireTemps::OnewireTemps(bool verbose) :
    Periodic(1000), _bus(onewirepin, addresses, names, n_devs, 1, verbose)
//_bus(onewirepin, verbose)
{
    for (int i = 0; i < n_devs; ++i)
    {
        _temps[i] = NAN;
        _bus.set_output(i, &_temps[i]);
    }

    /* @todo There is a bug in any_parasite.
    if (_bus.any_parasite())
    {
        sprint_pgm(PSTR("OnewireTemps can't handle parasite-powered temperature
    probes\n"));
    }
    */
}

void OnewireTemps::_dispatch(ms_t now)
{
    /* Because we know that it'll take 760ms to convert temp, we can
     * safely read a measurement and then start a new conversion at
     * our period. */
    _bus.readout();
    _bus.start_conversion();
}

void OnewireTemps::send_data(uint16_t & crc)
{
    for (int i = 0; i < n_devs; ++i)
    {
        ::send_data_pgmaddr(addresses[i], _temps[i], crc);
    }
}

const prog_char * name(uint8_t i)
{
    return names[i];
}
