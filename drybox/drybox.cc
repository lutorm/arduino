#include "Display.h"
#include "EventLoop.h"
#include "dewpoint.h"
#include "objects.h"
#include "serial_data.h"
#include "version.h"
#include <Arduino.h>
#include <util/delay.h>

/* Timer2 runs with prescaler 1 so a period of 256 ticks. */
Timekeeper time(256);

float constexpr timer1_tovf = 65535.*64/F_CPU;
Fantach fan_tach(timer1_tovf);

HumidityReader humidity(1000);

ThermistorReader thermistor(1000);

OnewireTemps * onewire;

/* Controller input/outputs */
float boxtemp_setpoint = 0;
float boxtemp_input    = 0;
float heater_setpoint  = boxtemp_setpoint;  // to avoid pid bump at start
float cooler_setpoint  = 0;
float dewpoint_spread  = 5;
float fan1_setpoint    = 7000;
float fan2_setpoint    = 1000;
float fan3_setpoint    = 1000;

Settings settings;

class BoxtempPid : public PIDEvent
{
public:
    BoxtempPid(const float * input,
               float *       output,
               const float * setpoint,
               bool          positiveDirection) :
        PIDEvent(input, output, setpoint, positiveDirection) {
    }

    float feedForward() const override {
        auto const ambient = onewire->temp(OnewireTemps::Probes::Ambient);
        auto const ff =
            ambient + (boxtemp_setpoint - ambient) * settings.boxtemp_feedforward_gain;
        return ff;
    }
};

prog_uint8_t const boxtemp_pid_name[] = "HETRSETP";
PIDEvent *         boxtemp_pid;
prog_uint8_t const heater_pid_name[] = "HEATRPWM";
PwmPIDBase *       heater_pid;
prog_uint8_t const cooler_setpoint_name[] = "COOLSETP";
prog_uint8_t const cooler_pwm_name[]      = "COOLRPWM";
//PwmPIDBase *       cooler_pid;

//prog_uint8_t const fan1_pid_name[] = "FANPWM_1";
//PwmPIDBase *       fan1_pid;
prog_uint8_t const fan2_pid_name[] = "FANPWM_2";
PwmPIDBase *       fan2_pid;
//prog_uint8_t const fan3_pid_name[] = "FANPWM_3";
//PwmPIDBase *       fan3_pid;


Display * display;

/* This is defined inline here so it sees all the global objects and
 * can ask them to send. */
class SerialSender : public Periodic
{
public:
    SerialSender(ms_t period);

private:
    void _dispatch(ms_t) override final;
};

SerialSender::SerialSender(ms_t period) : Periodic(period) {}

void SerialSender::_dispatch(ms_t)
{
    uint16_t   crc  = 0x0000;
    const ms_t tick = time.millis();
    send_header(14 + Fantach::n_fans + OnewireTemps::n_devs, tick);
    
    fan_tach.send_data(crc);
    onewire->send_data(crc);
    thermistor.send_data(crc); // 1
    humidity.send_data(crc); // 6
    heater_pid->send_data(crc); // 1
    //cooler_pid->send_data(crc); // 1
    //fan1_pid->send_data(crc); // 1
    fan2_pid->send_data(crc); // 1
    //fan3_pid->send_data(crc); // 1
    state_machine.send_data(crc); // 2
    
    ::send_data_pgmaddr(boxtemp_pid_name, heater_setpoint, crc);
    ::send_data_pgmaddr(cooler_setpoint_name, cooler_setpoint, crc);
    ::send_data_pgmaddr(cooler_pwm_name, OCR1A/65535.f, crc);

    // we dont' even wait for a response, that's useless.
    bool ack = wait_for_response(crc, 0);
}

SerialSender sender(10000);

WatchdogPetter wd(2000);

StateMachine state_machine;

void setup_timers()
{
    cli();

    /* Timer 0 runs fast pwm, inverting mode, on both OC0A and OC0B
     * with prescaler 1 for Fan 1/2. (We keep the overflow interrupt
     * enabled so millis doesn't stall. Instead, it will run about 30x
     * too fast.) */
    OCR0A = 0x00;
    OCR0B = 0x00;
    TCCR0A =
        _BV(COM0A0) | _BV(COM0A1) | _BV(COM0B0) | _BV(COM0B1) | _BV(WGM00) | _BV(WGM01);
    TCCR0B = _BV(CS00);
    TIFR0  = 0xff;
    TIMSK0 = 0;  //_BV(TOIE0);
    /* OC0A/B is PD5/6. Set as outputs. */
    DDRD |= 0x60;

    /* Timer 1 runs 16-bit phase-correct pwm, inverting mode, on both
     * OC1A and OC1B with prescaler 64 (0.95Hz). Clear interrupt flags
     * and enable timer overflow interrupt. Set OC1A/B to outputs. */
    OCR1A  = 0x0000;
    OCR1B  = 0x0000;
    ICR1   = 0xffff;
    TCCR1A = _BV(COM1A0) | _BV(COM1A1) | _BV(COM1B0) | _BV(COM1B1) | _BV(WGM11);
    TCCR1B = _BV(WGM13) | _BV(CS11) | _BV(CS10);
    TIFR1  = 0xff;
    TIMSK1 = _BV(TOIE1);
    /* OC1A/B is PB1/2. Set as outputs. */
    DDRB |= 0x06;

    /* Timer 2 runs fast PWM, inverting mode, on OC2B with prescaler 1
     * for Fan3. OC2A is disconnected. Enable timer overflow interrupt
     * (for the time keeper). Ensure timer2 is connected to the IO
     * clock. */
    ASSR &= ~_BV(AS2);
    OCR2A  = 0x00;
    TCCR2A = _BV(COM2B0) | _BV(COM2B1) | _BV(WGM20) | _BV(WGM21);
    TCCR2B = _BV(CS20);
    TIFR2  = 0xff;
    TIMSK2 = _BV(TOIE2);
    /* OC2B is PD3. Set as output. */
    DDRD |= 0x08;

    // Make sure all timers are enabled in the power reduction register
    PRR &= ~(_BV(PRTIM0) | _BV(PRTIM1) | _BV(PRTIM2));

    /* Set up watchdog timer for 4s. */
    wdt_reset();
    WDTCSR |= _BV(WDE) | _BV(WDCE);
    WDTCSR = _BV(WDIF) | _BV(WDIE) | _BV(WDE) | _BV(WDP3);

    sei();
}

void setup()
{
    uint8_t mcusr = MCUSR;
    MCUSR         = 0;

    Serial.begin(19200);
    if (mcusr & _BV(WDRF))
    {
        /* This doesn't work -- MCUSR is zero after a watchdog reset? */
        sprint_pgm(PSTR("System reset due to watchdog timer. Safing!\r\n"));
        state_machine.transition_to(StateMachine::Safe);
    }

    sprint_pgm(PSTR("Drybox controller version "));
    sprint_pgm(version);
    sprint_pgm(PSTR("\r"));
    
    /* Creating the display also displays the booting screen. */
    display = new Display();

    setup_timers();

    /* Attempt to read settings from EEPROM. */
    {
        Settings eeprom_settings;
        bool const success = eeprom_settings.read_from_eeprom();
        if (success)
        {
            sprint_pgm(PSTR("Settings read from EEPROM\r"));
            settings = eeprom_settings;
        }
        else
        {
            sprint_pgm(PSTR("Failed reading settings from EEPROM. Restoring defaults\r"));
            settings.save_to_eeprom();
        }
    }
        
    onewire = new OnewireTemps(false);

    /* Outer PID controller that controls the heater setpoint to
     * achieve a certain box internal temperature. The setpoint is
     * also used as a feedforward since we know the heater must
     * basically have that temperature. */
    boxtemp_pid =
        new BoxtempPid(&boxtemp_input, &heater_setpoint, &boxtemp_setpoint, true);
    boxtemp_pid->set_period(6000);  // 3x inner controller
    boxtemp_pid->SetTunings(settings.boxtemp_pid_pgain,
                            settings.boxtemp_pid_igain,
                            settings.boxtemp_pid_dgain);
    boxtemp_pid->SetOutputLimits(0.0, settings.boxtemp_pid_maxout);  // if zero is not
                                                                     // included, pid init
                                                                     // does funny things.
    boxtemp_pid->SetEngaged(false);

    heater_pid = new PwmPID<decltype(OCR1B)>(OCR1B,
                                             thermistor.temperature(),
                                             &heater_setpoint,
                                             heater_pid_name);
    heater_pid->set_period(2000);
    heater_pid->SetTunings(settings.heater_pid_pgain,
                           settings.heater_pid_igain,
                           settings.heater_pid_dgain);
    heater_pid->SetEngaged(false);

    /*
    cooler_pid =
        new PwmPID<decltype(OCR1A)>(OCR1A,
                                    &onewire->temp(OnewireTemps::Probes::Coldsink),
                                    &cooler_setpoint,
                                    cooler_pid_name);
    cooler_pid->set_period(2000);
    cooler_pid->SetDirection(false);
    cooler_pid->SetTunings(settings.cooler_pid_pgain,
                           settings.cooler_pid_igain,
                           settings.cooler_pid_dgain);
    cooler_pid->SetEngaged(false);
    */

    /* Fan 1 rpm is rpm 1. */
    /*
    fan1_pid = new PwmPID<decltype(OCR0A)>(OCR0A,
                                           &fan_tach.rpm(1),
                                           &fan1_setpoint,
                                           fan1_pid_name);
    fan1_pid->set_period(1000);
    fan1_pid->SetTunings(1e-4, 5e-5, 0);
    fan1_pid->SetOutputLimits(0.1, 1.0);
    fan1_pid->SetEngaged(true);
    */
    
    /* Fan 2 is controlled to maintain the cooler setpoint. Cooler is always on full. */
    fan2_pid = new PwmPID<decltype(OCR0B)>(OCR0B,
                                           &onewire->temp(OnewireTemps::Probes::Coldsink),
                                           &cooler_setpoint,
                                           fan2_pid_name);
    fan2_pid->set_period(6000);
    fan2_pid->SetTunings(settings.fan2_pid_pgain,
                         settings.fan2_pid_igain,
                         settings.fan2_pid_dgain);
    fan2_pid->SetOutputLimits(settings.fan2_min_pwm, settings.fan2_max_pwm);
    fan2_pid->SetEngaged(false);

    /* Fan 3 rpm is rpm 0. */
    /*
    fan3_pid = new PwmPID<decltype(OCR2B)>(OCR2B,
                                           &fan_tach.rpm(0),
                                           &fan3_setpoint,
                                           fan3_pid_name);
    fan3_pid->set_period(1000);
    fan3_pid->SetTunings(1e-4,5e-5,0);
    fan3_pid->SetOutputLimits(0.1, 1.0);
    fan3_pid->SetEngaged(true);
    */
    
    event_loop.add_event(onewire);
    event_loop.add_event(&humidity);
    event_loop.add_event(&thermistor);
    event_loop.add_event(&fan_tach);
    event_loop.add_event(boxtemp_pid);
    event_loop.add_event(heater_pid);
    //event_loop.add_event(cooler_pid);
    //event_loop.add_event(fan1_pid);
    event_loop.add_event(fan2_pid);
    //event_loop.add_event(fan3_pid);
    event_loop.add_event(&sender);
    event_loop.add_event(&wd);
    event_loop.add_event(display);
    event_loop.add_event(&state_machine);

    sprint_pgm(PSTR("Boot successful\r"));
    Serial.print(get_free_memory());
    sprint_pgm(PSTR(" bytes free\r"));
}

void loop()
{
    /*
    ms_t now = time.millis();
    ms_t mi = millis();
    Serial.print(now);
    Serial.print(" "); Serial.println(mi);
    */
    if (humidity.temperature(0) - onewire->temp(OnewireTemps::Probes::Top) < -8)
    {
        // Humidity sensor does not seem to sense the max temp, switch
        // control to 1-wire probe
        boxtemp_input = onewire->temp(OnewireTemps::Probes::Top);
    }
    else
    {
        boxtemp_input = humidity.temperature(0);
    }

    event_loop.dispatch(time.millis());

    char tok;
    if (Serial.available() > 0)
    {
        tok = Serial.read();

        switch (tok)
        {
        case 'w':
            state_machine.transition_to(StateMachine::Warmup);
            break;
/*        case 'd':
            state_machine.transition_to(StateMachine::Dry);
            break;
*/        case 'c':
            state_machine.transition_to(StateMachine::Drydown);
            break;
        case '!':
            state_machine.transition_to(StateMachine::Safe);
            break;
        case 'F':
            settings.fan2_min_pwm = settings.fan2_min_pwm + 0.01;
            fan2_pid->SetOutputLimits(settings.fan2_min_pwm, settings.fan2_max_pwm);
            break;
        case 'f':
            settings.fan2_min_pwm = settings.fan2_min_pwm - 0.01;
            fan2_pid->SetOutputLimits(settings.fan2_min_pwm, settings.fan2_max_pwm);
            break;
        case 'T':
            settings.bake_temp = settings.bake_temp + 5.0f;
            Serial.println(settings.bake_temp);
            break;
        case 't':
            settings.bake_temp = settings.bake_temp - 5.0f;
            Serial.println(settings.bake_temp);
            break;
        case 'S':
            settings.dewpoint_spread = settings.dewpoint_spread + 1.0f;
            break;
        case 's':
            settings.dewpoint_spread = settings.dewpoint_spread - 1.0f;
            break;
/*        case 'P':
            settings.fan2_pid_pgain = settings.fan2_pid_pgain + 0.01;
            Serial.println(settings.fan2_pid_pgain);
            break;
        case 'p':
            settings.fan2_pid_pgain = settings.fan2_pid_pgain - 0.01;
            Serial.println(settings.fan2_pid_pgain);
            break;
        case 'I':
            settings.fan2_pid_igain = settings.fan2_pid_igain + 1e-4;
            Serial.println(settings.fan2_pid_igain);
            break;
        case 'i':
            settings.fan2_pid_igain = settings.fan2_pid_igain - 1e-4;
            Serial.println(settings.fan2_pid_igain);
            break;*/
        case 'E':
            settings.save_to_eeprom();
            break;
        case '0':
            /* Restore defaults. */
            settings = Settings();
            break;
        }
        fan2_pid->SetTunings(settings.fan2_pid_pgain,
                             settings.fan2_pid_igain,
                             settings.fan2_pid_dgain);
        
    }
}
