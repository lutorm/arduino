#pragma once

#include "Fantach.h"
#include "Humidity.h"
#include "OnewireTemps.h"
#include "PID.h"
#include "PwmPID.h"
#include "Thermistor.h"
#include "Timekeeper.h"
#include "WatchdogPetter.h"
#include "Statemachine.h"
#include "Settings.h"

extern Timekeeper       time;
extern Fantach          fan_tach;
extern HumidityReader   humidity;
extern ThermistorReader thermistor;
extern OnewireTemps *   onewire;
extern PIDEvent *       boxtemp_pid;
extern PwmPIDBase *     heater_pid;
//extern PwmPIDBase *     cooler_pid;
extern PwmPIDBase *     fan1_pid;
extern PwmPIDBase *     fan2_pid;
extern PwmPIDBase *     fan3_pid;
extern WatchdogPetter   wd;
extern StateMachine     state_machine;
extern Settings         settings;
extern float            boxtemp_setpoint;
extern float            heater_setpoint;
extern float            boxtemp_input;
extern float            cooler_setpoint;
