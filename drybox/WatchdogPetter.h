#pragma once

#include "EventLoop.h"
#include <avr/wdt.h>

class WatchdogPetter : public Periodic
{
public:
    WatchdogPetter(ms_t period) : Periodic(period) {}

    void _dispatch(ms_t now) override final { wdt_reset(); }
};
