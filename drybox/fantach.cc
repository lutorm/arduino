#include "Fantach.h"
#include "serial_data.h"
#include <avr/interrupt.h>

const prog_uint8_t Fantach::_address[] PROGMEM = "FANRPM_0";

namespace
{
    volatile float    _t_ovf = 0;
    volatile uint16_t _counter[Fantach::n_fans];
    volatile uint16_t _last_counter[Fantach::n_fans];
    volatile uint8_t  _old_state = 0;
}  // namespace

/* Pin change interrupt. If the pins we monitor have changed state,
 * increment their counters. */
ISR(PCINT1_vect)
{
    uint8_t const state = PINC;

    for (uint8_t pin = 1; pin <= 3; ++pin)
    {
        if ((state & (1 << pin)) && !(_old_state & (1 << pin)))
        {
            // rising edge on this pin, increment counter
            ++_counter[pin - 1];
        }
    }

    _old_state = state;
}

/* Timer 1 overflow -- copy final counter value. */
ISR(TIMER1_OVF_vect)
{
    for (uint8_t pin = 0; pin < Fantach::n_fans; ++pin)
    {
        _last_counter[pin] = _counter[pin];
        _counter[pin]      = 0;
    }
}

Fantach::Fantach(float t_ovf) : Periodic(100)
{
    _t_ovf = t_ovf;

    for (uint8_t pin = 0; pin < 3; ++pin)
    {
        _rpm[pin]     = 0;
        _counter[pin] = 0;
    }

    /* Set PC123 to inputs, no pull-up. */
    DDRC &= ~0x0e;
    PORTC &= ~0x0e;

    /* Set pin change interrupt mask for for PC123. */
    PCMSK1 = 0x0e;

    /* Enable pin change interrupt 1. */
    PCICR |= _BV(PCIE1);
}

/* Calculate RPM over the last period. Because the interrupt might change
 * the last_counter at any time, we have to protect the reading. */
void Fantach::_dispatch(ms_t)
{
    for (uint8_t pin = 0; pin < 3; ++pin)
    {
        cli();
        auto const cnt = _last_counter[pin];
        sei();
        _rpm[pin] = 60.0f / _pulses_per_rev * cnt / _t_ovf;
    }
}

void Fantach::send_data(uint16_t & crc)
{
    for (uint8_t pin = 0; pin < 3; ++pin)
    {
        uint8_t addr[8];
        memcpy_P(addr, _address, 8);

        addr[7] = ('0' + pin);
        ::send_data(addr, rpm(pin), crc);
    }
}
