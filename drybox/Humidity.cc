#include "Humidity.h"
#include "Wire.h"
#include "dewpoint.h"
#include "memory.h"
#include "serial_data.h"

const prog_uint8_t HumidityReader::humaddress[] PROGMEM   = "BOXHUM_0";
const prog_uint8_t HumidityReader::tempaddress[] PROGMEM  = "BOXTEMP0";
const prog_uint8_t HumidityReader::dewptaddress[] PROGMEM = "BOXDEWP0";

HumidityReader::HumidityReader(ms_t period) : Periodic(period)
{
    Wire.begin();
    sensor.begin(Wire);

    _hum[0]   = NAN;
    _hum[1]   = NAN;
    _temp[0]  = NAN;
    _temp[1]  = NAN;
    _dewpt[0] = NAN;
    _dewpt[1] = NAN;
}

void HumidityReader::_dispatch(ms_t now)
{
    _switch_to_bus(0);
    _hum[0]   = sensor.getRH();
    _temp[0]  = sensor.getTemp();
    _dewpt[0] = ::dewpoint(_temp[0], _hum[0]);

    _switch_to_bus(1);
    _hum[1]   = sensor.getRH();
    _temp[1]  = sensor.getTemp();
    _dewpt[1] = ::dewpoint(_temp[1], _hum[1]);
}

void HumidityReader::_switch_to_bus(uint8_t i)
{
    if (i > 1)
    {
        return;
    }

    Wire.beginTransmission(0x70);
    Wire.write(0x04 | i);
    uint8_t res = Wire.endTransmission();

    if (res == 0)
    {
        /* Slave acked. Verify setting. */
        Wire.requestFrom(0x70, 1);
        if (Wire.available() > 0)
        {
            uint8_t reg = Wire.read();
            if ((reg & 0x07) == (0x04 + i))
            {
                return;
            }
        }
    }

    sprint_pgm(PSTR("Humidity: Could not set I2C switch\r\n"));
}

void HumidityReader::send_data(uint16_t & crc) const
{
    for (uint8_t i = 0; i < 2; ++i)
    {
        uint8_t addr[8];

        memcpy_P(addr, humaddress, 8);
        addr[7] = ('1' + i);
        ::send_data(addr, _hum[i], crc);

        memcpy_P(addr, tempaddress, 8);
        addr[7] = ('1' + i);
        ::send_data(addr, _temp[i], crc);

        memcpy_P(addr, dewptaddress, 8);
        addr[7] = ('1' + i);
        ::send_data(addr, _dewpt[i], crc);
    }
}
