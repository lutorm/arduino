#include "dewpoint.h"
#include <math.h>

/* Magnus formula for calculating dewpoint, from
 * https://en.wikipedia.org/wiki/Dew_point */

static float constexpr b = 18.678;
static float constexpr c = 257.14;
static float constexpr d = 234.5;

float gamma(float T, float RH)
{
    return log(RH / 100. * exp((b - T / d) * (T / (c + T))));
}

float dewpoint(float T, float RH)
{
    return c * gamma(T, RH) / (b - gamma(T, RH));
}
