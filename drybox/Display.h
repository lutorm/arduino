#pragma once

#include "EventLoop.h"
#include "SFE_MicroOLED.h"

class Display : public Periodic
{
public:
    Display();

    void _dispatch(ms_t) override;

private:
    void _screen_main();
    void _screen0();
    void _screen1();
    void _screen2();
    void _screen3();
    void _screen4();

    MicroOLED_SPI _disp;

    /* The screen currently being displayed. */
    uint8_t _screen = 0;

    /* Updates remaining until next screen. */
    uint8_t _counter = 0;

    static constexpr uint8_t _rst_pin = 2;  // PD2, Arduino pin D2
    static constexpr uint8_t _dc_pin  = 8;  // PB0, Arduino pin D8
    static constexpr uint8_t _cs_pin  = 7;  // PD7, Arduino pin D7
};
