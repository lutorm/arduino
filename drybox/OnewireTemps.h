#include "EventLoop.h"
#include "onewiretemp.h"
#include <inttypes.h>

/* Class periodically reads temperature probes over 1-wire. Note that
 * this class cannot be initialized statically. */
class OnewireTemps : public Periodic
{
public:
    OnewireTemps(bool verbose);

    void _dispatch(ms_t now) override;

    // Make an enum with symbolic names to the probes
    enum class Probes : uint8_t
    {
        Right,
        Left,
        Top,
        Hotsink,
        Coldsink,
        Ambient
    };

    float const & temp(Probes i) { return _temps[uint8_t(i)]; }

    const prog_char * name(Probes i);

    void send_data(uint16_t & crc);

    static uint8_t constexpr n_devs = 6;

private:
    /* Arduino pin 4 has the 1-wire bus. */
    static uint8_t constexpr onewirepin = 4;

    float _temps[n_devs];

    OneWireBus _bus;
};
