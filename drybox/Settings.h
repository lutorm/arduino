#pragma once

#include "float16.h"

/* Drybox settings, saved to EEPROM and loaded at boot. Init values
 * are the "factory defaults" used if eeprom does not contain
 * anything. */
struct Settings
{
    /* Controller settings. */
    float16 boxtemp_pid_pgain = 10;
    float16 boxtemp_pid_igain = 0.01;
    float16 boxtemp_pid_dgain = 0;
    float16 boxtemp_pid_maxout = 110;

    /* Empirically, the heater setpoint needed to maintain 60C with a
     * 22C ambient was 80C, so for a 38C delta-T in the box we needed
     * a 58C delta-T on the heater. */
    float16 boxtemp_feedforward_gain = (80.0f - 22.0f) / (60.0f - 22.0f);

    float16 heater_pid_pgain = 0.1;
    float16 heater_pid_igain = 1e-3;
    float16 heater_pid_dgain = 0;

    /* for cooler pwm control, not used. 
    float16 cooler_pid_pgain = 0.3;
    float16 cooler_pid_igain = 1e-3;
    float16 cooler_pid_dgain = 0;
    */

    float16 fan2_pid_pgain = 0.05;
    float16 fan2_pid_igain = 5e-4;
    float16 fan2_pid_dgain = 0;

    float16 fan2_min_pwm = 0.07;
    float16 fan2_max_pwm = 0.15;

    /* State machine settings. */

    /* Dewpoint threshold for starting to dry. */
    float16 idle_dewpoint_threshold = 10;

    /* Once dewpoint has been above threshold for 6h, start a dry cycle. */
    uint16_t idle_dewpoint_persistence = 6*60*60;

    /* boxtemp setpoint during Baking. */
    float16 bake_temp = 80;

    /* If dewpoint goes above this during warmup, start drying. */
    float16 warmup_dewpoint_threshold = 35;

    /* Dewpoint spread during drying. */
    float16 dewpoint_spread = 7;

    /* When tracking the dewpoint, we never set the cooler to less
     * than this to avoid freezing. (The probe is closer to the
     * Peltier element than the cold sink, so the cold sink will
     * always be warmer than the probe.) */
    float16 min_cooler_setpoint = 1.0;

    /* Dehumidifier fan speed (when not controlled) */
    float16 dehum_fan_speed = 26./255;

    /* Hotsink fan speed (when not at full). */
    uint8_t hotsink_fan_speed = 25;

    /* Case fan speeds in various states. */
    uint8_t case_fan_speed_idle = 25;
    uint8_t case_fan_speed_cooldown = 128;

    /* Cycles before a bad sensor triggers safe mode. */
    uint8_t bad_sensor_persistence = 20;
    
    /* Cycles before a railed heater triggers safe mode. */
    uint16_t railed_heater_persistence = 7*60;
    
    /* Minimum time for baking state. */
    uint32_t min_baking_cycles = 2LL*60*60;

    /* Backup timeout of baking state. */
    uint32_t max_baking_cycles = 24LL*60*60;

    /* Run one bake cycle per month even if dewpoint hasn't gone up. [s]*/
    uint32_t bake_interval = 7LL*24*60*60;
    
    /* Save settings and crc to eeprom. */
    void save_to_eeprom() const;

    /* Read back settings from eeprom. Return true if CRC matched,
     * false if not. */
    bool read_from_eeprom();
};
