#pragma once

#include "EventLoop.h"
#include <avr/pgmspace.h>
#include <inttypes.h>

/* Class monitors pin change interrupts for the 3 fan tach pins
 * PINC1-3 and counts pin changes to calculate RPM. The counts are
 * evaluated when timer 1 overflows, so the time for that needs to be
 * supplied in the constructor. Timer 1 overflow interrupt needs to be
 * enabled.
 *
 * Note that the ordering is such that Fan 1 is rpm 1, Fan 2 rpm 2,
 * and Fan 3 rpm 0. */

class Fantach : public Periodic
{
public:
    Fantach(float t_ovf);

    float const & rpm(uint8_t i) const { return _rpm[i]; }

    void _dispatch(ms_t) override;

    void send_data(uint16_t & crc);

    static uint8_t constexpr n_fans = 3;

private:
    /* 64-bit address to send fan RPMS with. fan number is put in the last char. */
    static const prog_uint8_t _address[];

    /* Storage for the calculated RPM. Copied over from the volatile in dispatch. */
    float _rpm[n_fans];

    /* Empirically determined based on full speed. */
    static int8_t const _pulses_per_rev = 4;
};
