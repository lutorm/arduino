#include "Settings.h"
#include <util/crc16.h>
#include <EEPROM.h>

void Settings::save_to_eeprom() const
{
    uint16_t        crc  = 0;
    uint8_t const * data = reinterpret_cast<uint8_t const *>(this);
    uint8_t         i    = 0;

    for (; i < sizeof(Settings); ++i)
    {
        crc = _crc16_update(crc, data[i]);
        EEPROM.write(i, data[i]);
    }
    EEPROM.write(i, crc & 0x00ff);
    EEPROM.write(i + 1, crc >> 8);
}

bool Settings::read_from_eeprom()
{
    uint16_t  crc  = 0;
    uint8_t * data = reinterpret_cast<uint8_t *>(this);
    uint8_t   i    = 0;

    for (; i < sizeof(Settings); ++i)
    {
        data[i] = EEPROM.read(i);
        crc     = _crc16_update(crc, data[i]);
    }

    uint16_t stored_crc;
    stored_crc = EEPROM.read(i) + (EEPROM.read(i + 1) << 8);

    return stored_crc == crc;
}
