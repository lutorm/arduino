#include "Thermistor.h"
#include "serial_data.h"

const prog_uint8_t ThermistorReader::address[] PROGMEM            = "HETRTEMP";
const prog_uint8_t ThermistorReader::address_resistance[] PROGMEM = "HEATRRES";

ThermistorReader::ThermistorReader(ms_t period) : Periodic(period)
{
    // enable adc, prescaler 128, no interrupts
    ADCSRA = _BV(ADEN) | _BV(ADPS0) | _BV(ADPS1) | _BV(ADPS2);
    // Read ADC7 against AVcc
    ADMUX = _BV(REFS0) | pin;
}

void ThermistorReader::_dispatch(ms_t now)
{
    uint16_t result;
    ADCSRA |= _BV(ADSC);  // Convert
    while (bit_is_set(ADCSRA, ADSC))
        ;
    result = ADCL;
    result |= ADCH << 8;

    // Resistance can be calculated from the voltage on the voltage divider.
    float const R = Rref*result / (0x03ff - result);

    // Calculate the temperature in C. 
    _temp = B/logf(R/Rinf) - 273.15;
}

void ThermistorReader::send_data(uint16_t & crc)
{
    ::send_data_pgmaddr(address, _temp, crc);
}
