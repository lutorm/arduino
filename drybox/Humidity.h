#pragma once

#include "EventLoop.h"
#include "Wire.h"
#include "SparkFun_Si7021_Breakout_Library.h"

/* Reads Si7021 humidity/temperature readings. Reads from 2 probes,
 * that are isolated on two I2C buses using an PI4MSD5V9540B I2C
 * switch. */
class HumidityReader : public Periodic
{
public:
    HumidityReader(ms_t period);

    float const & humidity(uint8_t i) { return _hum[i]; }
    float const & temperature(uint8_t i) { return _temp[i]; }
    float const & dewpoint(uint8_t i) { return _dewpt[i]; }

    void send_data(uint16_t & crc) const;

private:
    void _dispatch(ms_t) override final;

    /* Switch the I2C switch to the specified bus. */
    void _switch_to_bus(uint8_t i);

    /* Sparkfun Si7021 library. */
    Weather<TwoWire> sensor;

    /* Humidities and temperatures we read, plus calculated
     * dewpoint. */
    float _hum[2];
    float _temp[2];
    float _dewpt[2];

    /* 64-bit addresses to send humidity/temperature with. probe
     * number is put in the last char. */
    static const prog_uint8_t humaddress[];
    static const prog_uint8_t tempaddress[];
    static const prog_uint8_t dewptaddress[];
};
