#pragma once

#include "EventLoop.h"
#include "memory.h"
#include "float16.h"

/* State machine operates on a 1s cadence. */
class StateMachine : public Periodic
{
public:
    StateMachine();

    enum State : uint8_t
    {
        Poweron = 0,
        Init,
        Safe,
        Idle,
        Warmup,
        Predry,
        Dry,
        Drydown,
        Cooldown,
        n_states
    };

    const prog_char * current_state() const;

    void send_data(uint16_t & crc);

    /* Request a transition to a new state. This may not happen if
     * another transition has already been requested. */
    void transition_to(State new_state);

private:
    void _start();
    
    void _dispatch(ms_t now) override final;

    void _check_safe();

    State _state = Poweron;

    /* Next state, set by transition commands. Immediately transition to init. */
    State _new_state = Init;

    /* Cycles spent in current state. */
    uint32_t _state_cycles = 0;

    uint16_t _idle_dewpoint_ctr;

    /* For keeping track of temperature derivative. */
    float16 _last_temp = 0;
    
    /* For keeping track of dewpoint derivative. */
    float16 _last_coldsink = 0;
    
    /* For keeping track of dewpoint derivative. */
    float16 _last_dewpoint = 0;

    /* Cycles with bad temp sensors. */
    uint8_t _bad_sensor_ctr = 0;

    /* Cycles with railed heater. */
    uint16_t _railed_heater_ctr = 0;
};
