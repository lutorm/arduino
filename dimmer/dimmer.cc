#include "memory.h"
#include <Arduino.h>
#include <avr/sleep.h>

/* The number of e-foldings in light from max to min. The LEDs are basically off at 30. */
uint16_t const out_max = 32768;
uint16_t const out_min = 30;

void setup()
{
    Serial.begin(57600);

    sprint_pgm(PSTR("\n\n\n\nPatrik's lanai dimmer booting\n\r"));
    // configure sleep mode
    set_sleep_mode(SLEEP_MODE_IDLE);

    // Read ADC3 against AVcc
    ADMUX = _BV(REFS0) | 0x03;
    // enable adc, prescaler 128, no interrupts
    ADCSRA = _BV(ADEN) | _BV(ADPS0) | _BV(ADPS1) | _BV(ADPS2);

    /* Set all pins to input pull-up, except PB5 which has the LED and the ADC3/PC3 */
    DDRB  = 1 << 5;
    DDRC  = 0x00;
    DDRD  = 0x00;
    PORTB = 0xff;
    PORTC = 0xf7;
    PORTD = 0xff;

    /* Timer 1 runs 16-bit fast pwm on OC1A with prescaler 1 with ICR1
     * as the top value. Clear interrupt flags, disable timer overflow
     * interrupt. Set OC1A to outputs. */
    OCR1A  = 0x0000;
    ICR1   = out_max;
    TCCR1A = _BV(COM1A1) | _BV(WGM11);
    TCCR1B = _BV(WGM12) | _BV(WGM13) | _BV(CS10);
    TIFR1  = 0xff;
    /* OC1A is PB1. Set as output. */
    DDRB |= 0x02;

    sprint_pgm(PSTR("Free mem after initialization: "));
    Serial.println(get_free_memory());
}

void loop()
{
    /* Toggle LED */
    PINB |= 1 << 5;

    uint16_t result;
    ADCSRA |= _BV(ADSC);  // Convert
    while (bit_is_set(ADCSRA, ADSC))
        ;
    result = ADCL;
    result |= ADCH << 8;

    sprint_pgm(PSTR("Read ADC "));
    Serial.print(result);
    sprint_pgm(PSTR(" input V= "));
    Serial.print(result / 1024. * 5);

    /* ADC values for min/max intensity, The dimmer is very noisy at
     * the lowest end so we have to truncate at a larger number so we
     * don't flicker.
     */
    uint16_t const adcmax = 970;
    uint16_t const adcmin = 200;

    if (result < adcmin)
        result = adcmin;
    if (result > adcmax)
        result = adcmax;

    float const frac = float(result - adcmin) / (adcmax - adcmin);

    sprint_pgm(PSTR(" frac "));
    Serial.print(frac);

    /* Output intensity in logs. */
    float const out_logrange = log(out_max / out_min);
    float const log_output   = out_logrange * frac;

    /* Output intensity in real units. */

    // uint16_t const output = frac*(out_max-out_min)+out_min;
    uint16_t const output = exp(log_output) * out_min;

    OCR1A = output;

    // sprint_pgm(PSTR(" Log-output "));
    // Serial.println(log_output);
    sprint_pgm(PSTR(" Output "));
    Serial.println(output);

    delay(20);
}
