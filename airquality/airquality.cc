#include "EventLoop.h"
#include "memory.h"
#include "serial_data.h"
#include <Arduino.h>
#include <avr/sleep.h>
#include <avr/wdt.h>
#include <util/delay.h>
#include "SoftwareWire.h"
#include "SparkFunBME280.h"
#include "SparkFun_ENS160.h"
#include "SparkFun_Si7021_Breakout_Library.h"

bool verbose = false;

int i2c_power_pin = 4;
int sdaPin = A2;
int sclPin = A3;

SoftwareWire sw(sdaPin, sclPin, false);

BME280 bme280;
Weather<SoftwareWire> si7021;
SparkFun_ENS160<SoftwareWire> ens160;

bool bme280_present = false;
bool si7021_present = false;
bool ens160_present = false;

float temperature;
float humidity;
float pressure;
int ens160_status;
float tvoc;

const prog_uint8_t address_temp[] PROGMEM   = "ROOMTEMP";
const prog_uint8_t address_humidity[] PROGMEM    = "ROOMHUMI";
const prog_uint8_t address_pressure[] PROGMEM = "ROOMPRES";
const prog_uint8_t address_tvoc[] PROGMEM = "ROOMTVOC";

/* This is defined inline here so it sees all the global objects and
 * can ask them to send. */
class SerialSender : public Periodic
{
public:
    SerialSender(ms_t period);

private:
    void _dispatch(ms_t) override final;
  int i=0;
};

SerialSender::SerialSender(ms_t period) : Periodic(period) {}

void SerialSender::_dispatch(ms_t)
{
    digitalWrite(LED_BUILTIN, HIGH);
    const ms_t tick = millis();

    uint16_t crc = 0x0000;
    send_header(4, tick);

    uint8_t addr[8];
    memcpy_P(addr, address_temp, 8);
    ::send_data(addr, temperature, crc);

    memcpy_P(addr, address_humidity, 8);
    ::send_data(addr, humidity, crc);

    memcpy_P(addr, address_pressure, 8);
    ::send_data(addr, pressure, crc);

    memcpy_P(addr, address_tvoc, 8);
    ::send_data(addr, tvoc, crc);

    // we dont' even wait for a response, that's useless.
    bool ack = wait_for_response(crc, 0);
    digitalWrite(LED_BUILTIN, LOW);
}

SerialSender sender(30000);

/* This is defined inline here so it sees all the global objects and
 * can ask them to send. */
class MeasurementEvent : public Periodic
{
public:
    MeasurementEvent(ms_t period);

private:
    void _dispatch(ms_t) override final;
};

MeasurementEvent::MeasurementEvent(ms_t period) : Periodic(period) {}

void MeasurementEvent::_dispatch(ms_t)
{
  if (bme280_present)
    {
  humidity = bme280.readFloatHumidity();
  pressure = bme280.readFloatPressure();
  temperature = bme280.readTempC();
    }
  if (si7021_present)
    {
      humidity = si7021.getRH();
      temperature = si7021.readTemp();
      pressure = NAN;
    }
  
    if (verbose)
    {
        Serial.print("Temperature = ");
        Serial.println(temperature);
        Serial.print("Humidity = ");
        Serial.println(humidity);
        Serial.print("Pressure = ");
        Serial.println(pressure);
    }

    if (ens160_present)
      {
	if (temperature == temperature)
	  {
	    ens160.setTempCompensationCelsius(temperature);
	    ens160.setRHCompensationFloat(humidity);
	  }
    
	ens160_status = ens160.getFlags();
	tvoc = ens160.getTVOC();
      }
    else
      {
	ens160_status = -1;
	tvoc = NAN;
      }
    
    if (verbose)
      {
	Serial.print("ENS160 status ");
	Serial.println(ens160_status);
	Serial.print("TVOC = ");
	Serial.println(tvoc);
      }
}

MeasurementEvent measure(30000);

void setup()
{
  // Figure out why we reset and re-configure the watchdog timer to
  // ensure we don't go into a reset loop.
    uint8_t mcusr = MCUSR;
    MCUSR         = 0;

    /* Set up watchdog timer to enable watchdog reset after
     * 8s. */
    cli();
    wdt_reset();
    WDTCSR |= _BV(WDE) | _BV(WDCE);
    WDTCSR = _BV(WDE) | _BV(WDP3) | _BV(WDP0);
    sei();

    // Configure pin for toggling i2c device power.
    pinMode(i2c_power_pin, OUTPUT);

    Serial.begin(19200);

    if (mcusr == 8)
      {
	// We got a watchdog reset. This may mean the I2C bus hung. Power-cycle the devices.
	Serial.print("Watchdog reset");
	digitalWrite(i2c_power_pin, LOW);
	delay(1000);
      }
    digitalWrite(i2c_power_pin, HIGH);
    delay(100);
    
    sw.begin();
    if (bme280.beginI2C(sw))
      {
	Serial.println("BME280 found.");
	bme280_present = true;
      }
    else
      {
	Serial.println("BME280 not detected.");
      }

    if (si7021.begin(sw))
      {
	Serial.println("Si7021 found.");
	si7021_present = true;
      }
    else
      {
	Serial.println("Si7021 not detected.");
      }
    
    if (ens160.begin(sw))
      {
	Serial.println("ENS160 found.");
	ens160_present = true;
      }
    else
      {
	Serial.println("ENS160 not detected.");
      }

    if (ens160_present)
    {
	ens160.setOperatingMode(SFE_ENS160_STANDARD);
    }
      
    event_loop.add_event(&measure);
    event_loop.add_event(&sender);

    sprint_pgm(PSTR("Air quality sensor controller booted successfully\r\n"));
    Serial.print(get_free_memory());
    sprint_pgm(PSTR(" bytes free\r\n"));
}

void loop()
{
    event_loop.dispatch(millis());
    wdt_reset();
    delay(100);
}
