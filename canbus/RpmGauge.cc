#include "RpmGauge.h"
#include "Arduino.h"
#include "debug.h"
#include "memory.h"

extern char tok;

RpmGauge::RpmGauge(ms_t period) : Periodic(period), _mode(0), _incoming_rpm(nullptr)
{
    _setup_rpm_timer();
}

void RpmGauge::set_incoming_ptr(uint16_t * ptr)
{
    _incoming_rpm = ptr;
}

/* The upper limit of the timer. This is adjusted to give the correct
 * output frequency on the RPM pin. */
volatile uint16_t rpm_timer_limit;

/* Timer 1 output compare match interrupt. We only change the OCR1A
 * from here to avoid "missing" cycles when the value is dropping. */
ISR(TIMER1_COMPA_vect)
{
    OCR1A = rpm_timer_limit;
}

void RpmGauge::_setup_rpm_timer()
{
    // Set timer 1 to CTC mode, toggle output 1 on timer, prescaler 8x.
    TCCR1A = _BV(COM1A0);
    TCCR1B = _BV(WGM12) | _BV(CS11);

    // Initialize to 10k rpm to give visual indication of boot
    _update_rpm_timer(10000);
    OCR1A = rpm_timer_limit;

    // Enable timer interrupt and clear all interrupt flags (by writing one)
    TIFR1  = 0xff;
    TIMSK1 = _BV(OCIE1A);

    // OC1A is PB1, Arduino pin 9
    pinMode(9, OUTPUT);
}

/* Write the timer limit for the output to correspond to the input
 * RPM. (This generates a pulse train with 4*RPM frequency, which is
 * what the NC30 tach wants.) */
void RpmGauge::_update_rpm_timer(uint16_t rpm_in)
{
    /* Apply correction curve to the tach response. */
    float const corr =
        8.24910999e-01 + 3.99888010e-05 * rpm_in + -1.63626177e-09 * rpm_in * rpm_in;

    float rpm = rpm_in / corr;

    /* Sanity check. The min rpm is so the gauge doesn't bounce off
     * the lower hard stop when the engine isn't running. (This is
     * easier than stopping the timer.) */
    if (rpm > 16500)
    {
        rpm = 16500;
    }
    if (rpm < 700)
    {
        rpm = 700;
    }

    uint16_t const timval = F_CPU * 7.5 / (8. * rpm) - 1;

    /* update tach output timer. We disable interrupts while
     * writing to the limit so the interrupt doesn't copy a
     * half-written value to OCR1A. */

    cli();
    rpm_timer_limit = timval;
    sei();
}

void RpmGauge::_dispatch(ms_t now)
{
    if (!_mode)
    {
        _update_rpm_timer(swap_order(*_incoming_rpm));

        if (debug_level && tok == 't')
        {
            sprint_pgm(PSTR("Entering RPM test mode\r\n"));
            _mode         = 0x11;
            _override_rpm = 0;
            tok           = 0;
        }
    }
    else
    {
        if (_mode == 0x11)
        {
            /* Test mode. */
            if (tok > 0)

            {
                if (tok == '+')
                {
                    _override_rpm += 1000;
                    tok = 0;
                }
                else if (tok == '-')
                {
                    if (_override_rpm < 1000)
                        _override_rpm = 0;
                    else
                        _override_rpm -= 1000;
                    tok = 0;
                }
                sprint_pgm(PSTR("RPM: "));
                Serial.println(_override_rpm);
            }
        }
        else
        {
            /* Warning mode. Flip between mode nibbles at 1Hz. */
            _override_rpm =
                (now / 500) % 2 ? (_mode & 0x0f) * 1000 : ((_mode & 0xf0) >> 8) * 1000;
        }

        _update_rpm_timer(_override_rpm);
    }
}
