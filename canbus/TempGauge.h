#pragma once

#include "EventLoop.h"
#include "byteorder.h"
#include "misc.h"

/* Handles updating the PWM signal to temp gauge hooked
 * up to the Arduino's OC2B (pin X) based on the incoming value
 * from the MS. The value can also be overridden for testing
 * purposes. */
class TempGauge : public Periodic
{
public:
    TempGauge(ms_t period);

    /* Set the pointer where the incoming temp is stored. */
    void set_incoming_ptr(int16_t *);

    void _dispatch(ms_t) override final;

    /* Return current as-read temp in C. */
    float current_temp() const
    {
        return _incoming_temp ? f2c(0.1f * swap_order(*_incoming_temp)) : 0.0;
    }

private:
    void _setup_temp_pwm();

    void _update_temp_pwm(float temp);

    uint8_t _override;

    /* The address containing the incoming temperature. This is in
     * tenths of a degF and is assumed to need to be byte-swapped. */
    int16_t * _incoming_temp;

    /* The override temp. This is used by the test mode. */
    uint8_t _override_temp;
};
