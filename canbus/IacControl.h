#pragma once

#include "AccelStepper.h"
#include "EventLoop.h"

/* Controls the Idle Air Control Valve stepper motor. The desired
 * position is broadcast from the MS, and the AccelStepper library is
 * used to run the motor to that position. The correspondence between
 * the positions used by the MS and this class is done by homing the
 * motor and then initializing the position to the same value the MS
 * uses. This means that the value for "Wide Open Steps" in MS MUST be
 * the same the "open steps" config here. The MS homing sequence
 * shouldn't matter because it's over before we connect. */
class IacControl : public Event
{
public:
    IacControl();

    void dispatch(ms_t) override final;

    void set_command_ptr(int16_t *);

    int16_t target() { return _stepper.targetPosition(); }

    int16_t current() { return _stepper.currentPosition(); }

private:
    bool _homed;

    /* The pointer where the IAC position is stored. The data here
     * are assumed to need to be byte swapped. */
    int16_t * _pos_cmd;

    AccelStepper _stepper;

    /* The initial homing move. Note that this is issued as full
     * steps. Because the stepper controller is stateful (even in full
     * step mode it can be in one of two states) which phase is
     * powered at the end of the homing move depends on where it was
     * before. At power-on, this should be repeatable, but if the
     * Arduino is reset but the stepper driver is not, the position
     * after the homing move can differ by one full step. To be fully
     * repeatable, we would have to reset the controller before the
     * move, in which case we might not even have to worry about the
     * step size. (Although it's probably still more repeatable with
     * full steps.) */
    static const int16_t home_move = 65;

    /* Speed of the homing move, in full steps / second. */
    static constexpr float home_speed = 50;

    /* The position the motor will be initialized to after the initial
     * homing move. This is assumed to be more than fully open position, and
     * should be set such that 0 is fully closed. */
    static const int16_t home_pos = 186;

    /* Max commanded position. Commands greater than this are
     * truncated, which prevents the MS from commanding the motor into
     * the home stop. */
    static const int16_t open_pos = 180;

    /* Max speed in steps/second. 60 rpm should be plenty.  */
    static constexpr float max_speed = 400;

    /* Max accel in steps/s^2. */
    static constexpr float max_accel = 800;

    /* The pin used to switch the stepper driver from full steps (for
     * the home move) to quad step. */
    static const uint8_t step_pin = 2;
};
