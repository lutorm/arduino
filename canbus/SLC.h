#pragma once

#include "EventLoop.h"
#include <inttypes.h>

/* Interface to the 14.7 Sigma Lambda Controller. Uses I2C. The SLC
 * and the Atmega uses a different byte order than the MS, so the
 * returned values are byte swapped. The main function is to schedule
 * readers for lambda and temperature with an eventloop. */
class SLC
{
public:
    /* Connect to an SLC at the specified I2C addr. */
    SLC(uint8_t addr);

    /* Set a target memory destination in which the lambda value will
     * be deposited, and start reading with the specified period. */
    void start_reading_lambda(uint16_t * data_dest, ms_t period);

    /* Set a target memory destination in which the tmeperature value
     * will be deposited, and start reading with the specified
     * period. */
    void start_reading_temp(uint16_t * data_dest, ms_t period);

    uint8_t addr() const { return _lambda_reader.addr; }

private:
    struct Reader : public Periodic
    {
        bool for_lambda;

        uint8_t addr;

        /* Max number of read failures to accept before setting the
         * output to an invalid value. */
        uint8_t max_fails;

        uint8_t n_fails;

        uint16_t * data_dest;

        Reader();

        void _dispatch(ms_t) override final;
    };

    void _start_reading(uint16_t *, ms_t, Reader *);

    Reader _lambda_reader;
    Reader _temp_reader;
};

/* Read lambda16 value from the specified slc and write it to the
 * table. base_addr is the i2c addr for index 0, The return value is
 * the value that can be sent to the MS.
 * The calibration is such that it can never read < 0.68 (136) so
 * to maximize dynamic range we subtract that.
 * This means the TunerStudio calibration is:
 * "0V" = 136*0.005 = lambda 0.68 = 10.00 AFR.
 * "5V" = (136+1023)*0.005 = lambda 5.795 = 85.19 AFR.
 *
 * If no value can be read, 1023 is returned to distinguish from when
 * the MS can't read the canbus value which usually ends up getting it
 * set to zero. In that case, error is also set to true.
 */
uint16_t read_lambda(int addr, bool & error);

/* Read the ri_max and ri_min from the specified SLC and use this to
 * interpolate the element temperature. The returned value is the
 * temperature in C. If no value can be read, 0 is returned and error
 * is set to true. */
uint16_t read_temp(int addr, bool & error);

/* Read and print the Vref of the SLC at the specified I2c address. */
void read_vref(int addr);

/* Read and print the Free Air Calibration value of the SLC at the
 * specified I2c address. */
void read_fac(int addr);

/* Trigger SLC hardware calibration of the device at the specified I2C
 * address. */
void do_hardware_cal(int addr);

/* Trigger SLC free air calibration. */
void do_free_air_cal(int addr);

/* Trigger SLC register dump. */
void regdump(int addr);

/* Read and print FW rev. */
void read_fw(int addr);
