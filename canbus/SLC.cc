#include "SLC.h"
#include "I2C.h"
#include "byteorder.h"
#include "debug.h"
#include "misc.h"
#include <avr/pgmspace.h>

SLC::SLC(uint8_t addr)
{
    _lambda_reader.addr       = addr;
    _lambda_reader.for_lambda = true;
    _lambda_reader.max_fails  = 10;

    _temp_reader.addr       = addr;
    _temp_reader.for_lambda = false;
    /* Temp reader is used much less often so we need this threshold
     * to be less or we won't know about a read failure for quite a
     * while. */
    _temp_reader.max_fails = 1;
}

void SLC::start_reading_lambda(uint16_t * data_dest, ms_t period)
{
    _start_reading(data_dest, period, &_lambda_reader);
}

void SLC::start_reading_temp(uint16_t * data_dest, ms_t period)
{
    _start_reading(data_dest, period, &_temp_reader);
}

void SLC::_start_reading(uint16_t * data_dest, ms_t period, Reader * reader)
{
    reader->data_dest = data_dest;
    reader->set_period(period);

    event_loop.add_event(reader);
}

SLC::Reader::Reader() : n_fails(0) {}

void SLC::Reader::_dispatch(ms_t)
{
    bool           error;
    uint16_t const result =
        for_lambda ? read_lambda(addr, error) : read_temp(addr, error);

    if (!error)
    {
        n_fails = 0;
    }

    if (error && n_fails < max_fails)
    {
        ++n_fails;
    }

    /* Only set the output on success or if we've failed sufficiently
     * many times in a row. */
    if (n_fails == 0 || n_fails >= max_fails)
    {
        *data_dest = swap_order(result);
    }
}

PROGMEM float const nermest_delta[] = {40,  45,  50,  55,  60,  65,  70,  75,  80,
                                       85,  90,  95,  100, 105, 110, 120, 130, 140,
                                       150, 160, 170, 180, 190, 300, 400, 500, 750};

/* LSU4.9 temperature for the corresponding nermest delta values. */
PROGMEM float const nermest_temp[] = {1004, 926, 885, 859, 832, 815, 798, 788, 780,
                                      774,  769, 762, 757, 748, 733, 722, 713, 704,
                                      696,  694, 693, 688, 684, 641, 620, 598, 562};

int const ntemp = sizeof(nermest_temp) / sizeof(nermest_temp[0]);

uint8_t readBytes(uint8_t i2c_addr, uint8_t address, uint8_t n, uint8_t * value)
// Reads a byte from an I2C register address
// Address: device register (0 to 15)
// Value will be set to stored byte
// Returns I2c status
{
    uint8_t status = I2c.read(i2c_addr, address, n, value);
    return status;
}

uint8_t writeByte(uint8_t i2c_addr, uint8_t address, uint8_t value)
// Write a byte to a I2C register address
// Address: device register (0 to 15)
// Value: byte to write to address
// Returns I2c status
{
    uint8_t status = I2c.write(i2c_addr, address, value);
    return status;
}

bool read_float(uint8_t i2c_addr, uint8_t address, float & val)
{
    uint8_t buf[4];
    if (readBytes(i2c_addr, address, 4, buf))
    {
        return false;
    }

    /* read msb first. avr is little endian so it should go in the
     * largest byte. */
    uint8_t * vp = (uint8_t *)&val;
    vp[3]        = buf[0];
    vp[2]        = buf[1];
    vp[1]        = buf[2];
    vp[0]        = buf[3];
    return true;
}

uint8_t read_uint16(uint8_t i2c_addr, uint8_t address, uint16_t & val)
{
    uint16_t buf;

    uint8_t status = readBytes(i2c_addr, address, 2, (uint8_t *)&buf);
    if (status)
    {
        dppgm(1, "read_uint16 got");
        dpln(1, status);

        return false;
    }

    val = swap_order(buf);

    return true;
}

void read_vref(int addr)
{
    float f = 0;
    if (!read_float(addr, 23, f))
    {
        dppgm(2, "Could not read vref cal\r\n");
    }
    else
    {
        dp(2, "Vref cal: ");
        dpln(2, f, HEX);
    }
}

void read_fac(int addr)
{
    float f = 0;
    if (!read_float(addr, 31, f))
    {
        dppgm(2, "Could not read FAC value\r\n");
    }
    else
    {
        dppgm(2, "FAC value: ");
        dpln(2, f, HEX);
    }
}

uint16_t read_lambda(int addr, bool & error)
{
    uint16_t l;
    if (!read_uint16(addr, 35, l))
    {
        dppgm(1, "Failed reading lambda from I2C addr ");
        dpln(1, addr);
        // Set max val as a signal for failed I2C read, to
        // distinguish from when the MS isn't connected when it
        // usually read 0.
        error = true;
        return 1023;
    }

    error = false;

    /* The calibration is such that it can never read < 0.68 (136) so
     * to maximize dynamic range we should subtract that before we
     * transmit. This means the TunerStudio calibration is:
     * "0V" = 136*0.005 = lambda 0.68 = 10.00 AFR.
     * "5V" = (136+1023)*0.005 = lambda 5.795 = 85.19 AFR.
     */
    l -= 136;

    if (l > 1023)
        l = 1023;

    return l;
}

uint16_t read_temp(int addr, bool & error)
{
    uint16_t ri_max, ri_min;

    if (!read_uint16(addr, 11, ri_max) || !read_uint16(addr, 13, ri_min))
    {
        dppgm(2, "Failed reading ri_max/min from I2C addr ");
        dpln(2, addr);
        error = true;
        return 0;
    }

    error = false;

    int16_t const delta = ri_max - ri_min;

    int16_t current_temp = interpolate(delta, nermest_delta, nermest_temp, ntemp);

    if (current_temp > 1023)
        current_temp = 1023;

    return current_temp;
}

void do_hardware_cal(int addr)
{
    sprint_pgm(PSTR("Triggering hardware calc\r\n"));
    writeByte(addr, 0x01, 0x01);
}

void do_free_air_cal(int addr)
{
    sprint_pgm(PSTR("Triggering free air calc\r\n"));
    writeByte(addr, 0x02, 0x02);
}

void regdump(int addr)
{
    sprint_pgm(PSTR("\nRegister dump for addr "));
    Serial.println(addr);

    const int n = 38;
    uint8_t   buf[n];
    for (int i = 0; i < n; ++i)
        buf[i] = 0;

    if (readBytes(addr, 0, n, buf))
    {
        sprint_pgm(PSTR("Could not read regs\r\n"));
    }
    else
    {
        for (int i = 0; i < n; ++i)
        {
            Serial.print(i);
            sprint_pgm(PSTR(": "));
            Serial.println(buf[i], HEX);
        }
    }
}

void read_fw(int addr)
{
    sprint_pgm(PSTR("SLC @ addr "));
    Serial.println(addr);

    uint8_t fw = 99;
    if (readBytes(addr, 0x00, 1, &fw))
    {
        sprint_pgm(PSTR("Could not communicate with SLC\n\r"));
    }
    else
    {
        sprint_pgm(PSTR("Connected to SLC, FW version: "));
        Serial.println(fw);
    }
}
