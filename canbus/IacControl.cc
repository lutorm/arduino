#include "IacControl.h"
#include "Arduino.h"
#include "byteorder.h"
#include "memory.h"

IacControl::IacControl() :
    Event(),
    _homed(false),
    _pos_cmd(nullptr),
    /* The stepper driver is connected to pin PD5, Arduino D5 (step)
     * and PD7, Arduino D7 (dir). */
    _stepper(AccelStepper::DRIVER, 5, 7)
{
    /* Set stepper driver to full step. */
    pinMode(step_pin, OUTPUT);
    digitalWrite(step_pin, LOW);

    /* 2us is smaller than obtainable but that's ok. */
    _stepper.setMinPulseWidth(2);

    _stepper.setMaxSpeed(home_speed);
    _stepper.setAcceleration(max_accel / 4);

    /* Home the motor. Plus direction should be more open, because the
     * end stop is fully open. This must be enough to move the motor
     * across its entire range of motion, and overrides the Megasquirt
     * homing setting. */
    _stepper.move(home_move);
}

void IacControl::set_command_ptr(int16_t * ptr)
{
    _pos_cmd = ptr;

    /* Initialize to zero so valve moves closed. */
    *_pos_cmd = swap_order(0);
}

void IacControl::dispatch(ms_t)
{
    if (!_homed)
    {
        if (!_stepper.isRunning())
        {
            /* We have reached home pos. Switch to quad step and set current pos. */
            _homed = true;
            digitalWrite(step_pin, HIGH);
            _stepper.setMaxSpeed(max_speed);
            _stepper.setAcceleration(max_accel);
            _stepper.setCurrentPosition(home_pos);
            sprint_pgm(PSTR("IAC homed\r\n"));
        }
    }

    if (_homed && _pos_cmd)
    {
        int16_t cmd = swap_order(*_pos_cmd);

        /* Truncate the command to allowed range. */
        cmd = (cmd < 0) ? 0 : ((cmd > open_pos) ? open_pos : cmd);

        /* Only update the command if it's new. It appears to
         * do a fair amount of work. */

        if (cmd != _stepper.targetPosition())
        {
            _stepper.moveTo(cmd);
        }
    }

    _stepper.run();
}
