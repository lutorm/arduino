#include "TempGauge.h"
#include "Arduino.h"
#include "debug.h"
#include "memory.h"
#include "misc.h"

extern char tok;

PROGMEM float const temp_table[] = {60, 70, 80, 90, 100, 110, 115};

PROGMEM float const pwm_table[] = {64, 119, 147, 173, 214, 246, 255};

int const ntemp = sizeof(temp_table) / sizeof(temp_table[0]);

TempGauge::TempGauge(ms_t period) :
    Periodic(period), _override(0), _incoming_temp(nullptr)
{
    _setup_temp_pwm();
}

void TempGauge::set_incoming_ptr(int16_t * ptr)
{
    _incoming_temp = ptr;
}

void TempGauge::_setup_temp_pwm()
{
    // Clock Timer2 from the I/O clock.
    // ASSR = ASSR & (0xff ^ _BV(AS2));

    // Set timer 2 to fast pwm mode, OC2B to non-inverting mode
    // Prescaler to 1024 which should give us a 61Hz output.
    TCCR2A = _BV(COM2B1) | _BV(WGM21) | _BV(WGM20);
    TCCR2B = _BV(CS22) | _BV(CS21) | _BV(CS20);

    // OC2B is PD3, Atmega pin 1, Arduino pin D3.
    // Configure it as an output
    DDRD = DDRD | _BV(3);
    PIND = PIND & (0xff ^ _BV(3));

    // Initialize to 100C temp to give visual indication of boot
    _update_temp_pwm(100.0);
}

/* Write the duty cycle for the output to correspond to the desired
 * temperature. */
void TempGauge::_update_temp_pwm(float temp)
{
    // Linear fit to a few points that we decide have a certain temp
    float duty_cycle = interpolate(temp, temp_table, pwm_table, ntemp);
    duty_cycle       = duty_cycle > 255 ? 255 : duty_cycle;
    duty_cycle       = duty_cycle < 0 ? 0 : duty_cycle;

    /* Update PWM duty cycle. */
    OCR2B = uint8_t(duty_cycle);
}

void TempGauge::_dispatch(ms_t now)
{
    if (!_override)
    {
        _update_temp_pwm(current_temp());

        if (debug_level && tok == 'c')
        {
            sprint_pgm(PSTR("Entering Temp test mode\r\n"));
            _override      = 1;
            _override_temp = 70;
            tok            = 0;
        }
    }
    else
    {
        /* Test mode. */
        if (tok > 0)
        {
            if (tok == 'p')
            {
                // PWM test mode
                sprint_pgm(PSTR("Temp PWM test mode: "));
                _override = 2;
                tok       = 0;
            }
            if (tok == '+')
            {
                _override_temp += 10;
                tok = 0;
            }
            else if (tok == '-')
            {
                _override_temp -= 10;
                tok = 0;
            }
            if (tok == ']')
            {
                _override_temp += 1;
                tok = 0;
            }
            else if (tok == '[')
            {
                _override_temp -= 1;
                tok = 0;
            }
            sprint_pgm(PSTR("Temp override: "));
            Serial.println(_override_temp);
        }

        if (_override == 2)
        {
            OCR2B = _override_temp;
        }
        else
        {
            _update_temp_pwm(_override_temp);
        }
    }
}
