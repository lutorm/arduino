#pragma once

#include "EventLoop.h"
#include "byteorder.h"

/* Handles updating the signal to a frequency-driven RPM gauge hooked
 * up to the Arduino's OC1A (pin 9) based on the incoming RPM value
 * from the MS. The value can also be overridden for signaling
 * purposes. */
class RpmGauge : public Periodic
{
public:
    RpmGauge(ms_t period);

    /* Set the pointer where the incoming RPM is stored. */
    void set_incoming_ptr(uint16_t *);

    /* Set a warning mode. This flashes the gauge between two
     * different values every second. The values are determined by the
     * two nibbles in the code, ie 0x3a will flash between 3000 and
     * 10000 RPM. Setting a warn mode of 0 restores normal
     * operation. */
    void set_warn_mode(uint8_t code) { _mode = code; }

    void _dispatch(ms_t) override final;

    uint16_t current_rpm() const
    {
        return _incoming_rpm ? swap_order(*_incoming_rpm) : 0;
    }

private:
    void _setup_rpm_timer();

    void _update_rpm_timer(uint16_t rpm_in);

    /* Is zero during normal operation. If nonzero, the RPM signal
     * isn't sent from _incoming_rpm but is override by the test mode
     * (value 0x11) or a warning mode (any other value). */
    uint8_t _mode;

    /* The address containing the incoming RPM signal. This is assumed
     * to need to be byte-swapped. */
    uint16_t * _incoming_rpm;

    /* The override RPM. This is used by test or warning modes. */
    uint16_t _override_rpm;
};
