#pragma once

#include "EventLoop.h"

/* Class to read Arduino AD converter and put the results in a
 * specified location. So far we only use it for the internal
 * temperature reference. */
class ArdAdc : public Periodic
{
public:
    ArdAdc();

    void _dispatch(ms_t);

    /* Set the pointer where the temperature is stored. */
    void set_temp_ptr(uint16_t * ptr) { _temp_ptr = ptr; }

private:
    void _setup_ard_temp();

    /* The address where the temperature is stored. This is assumed
     * to need to be byte-swapped. */
    uint16_t * _temp_ptr;
};
