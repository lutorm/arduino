#include "ArdAdc.h"
#include "Arduino.h"
#include "byteorder.h"

/* Set the period to almost 5s. Temperature doesn't change very
 * fast. Don't use exactly 5s because we don't want to end up with
 * cycles in which everything executed. */
ArdAdc::ArdAdc() : Periodic(4999), _temp_ptr(nullptr)
{
    _setup_ard_temp();
}

void ArdAdc::_setup_ard_temp()
{
    // Set the internal reference and mux.
    ADMUX = (_BV(REFS1) | _BV(REFS0) | _BV(MUX3));
    ADCSRA |= _BV(ADEN);  // enable the ADC
}

void ArdAdc::_dispatch(ms_t)
{
    if (!_temp_ptr)
    {
        /* nothing to do */
        return;
    }

    ADCSRA |= _BV(ADSC);  // Start the ADC

    // Detect end-of-conversion (this could be done in a separate
    // event to avoid polling, but it doesn't take long and we don't
    // do it often.)
    while (bit_is_set(ADCSRA, ADSC))
        ;

    // Reading register "ADCW" takes care of how to read ADCL and ADCH.
    uint16_t const adc_val = ADCW;

    // this calibration could be totally wrong
    float const temp_C = (adc_val - 324.31) / 1.22;

    /* To avoid negative temperatures, we report K... */
    uint16_t temp_K = temp_C + 273.15;

    if (temp_K > 1023)
    {
        temp_K = 1023;
    }

    *_temp_ptr = swap_order(temp_K);
}
