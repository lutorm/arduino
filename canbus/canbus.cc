#include "ArdAdc.h"
#include "EventLoop.h"
#include "I2C.h"
#include "IacControl.h"
#include "MsCan.h"
#include "RpmGauge.h"
#include "SLC.h"
#include "TempGauge.h"
#include "byteorder.h"
#include "debug.h"
#include "memory.h"
#include <Arduino.h>
#include <avr/sleep.h>
#include <mcp_can.h>

MCP_CAN * mcp_2515 = nullptr;

MsCan * mscan = nullptr;

/* Readers for the two SLC wideband controllers at I2C address 1 and 2. */
SLC slc1(1);
SLC slc2(2);

/* Update the RPM gauge at 10Hz. */
RpmGauge * rpm_gauge = nullptr;

TempGauge * clt_gauge = nullptr;

ArdAdc * ard_adc = nullptr;

/* For controlling the IAC stepper. */
IacControl * iac = nullptr;

/* Data sent to the MS: */

/* Lambda16 values from the SLC, truncated to 0-1023. */
uint16_t * lambda = nullptr;

/* Calculated O2 sensor temperatures in C, truncated to 0-1023 (though
 * it doesn't really measure across that range...) */
uint16_t * o2_temp = nullptr;

/* Output arduino tick so we can see if the Arduino restarts. */
uint16_t * tick = nullptr;

/* EventLoop max cycle time. */
uint16_t * eventloop_max_cycle_time = nullptr;

char tok = 0;

class DebugPrintout : public Periodic
{
public:
    DebugPrintout();

    void _dispatch(ms_t) override final;
};

DebugPrintout debug_printout;

/* Checks that the Widebanmd sensors come up to temperature once the
 * engine is running, and if not triggers a warning on the RPM
 * gauge. */
class WidebandWarning : public Periodic
{
public:
    WidebandWarning();

    void _dispatch(ms_t) override final;

private:
    /* The SLC min temp readout is actually 650.38 so we have to require higher
     * than that to know the heater is working */
    static const uint16_t _min_wb_temp = 660;

    /* Allow 30s to pass before triggering the warning. */
    static const ms_t _t_wb_temp_warn = 30000;

    ms_t _t_valid_temp;
};

WidebandWarning wb_warn;

void setup()
{
    rpm_gauge = new RpmGauge(100);
    clt_gauge = new TempGauge(1001);
    ard_adc   = new ArdAdc;
    iac       = new IacControl;

    Serial.begin(57600);

    sprint_pgm(PSTR("\n\n\n\nPatrik's can-bus relay booting\n\r"));

    // configure sleep mode
    set_sleep_mode(SLEEP_MODE_IDLE);

    /* MCP2515 is connected to SPI bus, CS pin is Arduino pin 10. */
    uint8_t const mcp_2515_cs = 10;
    pinMode(mcp_2515_cs, OUTPUT);
    digitalWrite(mcp_2515_cs, true);
    mcp_2515 = new MCP_CAN(mcp_2515_cs);

    uint8_t const our_can_id = 1;
    mscan                    = new MsCan(mcp_2515, our_can_id);

    if (CAN_OK == mcp_2515->begin(CAN_500KBPS))
    {
        sprint_pgm(PSTR("CAN-bus init ok!\n\r"));
    }
    else
    {
        sprint_pgm(PSTR("CAN-bus init fail\n"));
        /* Can't do anything useful without the can bus. */
        while (true)
            ;
    }

    /* Allocate tables for sending to the MS */
    mscan->allocate_tables(2);
    mscan->allocate_table(0, 8);
    mscan->allocate_table(1, 8);
    lambda  = (uint16_t *)mscan->get_table_ptr(0, 0);
    o2_temp = (uint16_t *)mscan->get_table_ptr(0, 4);
    tick    = (uint16_t *)mscan->get_table_ptr(1, 0);

    ard_adc->set_temp_ptr((uint16_t *)mscan->get_table_ptr(1, 2));

    eventloop_max_cycle_time = (uint16_t *)mscan->get_table_ptr(1, 4);

    /* Allocate listeners for 11-bit broadcasts. */
    mscan->allocate_listeners(3);
    uint16_t const base_id = 1520;
    mscan->set_listener(0, base_id + 0);
    mscan->set_listener(1, base_id + 2);
    mscan->set_listener(2, base_id + 6);

    /* RPM is 4th number in id 00. */
    rpm_gauge->set_incoming_ptr(mscan->get_listener_ptr(base_id + 0) + 3);

    /* CLT is 4th number in id 02. */
    clt_gauge->set_incoming_ptr((int16_t *)mscan->get_listener_ptr(base_id + 2) + 3);

    /* iac is 4th number in id 06. */
    iac->set_command_ptr((int16_t *)mscan->get_listener_ptr(base_id + 6) + 3);

    if (lambda)
    {
        /* The remote ADCs are read every 10ms by the MS, but
         * recommended polling rate of the SLC is 25Hz. We dispatch
         * these immediately before the MsCan to minimize the lag
         * between getting new lambda values from the SLC and sending
         * them to the MS.  */
        int const lambda_interval = 40;

        slc1.start_reading_lambda(lambda, lambda_interval);
        slc2.start_reading_lambda(lambda + 1, lambda_interval);
    }

    event_loop.add_event(mscan);

    /* Now dispatch IAC and RPM gauge, since we want to control on
     * those as fast as possible after getting a new value from the
     * MS. */
    event_loop.add_event(rpm_gauge);
    event_loop.add_event(iac);

    I2c.begin();
    I2c.setSpeed(0);
    I2c.pullup(1);
    I2c.timeOut(2);

    for (int addr = 1; addr < 3; ++addr)
    {
        read_fw(addr);
    }

    if (o2_temp)
    {
        /* Add the reading of SLC temp to the event loop. This is much
         * less time critical. */
        int const temp_interval = 5000;

        slc1.start_reading_temp(o2_temp, temp_interval);
        slc2.start_reading_temp(o2_temp + 1, temp_interval);
    }

    event_loop.add_event(clt_gauge);
    event_loop.add_event(ard_adc);
    event_loop.add_event(&wb_warn);
    event_loop.add_event(&debug_printout);

    sprint_pgm(PSTR("Free mem after initialization: "));
    Serial.println(get_free_memory());
}

uint8_t       selected_addr = 1;
long long int t_tick        = 0;
long long int tick_interval = 100;

uint16_t real_tick = 0;

void loop()
{
    event_loop.dispatch(millis());

    *eventloop_max_cycle_time = swap_order(uint16_t(event_loop.max_cycle_time()));

    long long int const t = millis();

    if (t - t_tick > tick_interval || t < t_tick)
    {
        t_tick = t;

        ++real_tick;
        if (real_tick == 1024)
        {
            real_tick = 0;
        }
        *tick = swap_order(real_tick);
    }

    if (Serial.available() > 0)
    {
        tok = Serial.read();
    }

    if (tok == 'd')
    {
        debug_level++;
        sprint_pgm(PSTR("Increasing debug level\r\n"));
        tok = 0;
    }
    if (debug_level)
    {
        /* in debug mode, turn on reading commands. */

        if (tok == 'D')
        {
            if (debug_level)
                debug_level--;
            sprint_pgm(PSTR("Decreasing debug level\r\n"));
            tok = 0;
        }
        if (tok >= '1' && tok <= '9')
        {
            selected_addr = tok - '0';

            sprint_pgm(PSTR("Selected sensor #"));
            Serial.println(selected_addr);
            tok = 0;
        }
        if (tok == 'h')
        {
            do_hardware_cal(selected_addr);
            tok = 0;
        }
        if (tok == 'a')
        {
            do_free_air_cal(selected_addr);
            tok = 0;
        }
        if (tok == 's')  // SLC Regdump
        {
            regdump(selected_addr);
            tok = 0;
        }
        if (tok == 'r')
        {
            read_vref(selected_addr);
            read_fac(selected_addr);
            tok = 0;
        }
    }
}

DebugPrintout::DebugPrintout() : Periodic(5000) {}

void DebugPrintout::_dispatch(ms_t now)
{
    if (debug_level)
    {
        /* in debug mode, turn on printing of values. */

        sprint_pgm(PSTR("0: lambda = "));
        Serial.print((swap_order(lambda[0]) + 136) * 0.005);
        sprint_pgm(PSTR("\ttemp = "));
        Serial.println(swap_order(o2_temp[0]));

        sprint_pgm(PSTR("1: lambda = "));
        Serial.print((swap_order(lambda[1]) + 136) * 0.005);
        sprint_pgm(PSTR("\ttemp = "));
        Serial.println(swap_order(o2_temp[1]));

        sprint_pgm(PSTR("RPM = "));
        Serial.println(rpm_gauge->current_rpm());

        sprint_pgm(PSTR("Coolant temp = "));
        Serial.println(clt_gauge->current_temp());

        sprint_pgm(PSTR("IAC target = "));
        Serial.print(iac->target());
        sprint_pgm(PSTR("    current = "));
        Serial.println(iac->current());

        auto const can_status = mscan->can_status();
        if (can_status != CAN_OK)
        {
            sprint_pgm(PSTR("There were can-bus errors: "));
            Serial.println(can_status);
        }
    }
}

WidebandWarning::WidebandWarning() : Periodic(1000), _t_valid_temp(0) {}

void WidebandWarning::_dispatch(ms_t now)
{
    uint16_t const cur_rpm = rpm_gauge->current_rpm();

    if (cur_rpm > 0)
    {
        /* Set t_valid_temp if we have a valid temps on both widebands. */
        bool valid_temp = true;
        valid_temp &= swap_order(o2_temp[0]) > _min_wb_temp;
        valid_temp &= swap_order(o2_temp[1]) > _min_wb_temp;

        if (now - _t_valid_temp > _t_wb_temp_warn)
        {
            /* Time since valid temp or engine start exceeds warning limit. */

            if (valid_temp)
            {
                /* Temp is now valid. Turn warn mode off and reset time. */
                rpm_gauge->set_warn_mode(0);
                _t_valid_temp = now;
            }
            else
            {
                /* We've had rpm for more than the allowed time without
                 * the widebands coming up to temp. Enter warning mode. */
                rpm_gauge->set_warn_mode(0x4d);
            }
        }
    }
    else
    {
        /* If rpm is zero, we consider the temp valid so we have the
         * proper delay once the engine starts. */
        rpm_gauge->set_warn_mode(0);
        _t_valid_temp = now;
    }
}
